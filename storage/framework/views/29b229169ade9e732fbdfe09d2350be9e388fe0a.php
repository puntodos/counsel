
<h5><b><?php echo app('translator')->getFromJson('app.admin.deltalcase.diagnostic.'.$field); ?></b></h5>

	<?php if(strpos(__('app.admin.deltalcase.diagnostic.'.$field.'.description'),'admin.deltalcase.diagnostic') == ""): ?>
		<p><?php echo app('translator')->getFromJson('app.admin.deltalcase.diagnostic.'.$field.'.description'); ?></p>
	<?php endif; ?>

	<?php if(strpos(__('app.admin.deltalcase.diagnostic.'.$field.'.description2'),'admin.deltalcase.diagnostic') == ""): ?>
		<p><?php echo app('translator')->getFromJson('app.admin.deltalcase.diagnostic.'.$field.'.description2'); ?></p><br>
	<?php endif; ?>


<?php $__currentLoopData = $fields[$field]; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
	<?php
		$checked = false;
		if($item == $diagnostic[$field]):
			$checked = true;
		endif;
	?>
<div class="radio">
  <label>
  	<?php echo e(Form::radio($field, $item, $checked, ['class' => ''])); ?>

  	<?php echo app('translator')->getFromJson('app.admin.deltalcase.diagnostic.'.$field.'.'.$item); ?>
  </label>
</div>
<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
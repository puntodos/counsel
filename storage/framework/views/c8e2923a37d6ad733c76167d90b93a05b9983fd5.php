<?php $__env->startSection('content'); ?>

   <div class="main-section">
    
    <a href="<?php echo e(route('dashboard.listdentist')); ?>">
    <div class="dashbord dashbord-green">
      <div class="icon-section">
        <i class="fa fa-stethoscope" aria-hidden="true"></i><br>
        <small><?php echo app('translator')->getFromJson('app.admin.menu.option2'); ?></small>
        <p><?php echo e($dentists); ?></p>
      </div>
      <div class="detail-section">
        <?php echo app('translator')->getFromJson('app.admin.menu.seemore'); ?>
      </div>
    </div>
    </a>

    <a href="<?php echo e(route('dashboard.list.dentalcases')); ?>">
    <div class="dashbord dashbord-orange">
      <div class="icon-section">
        <i class="fa fa-bell" aria-hidden="true"></i><br>
        <small><?php echo app('translator')->getFromJson('app.admin.menu.option3'); ?></small>
        <p><?php echo e($dentalCases); ?></p>
      </div>
      <div class="detail-section">
        <?php echo app('translator')->getFromJson('app.admin.menu.seemore'); ?>
      </div>
    </div>
    </a>

    <a href="<?php echo e(route('dashboard.list.dentalcases.actives')); ?>">
    <div class="dashbord dashbord-blue">
      <div class="icon-section">
        <i class="fa fa-tasks" aria-hidden="true"></i><br>
        <small><?php echo app('translator')->getFromJson('app.admin.menu.option4'); ?></small>
        <p><?php echo e($dentalCasesActive); ?></p>
      </div>
      <div class="detail-section">
        <?php echo app('translator')->getFromJson('app.admin.menu.seemore'); ?>
      </div>
    </div>
    </a>

    <a href="<?php echo e(route('dashboard.list.dentalcases.ended')); ?>">
    <div class="dashbord dashbord-red">
      <div class="icon-section">
        <i class="fa fa-tasks" aria-hidden="true"></i><br>
        <small><?php echo app('translator')->getFromJson('app.admin.menu.option5'); ?></small>
        <p><?php echo e($dentalCasesEnded); ?></p>
      </div>
      <div class="detail-section">
        <?php echo app('translator')->getFromJson('app.admin.menu.seemore'); ?>
      </div>
    </div>
    </a>

    <a href="<?php echo e(route('dashboard.list.dentalcases.pending')); ?>">
    <div class="dashbord dashbord-skyblue">
      <div class="icon-section">
        <i class="fa fa-tasks" aria-hidden="true"></i><br>
        <small><?php echo app('translator')->getFromJson('app.admin.menu.option8'); ?></small>
        <p><?php echo e($dentalCasesPending); ?></p>
      </div>
      <div class="detail-section">
        <?php echo app('translator')->getFromJson('app.admin.menu.seemore'); ?>
      </div>
    </div>
    </a>

    <a href="<?php echo e(route('dashboard.list.dentalcases.unread')); ?>">
    <div class="dashbord">
      <div class="icon-section">
        <i class="fa fa-tasks" aria-hidden="true"></i><br>
        <small><?php echo app('translator')->getFromJson('app.admin.menu.option6'); ?></small>
        <p><?php echo e($dentalCasesRead); ?></p>
      </div>
      <div class="detail-section">
        <?php echo app('translator')->getFromJson('app.admin.menu.seemore'); ?>
      </div>
    </div>
    </a>

    <a href="<?php echo e(route('dashboard.list.dentalcases.unread.messages')); ?>">
    <div class="dashbord dashbord-green">
      <div class="icon-section">
        <i class="fa fa-comments" aria-hidden="true"></i><br>
        <small><?php echo app('translator')->getFromJson('app.admin.menu.option9'); ?></small>
        <p><?php echo e($dentalCaseswithUnReadMessages); ?></p>
      </div>
      <div class="detail-section">
        <?php echo app('translator')->getFromJson('app.admin.menu.seemore'); ?>
      </div>
    </div>
    </a>

    <a href="<?php echo e(route('dashboards.users')); ?>">
    <div class="dashbord dashbord-orange">
      <div class="icon-section">
        <i class="fa fa-user" aria-hidden="true"></i><br>
        <small><?php echo app('translator')->getFromJson('app.admin.menu.option7'); ?></small>
        <p><?php echo e($users); ?></p>
      </div>
      <div class="detail-section">
        <?php echo app('translator')->getFromJson('app.admin.menu.seemore'); ?>
      </div>
    </div>
    </a>
    
  </div>
    
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.admin_template', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
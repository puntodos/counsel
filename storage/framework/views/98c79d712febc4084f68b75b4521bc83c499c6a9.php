<?php $__env->startSection('content'); ?>
 
    <?php echo Form::open(['route' => ['dentist.update', $dentist], 'method' => 'PUT', 'files' => true]); ?>


      <?php echo csrf_field(); ?>
      <?php echo $__env->make('dentist.form', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
      <div class="container">
          <div class="row">
                <div class="col-md-12 text-center">
                  <div class="justify-content-center">
                    <?php $__env->startComponent('components.app_label_error_alone', ['field' => 'tyc']); ?>
                    <?php echo $__env->renderComponent(); ?>
                    <br>
                    <?php if($dentist->tyc == 1): ?>
                    <?php echo e(Form::checkbox('tyc', 'on', true)); ?> <?php echo e(__('app.dentist.tyc')); ?>

                    <?php else: ?>
                    <?php echo e(Form::checkbox('tyc', 'on')); ?> <?php echo e(__('app.dentist.tyc')); ?>

                    <?php endif; ?>
                    <?php echo e(Form::submit(__('app.dentist.create_btn'),['class' => 'btn btn-primary'])); ?>

                  </div>
                </div>
            </div>
          </div>
      </div>
    <?php echo Form::close();; ?>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.dentist_template', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php $__env->startPush('scripts'); ?>
<script src="<?php echo e(asset('js/dashboard.js')); ?>" ></script>
<?php $__env->stopPush(); ?>

<?php $__env->startSection('content'); ?>
<?php echo $__env->make('flash::message', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header"><?php echo app('translator')->getFromJson('app.dentist.changepassword'); ?></div>

                <div class="card-body">
                    <?php echo Form::open(['route' => ['dashboards.users.edit_update', $dentist], 'method' => 'PUT', 'files' => true]); ?>

                        <?php echo csrf_field(); ?>

                        <input type="hidden" name="token" value="<?php echo e($token); ?>">

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right"><?php echo e(__('app.dentist.newpassword')); ?></label>

                            <div class="col-md-6">
                                <input id="newpassword" type="password" class="form-control<?php echo e($errors->has('newpassword') ? ' is-invalid' : ''); ?>" name="newpassword" required>

                                <?php if($errors->has('newpassword')): ?>
                                    <span class="invalid-feedback">
                                        <strong><?php echo e($errors->first('newpassword')); ?></strong>
                                    </span>
                                <?php endif; ?>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password-confirm" class="col-md-4 col-form-label text-md-right"><?php echo e(__('app.dentist.newpassword2')); ?></label>
                            <div class="col-md-6">
                                <input id="newpassword2" type="password" class="form-control<?php echo e($errors->has('newpassword2') ? ' is-invalid' : ''); ?>" name="newpassword2" required>

                                <?php if($errors->has('newpassword2')): ?>
                                    <span class="invalid-feedback">
                                        <strong><?php echo e($errors->first('newpassword2')); ?></strong>
                                    </span>
                                <?php endif; ?>
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    <?php echo e(__('app.dentist.changepassword')); ?>

                                </button>
                            </div>
                        </div>
                    <?php echo Form::close();; ?>

                </div>
            </div>
        </div>
    </div>
</div>
<?php $__env->stopSection(); ?>


<?php echo $__env->make('layouts.admin_template', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
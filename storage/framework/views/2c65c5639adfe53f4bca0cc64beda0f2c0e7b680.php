<!DOCTYPE html>
<html lang="<?php echo e(app()->getLocale()); ?>">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="<?php echo e(csrf_token()); ?>">

    <link rel="shortcut icon" href="<?php echo e(asset(config('constants.options.favico'))); ?>" type="image/x-icon" />
    <title><?php echo e(config('app.name', 'COUNSEL ORTHODONTIST')); ?></title>

    <!-- Scripts -->
    <script src="<?php echo e(asset('js/app.js')); ?>" ></script>
    <script src="<?php echo e(asset('js/popper.min.js')); ?>" ></script>
    <script src="<?php echo e(asset('js/general.js')); ?>" ></script>

    <?php echo $__env->yieldPushContent('scripts'); ?>
    <!-- Fonts -->
    <link rel="dns-prefetch" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Raleway:300,400,600" rel="stylesheet" type="text/css">

    <!-- Styles -->
    <link href="<?php echo e(asset('css/app.css')); ?>" rel="stylesheet">
    <link href="<?php echo e(asset('css/card.css')); ?>" rel="stylesheet">
    <link href="<?php echo e(asset('css/font-awesome.min.css')); ?>" rel="stylesheet">
    <link href="<?php echo e(asset('css/bootstrap-datepicker.min.css')); ?>" rel="stylesheet">

    
</head>
<body>
    <?php $__env->startComponent('components.app_errors_modal'); ?>
    <?php echo $__env->renderComponent(); ?>
    <?php echo $__env->make('layouts.header.admin', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <?php if(isset(session()->all()['flash_notification'])): ?>
        <div class="container py-2">
            <div class="row">
                <div class="col-md-12">
                    <?php echo $__env->make('flash::message', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                </div>
            </div>
        </div>
        <script>
        $('div.alert').not('.alert-important').delay(3000).fadeOut(100  0);
        </script>
    <?php endif; ?>

    <div id="app"  style="min-height: 70%">
        
            <div class="container py-2">
              <div class="row">
                <div class="col-md-2">
                  <ul class="nav nav-pills flex-column">
                    <li class="nav-item">
                      <a href="<?php echo e(route('dashboard.index')); ?>" class="active nav-link">
                        <i class="fa fa-home fa-home"></i>&nbsp;
                      <?php echo app('translator')->getFromJson('app.admin.menu.option1'); ?>
                      </a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link" href="<?php echo e(route('dashboard.listdentist')); ?>"><?php echo app('translator')->getFromJson('app.admin.menu.option2'); ?></a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link" href="<?php echo e(route('dashboard.list.dentalcases')); ?>"><?php echo app('translator')->getFromJson('app.admin.menu.option3'); ?></a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link" href="<?php echo e(route('dashboard.list.dentalcases.actives')); ?>"><?php echo app('translator')->getFromJson('app.admin.menu.option4'); ?></a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link" href="<?php echo e(route('dashboard.list.dentalcases.ended')); ?>"><?php echo app('translator')->getFromJson('app.admin.menu.option5'); ?></a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link" href="<?php echo e(route('dashboard.list.dentalcases.pending')); ?>"><?php echo app('translator')->getFromJson('app.admin.menu.option8'); ?></a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link" href="<?php echo e(route('dashboard.list.dentalcases.unread')); ?>"><?php echo app('translator')->getFromJson('app.admin.menu.option6'); ?></a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link" href="<?php echo e(route('dashboard.list.dentalcases.unread.messages')); ?>"><?php echo app('translator')->getFromJson('app.admin.menu.option9'); ?></a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link" href="<?php echo e(route('dashboards.users')); ?>"><?php echo app('translator')->getFromJson('app.admin.menu.option7'); ?></a>
                    </li>
                  </ul>
                </div>
                <div class="col-md-10">
                    <div class="row">
                    <?php echo $__env->yieldContent('content'); ?>   
                    </div>     
                </div>
              </div>
            </div>
            
        
    </div>
    <?php echo $__env->make('layouts.footer.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
</body>
</html>


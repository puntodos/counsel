<br>  

          <div class="row">
            <div class="col-md-12">
              <span class="badge badge-pill badge-danger">&nbsp;</span> <?php echo app('translator')->getFromJson('app.admin.list.dentist.unreadcases'); ?><br>
            </div>
          </div>
          <?php if($search != 'NA003'): ?>
          <?php echo Form::open(['url' => 'dashboard/list/dentalcases/'.$status, 'method' => 'GET', 'class' => 'form-2-loading']); ?>

          <?php echo csrf_field(); ?>
          <div class="row">
            <div class="col-md-4">
                <input type="text"  name="search" placeholder="<?php echo e(__('app.admin.list.dentist.search.title')); ?>">
              <button type="submit" class="btn btn-primary">
                  <?php echo e(__('app.admin.list.dentist.search')); ?>

              </button>
            </div>
            <?php if($search != ''): ?>
            <div class="col-md-8">
                <p>
                  <h5 style="font-weight: bold;"><?php echo e(__('app.admin.list.dentist.search.results')); ?> &quot;<?php echo e($search); ?>&quot;</h5>
                </p>
            </div>
            <?php endif; ?>
          </div>
          <?php echo Form::close();; ?>

          <?php endif; ?>
          
          <div class="row">
            <div class="col-md-12">
              <br>
              <table class="table table-hover">
                <thead>
                  <tr class="table-active">
                    <th scope="col"><?php echo app('translator')->getFromJson('app.admin.list.dentist.show.cases.patient.id'); ?></th>
                    <th scope="col"><?php echo app('translator')->getFromJson('app.admin.list.dentist.show.cases.patient.name'); ?></th>
                    <th scope="col"><?php echo app('translator')->getFromJson('app.admin.list.dentist.show.cases.patient.date_begin'); ?></th>
                    <th scope="col"><?php echo app('translator')->getFromJson('app.admin.list.dentist.show.cases.patient.dentist_name'); ?></th>
                    <th scope="col"><?php echo app('translator')->getFromJson('app.admin.list.dentist.show.cases.patient.status'); ?>
                    </th>

                  </tr>
                </thead>
                <tbody>
                  <?php $__currentLoopData = $dentalcases; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $dentalcase): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                  <tr scope="row" data-id="<?php echo e($dentalcase->id); ?>" 
                    data-href='<?php echo e(route('dashboard.dentalcase', $dentalcase->id)); ?>'>
                    <td class="dentist-show"><?php echo e($dentalcase->id); ?></td>
                    <td class="dentist-show"> 
                        <?php if($dentalcase->status_read == 'unread'): ?>
                          <span class="badge badge-pill badge-danger">&nbsp;</span>
                        <?php elseif($dentalcase->messages_unread_4_admin > 0): ?>
                          <span class="badge badge-pill badge-danger">&nbsp;</span>
                        <?php endif; ?>
                      <?php echo e($dentalcase->patient->name); ?>

                    </td>
                    <td class="dentist-show"><?php echo e($dentalcase->patient->created_at); ?></td>
                    <td class="dentist-show"><?php echo e($dentalcase->dentist->user->name); ?></td>
                    <td class="dentist-show">
                      <?php echo app('translator')->getFromJson('app.deltalcases.status.admin.'.$dentalcase->status); ?>
                    </td>
                  </tr>
                  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </tbody>
              </table>
            </div>
          
        </div>
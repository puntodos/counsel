
      <?php $__currentLoopData = $messages; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $message): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        <?php
          $bg = 'bg-primary ';
          $text_color = 'text-white';
          if($message->user->type != 'dentist'):
            $bg = 'bg-light';
            $text_color = '';
          endif;
        ?>
      <br>
      <div class="row">
        <div class="col-md-12">
          <div class="list-group">
            <span class="list-group-item list-group-item-action flex-column align-items-start <?php echo e($bg); ?> <?php echo e($text_color); ?>" >
              <div class="d-flex w-100 justify-content-between"> &nbsp; <small><?php echo e($message->created_at); ?></small> </div>
              <p class="mb-1">
                <?php echo e($message->text); ?>

              </p> 
              <small>
                <?php echo e($message->user->name); ?>

                <?php if($message->file_attach != ''): ?>
                - <a class="<?php echo e($text_color); ?>" href="<?php echo e($user_path.$message->file_attach); ?>" target="_blank">Archivo adjunto 
                  <i class="fa d-inline fa-lg fa-external-link <?php echo e($bg); ?>"></i>
                </a>
                <?php endif; ?>
              </small> 
            </span>
          </div>
        </div>
      </div>
      <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
     
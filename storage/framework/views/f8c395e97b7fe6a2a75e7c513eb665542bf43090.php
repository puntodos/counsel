
<?php if($errors->has($field)): ?>
<div class="alert alert-danger" role="alert">
  <strong><?php echo e($errors->first($field)); ?></strong>
</div>
<?php endif; ?>
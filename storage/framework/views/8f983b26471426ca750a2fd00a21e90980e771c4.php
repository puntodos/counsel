<?php echo Form::open(['route' => ['treatment.store', $dentalCase], 'method' => 'POST', 'files' => true]); ?>

<?php echo csrf_field(); ?>

	<?php $__currentLoopData = $diagnostic_json; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $group): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
		<?php
			$class = 'col';
			if($group['type'] == 'title'){
				$class = 'col-sm-12';
			}
			$cols = 0;
		?>

		<div class="row">
			<div class="<?php echo e($class); ?>">
				<h3>
					<b>
						<?php echo e($group['title']); ?>

					</b>
				</h3>
			</div>
		</div>
		<?php if(isset($group['fields']) && count($group['fields']) > 0): ?>
			<div class="row">
				<?php
					$cols = 0;
				?>
			<?php $__currentLoopData = $group['fields']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $field): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

				<?php if($field['type'] == "title"): ?>
						<div class="col">
							<h4>
								<b>
									<?php echo e($field['title']); ?>

								</b>
							</h4>
						</div>
					</div>
					<div class="row">
					<?php
						continue;
					?>
				<?php endif; ?>

				<?php
					$class = 'col';
					if(isset($field['width'])){
						$class = 'col-sm-'. $field['width'];
						$cols += $field['width'];
					}
				?>

				
				<div class="<?php echo e($class); ?>">
					<?php if($field['type'] == 'radio'): ?>
						<?php $__env->startComponent('components.app_diagnostic_radios_new', ['field' => $field, 'diagnostic' => $diagnostic]); ?>
						<?php echo $__env->renderComponent(); ?>
					<?php endif; ?>
					
					<?php if($field['type'] == 'number'): ?>
						<?php
						$value = '';
						if(isset($diagnostic[$field['name']])){
							$value = $diagnostic[$field['name']];
						}
						?>
						<h5><b><?php echo e($field['title']); ?></b></h5>
						<p><?php echo e($field['description']); ?></p>
						<div class="form-group"> 
				            <label style=""><?php echo e($field['description2']); ?></label>
				            <input class="form-control" placeholder="<?php echo e($field['placeholder']); ?>" required="" name="<?php echo e($field['name']); ?>" type="number" step="<?php echo e($field['step']); ?>" value="<?php echo e($value); ?>">
				        </div>
					<?php endif; ?>
					<?php if($field['type'] == 'check'): ?>
						<?php $__env->startComponent('components.app_diagnostic_check', ['field' => $field, 'diagnostic' => $diagnostic]); ?>
						<?php echo $__env->renderComponent(); ?>
					<?php endif; ?>
					<?php if($field['type'] == 'select'): ?>
						<h5><b><?php echo e($field['title']); ?></b></h5>
						<p><?php echo e($field['description']); ?></p>
						<div class="form-group"> 
				            <label style=""><?php echo e($field['description2']); ?></label>
				            <select class="form-control" name="<?php echo e($field['name']); ?>">
				            	<option value="">Seleccione una opcion</option>
				            	<?php $__currentLoopData = $field['values']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
				            		<?php
				            			$selected = '';
				            			if(isset($diagnostic[$field['name']]) && $value['value'] == $diagnostic[$field['name']]):
				            				$selected = 'selected';
				            			endif;
				            		?>
				            		<option value="<?php echo e($value['value']); ?>" <?php echo e($selected); ?>><?php echo e($value['value']); ?></option>
				            	<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
				            </select>
				        </div>
					<?php endif; ?>

					<?php if(isset($field['fields']) && count($field['fields']) > 0): ?>
						<?php $__currentLoopData = $field['fields']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $intField): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
							<?php if($intField['type'] == 'text'): ?>
								<?php
								$value = '';
								if(isset($diagnostic[$intField['name']])){
									$value = $diagnostic[$intField['name']];
								}
								?>
								<div class="form-group"> 
						            <input class="form-control" placeholder="<?php echo e($intField['placeholder']); ?>" name="<?php echo e($intField['name']); ?>" type="text" value="<?php echo e($value); ?>">
						        </div>
							<?php endif; ?>
						<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
					<?php endif; ?>
				</div>
				<?php if($cols == 12): ?>
					<?php	
						$cols = 0;
					?>
					</div>
					<hr>
					<div class="row">
				<?php endif; ?>
			<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
			</div>
		<?php endif; ?>
		<?php if($cols > 0): ?>
		<hr>
		<?php endif; ?>
	<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

	<div class="container">
	  	<div class="row">
	        <div class="col-md-12 text-center">
	          <div class="justify-content-center">
	            <br>
	            <?php echo e(Form::submit(__('app.admin.deltalcase.treatment.save_btn'),['class' => 'btn btn-primary'])); ?>

	          </div>
	        </div>
	    </div>
	 </div>
    <?php echo Form::close();; ?>
<?php $__env->startSection('loading_text'); ?>
<?php echo app('translator')->getFromJson('app.dentalcase.create.loading'); ?>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>

    <?php echo Form::open(['route' => 'dentalcases.store', 'method' => 'POST', 'files' => true, 'class' => 'form-2-loading']); ?>

      <?php echo csrf_field(); ?>
      
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class="row">
            <div class="col-md-12 bg-secondary">
              <h1 class="text-center my-1"><?php echo app('translator')->getFromJson('app.dentalcase.case_profile_title'); ?></h1>
            </div>
          </div>
          <br>
          <div class="row">
            <div class="col-md-4">
              <div class="form-group"> 
                <?php $__env->startComponent('components.app_label', ['field' => 'name', 'namespace' => 'app.dentalcase.patient']); ?>
                <?php echo $__env->renderComponent(); ?>
                  <?php echo Form::text('name'
                              , null
                              , ['class' => 'form-control'
                              , 'placeholder' => __('app.dentalcase.patient.name')
                              , 'required']); ?>

                </div>
              </div>
            <div class="col-md-4">
              <div class="form-group"> 
                <?php $__env->startComponent('components.app_label', ['field' => 'age', 'namespace' => 'app.dentalcase.patient']); ?>
                <?php echo $__env->renderComponent(); ?>
                  <?php echo Form::number('age'
                              , null
                              , ['class' => 'form-control'
                              , 'placeholder' => __('app.dentalcase.patient.age')
                              , 'required']); ?>

                </div>
              </div>
            <div class="col-md-4">
              <div class="form-group"> 
                <?php $__env->startComponent('components.app_label', ['field' => 'genere', 'namespace' => 'app.dentalcase.patient']); ?>
                <?php echo $__env->renderComponent(); ?>
                  <?php echo Form::select('genere'
                                , ['m' => __('app.dentalcase.patient.genere.m')
                                , 'f' => __('app.dentalcase.patient.genere.f')]
                                , null
                                , ['placeholder' => __('app.dentalcase.patient.genere.select'),
                                  'class' => 'form-control'
                                , 'required']); ?>

                </div>
              </div>
            
          </div>
          <div class="row">
            <div class="col-md-12 bg-secondary">
              <h1 class="text-center my-1"><?php echo app('translator')->getFromJson('app.dentalcase.patient.diagnosticimages'); ?></h1>
            </div>
          </div>

          <br>
          <div class="row">
            <div class="col-md-6">
                <?php $__env->startComponent('components.app_file_w_modal_help', ['field' => 'radiography1']); ?>
                <?php echo $__env->renderComponent(); ?>
            </div>
            <div class="col-md-6">
                <?php $__env->startComponent('components.app_file_w_modal_help', ['field' => 'radiography2']); ?>
                <?php echo $__env->renderComponent(); ?>
            </div>
          </div>
          <br>
          <div class="row">
            <div class="col-md-6">
                <?php $__env->startComponent('components.app_file_w_modal_help', ['field' => 'radiography3']); ?>
                <?php echo $__env->renderComponent(); ?>
            </div>
            <div class="col-md-6">
                <?php $__env->startComponent('components.app_file_w_modal_help', ['field' => 'radiography4']); ?>
                <?php echo $__env->renderComponent(); ?>
            </div>
          </div>
          <br>
          <div class="row">
            <div class="col-md-6">
                <?php $__env->startComponent('components.app_file_w_modal_help', ['field' => 'radiography5']); ?>
                <?php echo $__env->renderComponent(); ?>
            </div>
            <div class="col-md-6">
              
            </div>
          </div>
          <br>
          <div class="row">
            <div class="col-md-12 bg-secondary">
              <h1 class="text-center my-1"><?php echo app('translator')->getFromJson('app.dentalcase.patient.diagnosticimages2'); ?></h1>
            </div>
          </div>
          <br>
          <div class="row">
            <div class="col-md-6">
                 <?php $__env->startComponent('components.app_file_w_modal_help', ['field' => 'radiography6']); ?>
                <?php echo $__env->renderComponent(); ?>
            </div>
            <div class="col-md-6">
                 <?php $__env->startComponent('components.app_file_w_modal_help', ['field' => 'radiography7']); ?>
                <?php echo $__env->renderComponent(); ?>
            </div>
          </div>
          <br>
          <div class="row">
            <div class="col-md-6">
                 <?php $__env->startComponent('components.app_file_w_modal_help', ['field' => 'radiography8']); ?>
                <?php echo $__env->renderComponent(); ?>
            </div>
            <div class="col-md-6">
              
            </div>
          </div>
          <br>
          <div class="row">
            <div class="col-md-12 bg-secondary">
              <h1 class="text-center my-1"><?php echo app('translator')->getFromJson('app.dentalcase.patient.diagnosticimages3'); ?></h1>
            </div>
          </div>
          <br>
          <div class="row">
            <div class="col-md-6">
              <?php $__env->startComponent('components.app_file_w_modal_help', ['field' => 'radiography9']); ?>
                <?php echo $__env->renderComponent(); ?>
            </div>
            <div class="col-md-6">
              <?php $__env->startComponent('components.app_file_w_modal_help', ['field' => 'radiography10']); ?>
                <?php echo $__env->renderComponent(); ?>
            </div>
          </div>
          <br>
          <div class="row">
            <div class="col-md-6">
              <?php $__env->startComponent('components.app_file_w_modal_help', ['field' => 'radiography11']); ?>
                <?php echo $__env->renderComponent(); ?>
            </div>
            <div class="col-md-6">
              <?php $__env->startComponent('components.app_file_w_modal_help', ['field' => 'radiography12']); ?>
                <?php echo $__env->renderComponent(); ?>
            </div>
          </div>
          <br>
          <div class="row">
            <div class="col-md-6">
              <?php $__env->startComponent('components.app_file_w_modal_help', ['field' => 'radiography13']); ?>
                <?php echo $__env->renderComponent(); ?>
            </div>
            <div class="col-md-6">
              
            </div>
          </div>
          <br>
          <div class="row">
            <div class="col-md-12 bg-secondary">
              <h1 class="text-center my-1"><?php echo app('translator')->getFromJson('app.dentalcase.patient.objective_title'); ?></h1>
            </div>
          </div>
          <br>
          <div class="row">
            <div class="col-md-12">
             <div class="form-group"> <label><?php echo app('translator')->getFromJson('app.dentalcase.patient.objective_dentist'); ?></label> 
                <?php echo e(Form::textarea('objective_dentist'
                                  ,  null
                                  , ['size' => '4x5'
                                  , 'class' => 'form-control'
                                  , 'placeholder' => __('app.dentalcase.patient.objective_dentist')])); ?>

              </div>
            </div>
          </div>
          <br>
          <div class="row">
            <div class="col-md-12">
             <div class="form-group"> <label><?php echo app('translator')->getFromJson('app.dentalcase.patient.objective_patient'); ?></label> 
                <?php echo e(Form::textarea('objective_patient'
                                  ,  null
                                  , ['size' => '4x5'
                                  , 'class' => 'form-control'
                                  , 'placeholder' => __('app.dentalcase.patient.objective_patient')])); ?>

              </div>
            </div>
           
          </div>
          
          <hr>
      </div>
    </div>


      <div class="container">
          <div class="row">
                <div class="col-md-12 text-center">
                  <div class="justify-content-center">
                    <br>
                    <?php echo e(Form::button(__('app.dentalcase.patient.create_btn'),['class' => 'btn btn-primary ', 'id' => 'submit-btn'])); ?>

                    <script type="text/javascript">
                      $('#submit-btn').on("click", function () {
                        $('#loading').modal('show');
                        $('.form-2-loading').submit();
                      });
                    </script>
                  </div>
                </div>
            </div>
          </div>
      </div>
    <?php echo Form::close();; ?>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.dentist_template', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
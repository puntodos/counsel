<nav class="navbar navbar-expand-md bg-primary navbar-dark">
    <div class="container">
        <a class="navbar-brand" >
          <img width="220px" src="<?php echo e(asset(config('constants.options.logo2'))); ?>">
        </a>
      <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbar2SupportedContent" aria-controls="navbar2SupportedContent" aria-expanded="false"
        aria-label="Toggle navigation"> <span class="navbar-toggler-icon"></span> </button>
      <div class="collapse navbar-collapse text-center justify-content-end" id="navbar2SupportedContent">
        <a href="<?php echo e(route('login')); ?>" class="btn navbar-btn ml-2 text-white btn-link">
           <?php echo app('translator')->getFromJson('app.general.login'); ?> 
        </a>
        <a href="<?php echo e(route('register')); ?>" class="btn navbar-btn ml-2 text-white btn-secondary">
            <i class="fa d-inline fa-lg fa-user-circle-o"></i>
             <?php echo app('translator')->getFromJson('app.general.register'); ?> 
          </a>
      </div>
    </div>
</nav>
<?php $__env->startPush('scripts'); ?>
<script src="<?php echo e(asset('js/dashboard.js')); ?>" ></script>
<?php $__env->stopPush(); ?>

<?php $__env->startSection('content'); ?>

    <?php echo $__env->make('dentalcases.files_partial', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class="row">
            <div class="col-md-6 bg-secondary">
              <h3 class="my-1">
                <?php echo app('translator')->getFromJson('app.dentalcase.patient.show.trate'); ?> <?php echo e($patient->name); ?> 
                (<?php echo app('translator')->getFromJson('app.admin.deltalcases.status.'.$dentalCase->status); ?>)<br>
                ID: <?php echo e($dentalCase->case_by_dentist); ?>

              </h3>
            </div>
             <div class="col-md-2 bg-secondary">
              
            </div>
             <div class="col-md-4 bg-secondary">
                <br>
               
                  <a class="btn btn-warning btn-sm float-sm-right" style="margin: 1px" href="<?php echo e(route('dashboard.dentalcase', $dentalCase->id)); ?>">
                    <?php echo e(__('app.general.back_btn')); ?>

                  </a>
                  <?php if(count($diagnostic) > 0 && $dentalCase['diagnostic']['sndiagnostic'] != "N"): ?>
                  <a class="btn btn-dark btn-sm float-sm-right" style="margin: 1px" href="<?php echo e(route('diagnostic.pdf', $dentalCase['diagnostic']['id'])); ?>">
                    <?php echo e(__('app.admin.deltalcase.diagnostic.pdf.btn')); ?>

                  </a>
                  <?php endif; ?>
                
                
            </div>
          </div>
          <br>
          <div class="row">
            <div class="col-md-12 bg-secondary text-center">
              <h3 class="my-1">
                <?php echo app('translator')->getFromJson('app.admin.deltalcase.diagnostic.title'); ?>
              </h3>
            </div>            
          </div>
          <br>
          <div class="row">
            <div class="col-md-12 ">
              
              <?php echo $__env->make('dashboard.dentalcase_diagnostic_fields_v2', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

            </div>
          </div>
          <br>
          
          
          <hr>
      </div>
    </div>
  </div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.admin_template', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
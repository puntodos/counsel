<?php $__env->startPush('scripts'); ?>
<script src="<?php echo e(asset('js/dashboard.js')); ?>" ></script>
<?php $__env->stopPush(); ?>

<?php $__env->startSection('content'); ?>
 
        <div class="container">
          <div class="row">
            <div class="col-md-12 bg-secondary">
              <h1 class="text-center my-1"><?php echo app('translator')->getFromJson('app.admin.menu.option2'); ?></h1>
            </div>
          </div>
          <br>
          <div class="row">
            <div class="col-md-12">
              <span class="badge badge-pill badge-danger">&nbsp;</span> <?php echo app('translator')->getFromJson('app.admin.list.dentist.unreadcases'); ?>
            </div>
          </div>
          <div class="row">
            <div class="col-md-12">
              <br>
              <table class="table table-hover">
                <thead>
                  <tr class="table-active">
                    <th scope="col"><?php echo app('translator')->getFromJson('app.admin.list.dentist.id'); ?></th>
                    <th scope="col"><?php echo app('translator')->getFromJson('app.admin.list.dentist.dentist'); ?></th>
                    <th scope="col"><?php echo app('translator')->getFromJson('app.admin.list.dentist.email'); ?></th>
                    <th scope="col"><?php echo app('translator')->getFromJson('app.admin.list.dentist.country'); ?></th>
                    <th scope="col"><?php echo app('translator')->getFromJson('app.admin.list.dentist.cases'); ?></th>
                  </tr>
                </thead>
                <tbody>
                  <?php $__currentLoopData = $dentists; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $dentist): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                  <tr scope="row" data-id="<?php echo e($dentist->id); ?>" data-href="<?php echo e(route('dashboard.list.dentist' , $dentist->id)); ?>">
                    <td class="dentist-show"><?php echo e($dentist->id); ?></td>
                    <td class="dentist-show">
                      <?php if(count($dentist->dentalcases_unread) > 0): ?>
                          <span class="badge badge-pill badge-danger"><?php echo e(count($dentist->dentalcases_unread)); ?></span>
                      <?php endif; ?>
                      <?php echo e($dentist->user->name); ?>

                    </td>
                    <td class="dentist-show"><?php echo e($dentist->user->email); ?></td>
                    <td class="dentist-show"><?php echo e($dentist->country->name); ?></td>
                    <td class="dentist-show"><?php echo e(count($dentist->dentalcases)); ?></td>
                  </tr>
                  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      
    
    
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.admin_template', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php $__env->startPush('scripts'); ?>
<script src="<?php echo e(asset('js/dentalcases.js')); ?>" ></script>
<?php $__env->stopPush(); ?>

<?php $__env->startSection('content'); ?>
    <?php
    //dd(session()->all());
    ?>
    <?php if(count($dentalcases) == 0): ?>
      <div class="py-5 text-center filter-fade-in" style="background-image: url(&quot;<?php echo e(asset(config('constants.options.dentis_create_background'))); ?>&quot;);">
        <div class="container py-5">
          <div class="row">
            <div class="col-md-12">
              <h1 class="display-3 mb-4 text-primary"><?php echo app('translator')->getFromJson('app.deltalcases.welcome'); ?></h1>
              <p class="lead mb-5"><?php echo app('translator')->getFromJson('app.deltalcases.description'); ?></p>
              <a href="<?php echo e(route('dentalcases.create')); ?>" class="btn btn-lg mx-1 btn-secondary"><?php echo app('translator')->getFromJson('app.deltalcases.newcase'); ?></a>
            </div>
          </div>
        </div>
      </div>
    <?php else: ?>
        <div class="container">
          <div class="row">
            <div class="col-md-12">
              <a href="<?php echo e(route('dentalcases.create')); ?>" class="btn btn-lg mx-1 btn-secondary"><?php echo app('translator')->getFromJson('app.deltalcases.newcase'); ?></a>
              &nbsp;
              <span class="badge badge-pill badge-danger">&nbsp;</span> <?php echo app('translator')->getFromJson('app.dentalcase.list.unreadmessages'); ?>
            </div>
          </div>
          <div class="row">
            <div class="col-md-12">
              <br>
              <table class="table table-hover">
                <thead>
                  <tr class="table-active">
                    <th scope="col"><?php echo app('translator')->getFromJson('app.dentalcase.table.id'); ?></th>
                    <th scope="col"><?php echo app('translator')->getFromJson('app.dentalcase.table.name'); ?></th>
                    <th scope="col"><?php echo app('translator')->getFromJson('app.dentalcase.table.date_begin'); ?></th>
                    <th scope="col"><?php echo app('translator')->getFromJson('app.dentalcase.table.dentist'); ?></th>
                    <th scope="col"><?php echo app('translator')->getFromJson('app.dentalcase.table.status'); ?></th>
                  </tr>
                </thead>
                <tbody>
                  <?php $__currentLoopData = $dentalcases; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $dentalcase): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                  <tr scope="row" data-id="<?php echo e($dentalcase->id); ?>">
                    <td class="dentalcase-show"><?php echo e($dentalcase->case_by_dentist); ?></td>
                    <td class="dentalcase-show">
                      <?php if($dentalcase->messages_unread_4_dentist > 0): ?>
                          <span class="badge badge-pill badge-danger">
                            <?php echo e($dentalcase->messages_unread_4_dentist); ?>

                          </span>
                      <?php endif; ?>
                      <?php echo e($dentalcase->patient->name); ?>

                    </td>
                    <td class="dentalcase-show"><?php echo e($dentalcase->created_at); ?></td>
                    <td class="dentalcase-show"><?php echo e($dentalcase->dentist->user->name); ?></td>
                    <td class="dentalcase-show">
                      <?php if($dentalcase->status == 'pending'): ?>
                        <a class="btn btn-danger btn-sm text-white" 
                        
                        > <?php echo app('translator')->getFromJson('app.deltalcases.status.'.$dentalcase->status); ?></a>
                      <?php else: ?>
                        <?php echo app('translator')->getFromJson('app.deltalcases.status.'.$dentalcase->status); ?>
                      <?php endif; ?>
                    </td>
                  </tr>
                  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      
    <?php endif; ?>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.dentist_template', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
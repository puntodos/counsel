<?php $__env->startPush('scripts'); ?>
<script src="<?php echo e(asset('js/dashboard.js')); ?>" ></script>
<?php $__env->stopPush(); ?>

<?php $__env->startSection('content'); ?>

     
<div class="container">
    <div class="row">
        <div class="col-md-12">
          <div class="row">
            <div class="col-md-12 bg-secondary">
              <h1 class="text-center my-1"><?php echo app('translator')->getFromJson('app.dentist.personal_profile_title'); ?></h1>
            </div>
          </div>
          <br>
          <div class="row">
            <div class="col-md-4">
              <?php if($dentist->file_thumb == ""): ?>
                <img src="<?php echo e(asset(config('constants.options.dentist_profile'))); ?>" 
                      class="img-fluid attach_btn_img"
                     >
              <?php else: ?>
                <img src="<?php echo e(asset($user_path . '/' . $dentist->file_thumb)); ?>" class="img-fluid"
                      class="img-fluid attach_btn_img"
                >
              <?php endif; ?>
              
             </div>
            <div class="col-md-4">
              <div class="form-group"> 
                <?php $__env->startComponent('components.app_label', ['field' => 'name']); ?>
                <?php echo $__env->renderComponent(); ?>
                  <?php echo Form::text('name'
                              , $user->name
                              , ['class' => 'form-control'
                              , 'placeholder' => __('app.dentist.name')
                              , 'readonly' => 'readonly'
                              , 'required']); ?>

                </div>
              <div class="form-group"> 
                <?php $__env->startComponent('components.app_label', ['field' => 'cell_phone']); ?>
                <?php echo $__env->renderComponent(); ?>
                <?php echo Form::text('cell_phone'
                              , $dentist->cell_phone
                              , ['class' => 'form-control'
                              , 'placeholder' => __('app.dentist.cell_phone')
                              , 'readonly' => 'readonly'
                              , 'required']); ?>

              </div>
              <div class="form-group"> 
               <?php $__env->startComponent('components.app_label', ['field' => 'country']); ?>
                <?php echo $__env->renderComponent(); ?>
                <?php echo Form::select('country'
                                , $countries
                                , $dentist->country_id
                                , [
                                    'placeholder' => __('app.dentist.country')
                                    ,'class' => 'form-control'
                                    ,'id'=>'name'
                                    , 'required'
                                    , 'readonly' => 'readonly'
                                  ]); ?>

                      

               
              </div>
            </div>
            <div class="col-md-4">
              <div class="form-group"> <label><?php echo app('translator')->getFromJson('app.dentist.email'); ?></label>
                <?php echo Form::email(null
                              , $user->email
                              , ['class' => 'form-control'
                              , 'placeholder' => __('app.dentist.email')
                              , 'readonly' => 'readonly']); ?>

               </div>
              <div class="form-group"> 
                <?php $__env->startComponent('components.app_label', ['field' => 'city']); ?>
                <?php echo $__env->renderComponent(); ?>
                <?php echo Form::text('city'
                              , $dentist->city
                              , ['class' => 'form-control'
                              , 'placeholder' => __('app.dentist.city')
                              , 'required'
                              , 'readonly' => 'readonly'
                              ]); ?>

                 </div>
              <div class="form-group"> <label><?php echo app('translator')->getFromJson('app.dentist.company'); ?></label>
                <?php echo Form::text('company'
                              , $dentist->company
                              , ['class' => 'form-control'
                              , 'placeholder' => __('app.dentist.company')
                              , 'readonly' => 'readonly'
                              ]); ?>

               </div>
            </div>
          </div>
          <br>
          <div class="row">
            <div class="col-md-12 bg-secondary">
              <h1 class="text-center my-1"><?php echo app('translator')->getFromJson('app.dentist.profile_title'); ?></h1>
            </div>
          </div>
          <br>
          <div class="row">
            <div class="col-md-6">
              <div class="form-group"> <label><?php echo app('translator')->getFromJson('app.dentist.document_number'); ?></label>
                <?php echo Form::text('document_number'
                              , $dentist->document_number
                              , ['class' => 'form-control'
                              , 'placeholder' => __('app.dentist.document_number')
                              , 'readonly' => 'readonly'
                              ]); ?>

              </div>
              <div class="form-group"> <label><?php echo app('translator')->getFromJson('app.dentist.associations'); ?></label> 
                <?php echo e(Form::textarea('associations'
                                  ,  $dentist->associations
                                  , ['size' => '4x5'
                                  , 'class' => 'form-control'
                                  , 'readonly' => 'readonly'
                                  , 'placeholder' => __('app.dentist.associations')])); ?>

              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group"> <label><?php echo app('translator')->getFromJson('app.dentist.supports_institution'); ?></label>
                <?php echo Form::text('supports_institution'
                              , $dentist->supports_institution
                              , ['class' => 'form-control'
                              , 'readonly' => 'readonly'
                              , 'placeholder' => __('app.dentist.supports_institution')
                              ]); ?>

              </div>
              <div class="form-group">
                <?php $__env->startComponent('components.app_label', ['field' => 'university']); ?>
                <?php echo $__env->renderComponent(); ?>
                <?php echo Form::text('university'
                              , $dentist->university
                              , ['class' => 'form-control'
                              , 'readonly' => 'readonly'
                              , 'placeholder' => __('app.dentist.university')
                              , 'required'
                              ]); ?>

              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-12">
              <div class="form-group"> <label><?php echo app('translator')->getFromJson('app.dentist.specialities'); ?></label> </div>
            </div>
          </div>
          <div class="row">
            <?php $__currentLoopData = $specialities; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $speciality): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
              <?php
                $selected = false;
                if(in_array($speciality->id, $dentistxSpecialitiesIds)):
                  $selected = true;
                endif;
              ?>
            <div class="col-md-3">
              <div class="form-group">
                <?php echo e(Form::checkbox('specialities[]', $speciality->id , $selected, ['readonly' => 'readonly'])); ?>

                <label><?php echo e($speciality->name); ?></label> </div>
            </div>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
          </div>
          <div class="row">
            <div class="col-md-12">
              <label> <?php echo app('translator')->getFromJson('app.dentist.file_document'); ?></label> <br>
            </div>
            <div class="col-md-12">
              <div class="form-inline">
                
                  <?php if($dentist->file_document != ""): ?>
                  <?php echo Form::button(__('app.dentist.file_document_btn_view')
                                  , ['class' => 'btn btn-primary'
                                  , 'onclick' => 'window.open("'.$user_path . '/' . $dentist->file_document.'")'
                                  ]); ?>

                  <?php else: ?>
                    <b><?php echo app('translator')->getFromJson('app.dentalcase.patient.file_attach_error'); ?></b>
                  <?php endif; ?>
                <?php echo e(Form::file('file_document', ['id' => 'file_document'
                                              , 'style' => 'display:none'
                                              ])); ?>

              
            </div>
                
            </div>
          </div>
          <hr>
      </div>
</div>


        <br>  
        <div class="container">
          <div class="row">
            <div class="col-md-12 bg-secondary">
              <h1 class="text-center my-1"><?php echo app('translator')->getFromJson('app.dentist.cases'); ?></h1>
            </div>
          </div>

          <?php echo $__env->make('dashboard.dentalcases_table', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
          
        </div>
      
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.admin_template', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
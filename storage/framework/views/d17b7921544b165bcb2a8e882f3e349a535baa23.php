<?php $__env->startPush('scripts'); ?>
<script src="<?php echo e(asset('js/dentalcases.js')); ?>" ></script>
<?php $__env->stopPush(); ?>

<?php $__env->startSection('content'); ?>

    <?php echo $__env->make('dentalcases.files_partial', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>


    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class="row">
            <div class="col-md-6 bg-secondary">
              <h3 class="my-1">
                <?php echo app('translator')->getFromJson('app.dentalcase.patient.show.trate'); ?> <?php echo e($patient->name); ?><br>
                ID: <?php echo e($dentalCase->case_by_dentist); ?>

              </h3>
            </div>
             <div class="col-md-2 bg-secondary">
              
            </div>
             <div class="col-md-4 bg-secondary">
                <br>
               
                  <a class="btn btn-warning btn-sm float-sm-right" href="<?php echo e(route('dentalcases.show', $dentalCase->id)); ?>">
                    <?php echo e(__('app.general.back_btn')); ?>

                  </a>

                  <a class="btn btn-dark btn-sm float-sm-right" href="<?php echo e(route('dentist.treatment.pdf', $dentalCase['diagnostic']['id'])); ?>">
                    <?php echo e(__('app.admin.deltalcase.diagnostic.pdf.btn')); ?>

                  </a>
            </div>
          </div>
          <br>
          <div class="row">
            <div class="col-md-3 ">
             <table class="table table-striped table-sm">
              <thead>
                <tr>
                  <th scope="col"><?php echo app('translator')->getFromJson('app.dentalcase.patient.name'); ?></th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td><?php echo e($patient->name); ?></td>
                </tr>
              </tbody>
            </table> 
              <table class="table table-striped table-sm">
                <thead>
                  <tr>
                    <th scope="col"><?php echo app('translator')->getFromJson('app.dentalcase.patient.age'); ?></th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td><?php echo e($patient->age); ?></td>
                  </tr>
                </tbody>
              </table>
              <table class="table table-striped table-sm">
                <thead>
                  <tr>
                    <th scope="col"><?php echo app('translator')->getFromJson('app.dentalcase.patient.genere'); ?></th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td><?php echo app('translator')->getFromJson('app.dentalcase.patient.genere.'.$patient->genere ); ?></td>
                  </tr>
                </tbody>
              </table>   
              <?php if($patient->objective_patient != ""): ?>           
              <table class="table table-striped table-sm">
                <thead>
                  <tr>
                    <th scope="col"><?php echo app('translator')->getFromJson('app.dentalcase.patient.objective_patient'); ?></th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td><?php echo e($patient->objective_patient); ?></td>
                  </tr>
                </tbody>
              </table>
              <?php endif; ?>
              <?php if($patient->objective_dentist != ""): ?>           
              <table class="table table-striped table-sm">
                <thead>
                  <tr>
                    <th scope="col"><?php echo app('translator')->getFromJson('app.dentalcase.patient.objective_dentist'); ?></th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td><?php echo e($patient->objective_dentist); ?></td>
                  </tr>
                </tbody>
              </table>
              <?php endif; ?>
              <a class="btn btn-primary text-white" data-toggle="modal" data-target=".file-list-modal">
              <?php echo e(__('app.dentalcase.patient.show.files')); ?></a>
            </div>
            <div class="col-md-9 ">
              
              <br>
              <div class="row">
                <div class="col-md-12 bg-secondary text-center">
                  <h3 class="my-1">
                    <?php echo app('translator')->getFromJson('app.admin.deltalcase.treatment.title'); ?>
                  </h3>
                </div>            
              </div>
              <br>
              <div class="row">
                <div class="col-md-12 ">
                  
                  <?php echo $__env->make('dentalcases.dentalcase_treatment_fields', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

                </div>
              </div>

            </div>
          </div>
          <br>
          
          
          <hr>
      </div>
    </div>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.dentist_template', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
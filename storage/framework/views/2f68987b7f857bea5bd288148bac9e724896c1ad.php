<h5><b><?php echo e($field['title']); ?></b></h5>

	<?php if(isset($field['description'])): ?>
		<p><?php echo e($field['description']); ?></p>
	<?php endif; ?>

	<?php if(isset($field['description2'])): ?>
		<p><?php echo e($field['description2']); ?></p>
	<?php endif; ?>

<?php $__currentLoopData = $field['values']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
	<?php
		$checked = '';	
		 if(isset($diagnostic[$field['name']])):
		 	foreach($diagnostic[$field['name']] as $element):
		 		if($value['value'] == $element):
		 			$checked = 'checked="checked"';
		 		endif;
		 	endforeach;
		 endif;
	?>
<div class="checkbox">
  <label>
  	<input type="checkbox" name="<?php echo e($field['name']); ?>[]" value="<?php echo e($value['value']); ?>" <?php echo e($checked); ?>>
  	<?php echo e($value['value']); ?> 
  </label>
</div>
<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
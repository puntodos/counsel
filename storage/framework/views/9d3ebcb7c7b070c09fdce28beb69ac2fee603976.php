



<h5><b><?php echo e($field['title']); ?></b></h5>

	<?php if(isset($field['description'])): ?>
		<p><?php echo e($field['description']); ?></p>
	<?php endif; ?>

	<?php if(isset($field['description2'])): ?>
		<p><?php echo e($field['description2']); ?></p>
	<?php endif; ?>


<?php $__currentLoopData = $field['values']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
	<?php
		$checked = false;
		
		 if(isset($diagnostic[$field['name']]) && $value['value'] == $diagnostic[$field['name']]):
		 	$checked = true;
		 endif;
	?>
<div class="radio">
  <label>
  	 <?php echo e(Form::radio($field['name'], $value['value'], $checked, ['class' => ''])); ?>

  	<?php echo e($value['value']); ?> 
  </label>
  <?php if(isset($value['help'])): ?>
  	<i class="fa d-inline fa-lg fa-question-circle">
	  <div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
	  <div class="modal-dialog modal-dialog-centered modal-md">
	    <div class="modal-content">
	      <div class="modal-header">
	        <button type="button" class="close modal_close" data-dismiss="modal">&times;</button>
	      </div>
	      <div class="modal-body">
	        <h6><b><?php echo e($field['title']); ?> > <?php echo e($value['value']); ?></b></h6>
	        <h6><?php echo e($value['help']); ?></h6>
	      </div>
	    </div>
	  </div>
	</div>
	</i>
  <?php endif; ?>
</div>
<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
<!DOCTYPE html>
<html lang="<?php echo e(app()->getLocale()); ?>">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="<?php echo e(csrf_token()); ?>">

    <link rel="shortcut icon" href="<?php echo e(asset(config('constants.options.favico'))); ?>" type="image/x-icon" />
    <title><?php echo e(config('app.name', 'COUNSEL ORTHODONTIST')); ?></title>

    <!-- Scripts -->
    <script src="<?php echo e(asset('js/app.js')); ?>" ></script>
    <script src="<?php echo e(asset('js/popper.min.js')); ?>" ></script>
    <script src="<?php echo e(asset('js/general.js')); ?>" ></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Raleway:300,400,600" rel="stylesheet" type="text/css">

    <!-- Styles -->
    <link href="<?php echo e(asset('css/app.css')); ?>" rel="stylesheet">
</head>
<body>
    <?php $__env->startComponent('components.app_errors_modal'); ?>
    <?php echo $__env->renderComponent(); ?>
	<?php echo $__env->make('layouts.header.dentist_hold', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <br>
    <?php echo $__env->make('flash::message', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <div id="app" style="min-height: 70%">
        <main class="py-4">
            <?php echo $__env->yieldContent('content'); ?>
        </main>
    </div>
    <?php echo $__env->make('layouts.footer.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
</body>
</html>

<?php $__currentLoopData = $diagnostic_json; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $group): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
		<?php
			if($group['type'] == 'break'){
				continue;
			}
			$class = 'col';
			if($group['type'] == 'title'){
				$class = 'col-sm-12';
			}
			$cols = 0;
		?>

		<div class="row">
			<div class="<?php echo e($class); ?>">
				<h3>
					<b>
						<?php echo e($group['title']); ?>

					</b>
				</h3>
			</div>
		</div>
		<?php if(isset($group['fields']) && count($group['fields']) > 0): ?>
			<div class="row">
				<?php
					$cols = 0;
				?>
			<?php $__currentLoopData = $group['fields']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $field): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
				<?php
					if($field['type'] == 'break'){
						continue;
					}
					$class = 'col';
					if(isset($field['width'])){
						$class = 'col-sm-'. $field['width'];
						$cols += $field['width'];
					}
				?>
				<div class="col-sm-6">
					<?php if($field['type'] == 'radio'): ?>
						<?php $__env->startComponent('components.app_diagnostic_radios_read', ['field' => $field, 'diagnostic' => $diagnostic]); ?>
						<?php echo $__env->renderComponent(); ?>
					<?php endif; ?>
					<?php if($field['type'] == 'number'): ?>
						<?php
						$value = '';
						if(isset($diagnostic[$field['name']])){
							$value = $diagnostic[$field['name']];
						}
						?>
						<h5><b><?php echo e($field['title']); ?></b></h5>
						<div class="form-group"> 
				            <label style=""><?php echo e($value); ?></label>
				        </div>
					<?php endif; ?>
					<?php if($field['type'] == 'check'): ?>
						<h5><b><?php echo e($field['title']); ?></b></h5>
						<?php
						$value = 'NA';
						if(isset($diagnostic[$field['name']])){
							$value = $diagnostic[$field['name']];
							$value = '<ul><li>'.implode("</li><li>", $value).'</li></ul>';
						}
						?>
						
						<?php
							echo $value;
						?>
					<?php endif; ?>

					<?php if($field['type'] == 'select'): ?>
						<h5><b><?php echo e($field['title']); ?></b></h5>
						<?php
	            			$value = 'NA';
	            			if(isset($diagnostic[$field['name']])):
	            				$value = $diagnostic[$field['name']];
	            			endif;
	            		?>
	            		<label>
							<?php echo e($value); ?>

						</label>
					<?php endif; ?>

					<?php if(isset($field['fields']) && count($field['fields']) > 0): ?>
						<?php $__currentLoopData = $field['fields']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $intField): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
							<?php if($intField['type'] == 'text'): ?>
								<?php
								$value = '';
								if(isset($diagnostic[$intField['name']])){
									$value = $diagnostic[$intField['name']];
								}
								?>
								<br>
								<label><b><?php echo e($intField['title']); ?>:</b>&nbsp;
									<?php echo e($value); ?>

								</label>
							<?php endif; ?>
						<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
					<?php endif; ?>
				</div>
				<?php if($cols == 12): ?>
					<?php	
						$cols = 0;
					?>
					</div>
					<hr>
					<div class="row">
				<?php endif; ?>
			<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
			</div>
		<?php endif; ?>
		<?php if($cols > 0): ?>
		<hr>
		<?php endif; ?>
	<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

    
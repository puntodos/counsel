
<?php

	if(!isset($namespace)):
		$namespace = '';
	endif;
	$style = '';
	$text = '';
	$error = false;
	if ($errors->has($field)):
		$error = true;
		$style = 'color:red';
		$text = ucfirst($errors->first($field));
	endif;

?>
<?php if(!$error): ?>	
	<?php if($namespace == ''): ?>
	<label style="<?php echo e($style); ?>"><?php echo app('translator')->getFromJson('app.dentist.'.$field); ?></label>
	<?php else: ?>
	<label style="<?php echo e($style); ?>"><?php echo app('translator')->getFromJson($namespace.'.'.$field); ?></label>
	<?php endif; ?>
<?php else: ?>
	<label style="<?php echo e($style); ?>"><?php echo e($text); ?></label>
<?php endif; ?>

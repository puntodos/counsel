<!DOCTYPE html>
<html lang="<?php echo e(app()->getLocale()); ?>">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">


    <!-- CSRF Token -->
    <meta name="csrf-token" content="<?php echo e(csrf_token()); ?>">

    <link rel="shortcut icon" type="image/png" href="<?php echo e(asset(config('constants.options.favico'))); ?>"/>
    <title><?php echo e(config('app.name', 'COUNSEL ORTHODONTIST')); ?></title>

    <!-- Scripts -->
    <script src="<?php echo e(asset('js/app.js')); ?>" ></script>
    <script src="<?php echo e(asset('js/popper.min.js')); ?>" ></script>
    <script src="<?php echo e(asset('js/general.js')); ?>" ></script>

    <?php echo $__env->yieldPushContent('scripts'); ?>
    <!-- Fonts -->
    <link rel="dns-prefetch" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Raleway:300,400,600" rel="stylesheet" type="text/css">

    <!-- Styles -->
    <link href="<?php echo e(asset('css/app.css')); ?>" rel="stylesheet">
    <link href="<?php echo e(asset('css/font-awesome.min.css')); ?>" rel="stylesheet">


    <style>
    #loading {
        background: #ffffff;
        color: #666666;
        position: fixed;
        height: 100%;
        width: 100%;
        z-index: 100000;
        top: 0;
        left: 0;
        float: left;
        text-align: center;
        padding-top: 25%;
        display: none;
    }
    .loading_img{
        height: 35%;
    }
</style>
    
</head>
<body>

    <div id="loading">
        <div class="container">
            <div class="row">
                <div class="col text-center">
                    <?php echo $__env->yieldContent('loading_text'); ?>
                </div>
            </div>
        </div>
        <img class="loading_img" src="<?php echo e(asset(config('constants.options.loading'))); ?>" alt="Loading" /><br/>
    </div>
    <?php $__env->startComponent('components.app_errors_modal'); ?>
    <?php echo $__env->renderComponent(); ?>
	<?php echo $__env->make('layouts.header.dentist', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <?php if(isset(session()->all()['flash_notification'])): ?>
        <div class="container py-2">
            <div class="row">
                <div class="col-md-12">
                    <?php echo $__env->make('flash::message', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                </div>
            </div>
        </div>
        <script>
        $('div.alert').not('.alert-important').delay(3000).fadeOut(100 , 0);
        </script>
    <?php endif; ?>
    <div id="app" style="min-height: 70%">
        <main class="py-2">
            <?php echo $__env->yieldContent('content'); ?>
        </main>
    </div>
    <?php echo $__env->make('layouts.footer.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
</body>
</html>

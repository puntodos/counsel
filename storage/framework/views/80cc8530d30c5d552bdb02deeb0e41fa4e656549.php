
<div class="modal fade" role="dialog" id="errors">
    <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title"><b style="color:red;"><?php echo app('translator')->getFromJson('app.general.errors'); ?></b></h5>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <div class="modal-body" style="color:red;">
          <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
             - <?php echo e($error); ?><br>
          <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
          <h6><b></b></h6>
          <h6></h6>
        </div>
      </div>
    </div>
  </div>
<?php
  if(count($errors->all()) > 0):
?>
  <script>
    $(document).ready(function(){
        //$('.modal').modal();
        $('#errors').modal();
    }) 
  </script>
<?php
  endif;
?>
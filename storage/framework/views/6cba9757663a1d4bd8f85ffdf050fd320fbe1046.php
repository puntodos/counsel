<?php echo Form::open(['route' => ['diagnostic.store', $dentalCase], 'method' => 'POST', 'files' => true]); ?>

<?php echo csrf_field(); ?>
<div class="row">
	<div class="col">
		<h3>
			<b>
				<?php echo app('translator')->getFromJson('app.admin.deltalcase.diagnostic.frontal_photo'); ?>
			</b>
		</h3>
	</div>
</div>
<div class="row">
	<div class="col">
		<?php $__env->startComponent('components.app_diagnostic_radios', ['fields' => $fields, 'field' => 'facial_morphology', 'diagnostic' => $diagnostic]); ?>
		<?php echo $__env->renderComponent(); ?>
	</div>
	<div class="col">
		<?php $__env->startComponent('components.app_diagnostic_radios', ['fields' => $fields, 'field' => 'type_of_smile', 'diagnostic' => $diagnostic]); ?>
		<?php echo $__env->renderComponent(); ?>
	</div>
	
</div>
<hr>
<div class="row">
	<div class="col">
		<?php $__env->startComponent('components.app_diagnostic_radios', ['fields' => $fields, 'field' => 'fifth_facials', 'diagnostic' => $diagnostic]); ?>
		<?php echo $__env->renderComponent(); ?>
	</div>
	<div class="col">
		<?php $__env->startComponent('components.app_diagnostic_radios', ['fields' => $fields, 'field' => 'thirds_facials', 'diagnostic' => $diagnostic]); ?>
		<?php echo $__env->renderComponent(); ?>
	</div>
</div>
<hr>
<div class="row">
	<div class="col">
		<h3>
			<b>
				<?php echo app('translator')->getFromJson('app.admin.deltalcase.diagnostic.lateral_view'); ?>
			</b>
		</h3>
	</div>
</div>
<hr>
<div class="row">
	<div class="col">
		<?php $__env->startComponent('components.app_diagnostic_radios', ['fields' => $fields, 'field' => 'facial_type', 'diagnostic' => $diagnostic]); ?>
		<?php echo $__env->renderComponent(); ?>
	</div>
	<div class="col">
		<?php $__env->startComponent('components.app_diagnostic_radios', ['fields' => $fields, 'field' => 'angle_nasolabial', 'diagnostic' => $diagnostic]); ?>
		<?php echo $__env->renderComponent(); ?>
	</div>
</div>
<hr>
<div class="row">
	<div class="col">
		<?php $__env->startComponent('components.app_diagnostic_radios', ['fields' => $fields, 'field' => 'angle_mentolabial', 'diagnostic' => $diagnostic]); ?>
		<?php echo $__env->renderComponent(); ?>
	</div>
	<div class="col">
		<?php $__env->startComponent('components.app_diagnostic_radios', ['fields' => $fields, 'field' => 'upper_lip', 'diagnostic' => $diagnostic]); ?>
		<?php echo $__env->renderComponent(); ?>
	</div>
</div>
<hr>
<div class="row">
	<div class="col">
		<?php $__env->startComponent('components.app_diagnostic_radios', ['fields' => $fields, 'field' => 'lower_lip', 'diagnostic' => $diagnostic]); ?>
		<?php echo $__env->renderComponent(); ?>
	</div>
	<div class="col">
		<?php $__env->startComponent('components.app_diagnostic_radios', ['fields' => $fields, 'field' => 'lip_competition', 'diagnostic' => $diagnostic]); ?>
		<?php echo $__env->renderComponent(); ?>
	</div>
</div>
<hr>
<div class="row">
	<div class="col">
		<?php $__env->startComponent('components.app_diagnostic_radios', ['fields' => $fields, 'field' => 'chin', 'diagnostic' => $diagnostic]); ?>
		<?php echo $__env->renderComponent(); ?>
	</div>
	<div class="col">
		
	</div>
</div>
<hr>
<div class="row">
	<div class="col">
		<h3>
			<b>
				<?php echo app('translator')->getFromJson('app.admin.deltalcase.diagnostic.intraoral_analysis'); ?>
			</b>
		</h3>
	</div>
</div>
<hr>
<div class="row">
	<div class="col">
		<?php $__env->startComponent('components.app_diagnostic_radios', ['fields' => $fields, 'field' => 'relationship_canine_right', 'diagnostic' => $diagnostic]); ?>
		<?php echo $__env->renderComponent(); ?>
	</div>
	<div class="col">
		<?php $__env->startComponent('components.app_diagnostic_radios', ['fields' => $fields, 'field' => 'relationship_canine_left', 'diagnostic' => $diagnostic]); ?>
		<?php echo $__env->renderComponent(); ?>
	</div>
</div>
<hr>
<div class="row">
	<div class="col">
		<?php $__env->startComponent('components.app_diagnostic_radios', ['fields' => $fields, 'field' => 'molar_relationship_right', 'diagnostic' => $diagnostic]); ?>
		<?php echo $__env->renderComponent(); ?>
	</div>
	<div class="col">
		<h5><b><?php echo app('translator')->getFromJson('app.admin.deltalcase.diagnostic.overjet'); ?></b></h5>
		<p><?php echo app('translator')->getFromJson('app.admin.deltalcase.diagnostic.overjet.description'); ?></p>
		<div class="form-group"> 
            <label style=""><?php echo app('translator')->getFromJson('app.admin.deltalcase.diagnostic.overjet.description2'); ?></label>
            <input class="form-control" placeholder="<?php echo app('translator')->getFromJson('app.admin.deltalcase.diagnostic.overjet.description2'); ?>" required="" name="overjet" type="number" step="0.1" value="<?php echo e($diagnostic['overjet']); ?>">
        </div>
			
	</div>
</div>
<hr>
<div class="row">
	<div class="col">
		<?php $__env->startComponent('components.app_diagnostic_radios', ['fields' => $fields, 'field' => 'molar_relationship_left', 'diagnostic' => $diagnostic]); ?>
		<?php echo $__env->renderComponent(); ?>
	</div>
	<div class="col">
		<h5><b><?php echo app('translator')->getFromJson('app.admin.deltalcase.diagnostic.overbite'); ?></b></h5>
		<p><?php echo app('translator')->getFromJson('app.admin.deltalcase.diagnostic.overbite.description'); ?></p>
		<div class="form-group"> 
            <label style=""><?php echo app('translator')->getFromJson('app.admin.deltalcase.diagnostic.overbite.description2'); ?></label>
            <input class="form-control" placeholder="<?php echo app('translator')->getFromJson('app.admin.deltalcase.diagnostic.overbite.description2'); ?>" required="" name="overbite" type="number" step="0.1" value="<?php echo e($diagnostic['overbite']); ?>">
        </div>
	</div>
</div>
<hr>
<div class="row">
	<div class="col">
		<?php $__env->startComponent('components.app_diagnostic_radios', ['fields' => $fields, 'field' => 'cross_bite', 'diagnostic' => $diagnostic]); ?>
		<?php echo $__env->renderComponent(); ?>
	</div>
	<div class="col">
		<?php $__env->startComponent('components.app_diagnostic_radios', ['fields' => $fields, 'field' => 'cross_bite_type', 'diagnostic' => $diagnostic]); ?>
		<?php echo $__env->renderComponent(); ?>
	</div>
</div>
<hr>
<div class="row">
	<div class="col">
		<?php $__env->startComponent('components.app_diagnostic_radios', ['fields' => $fields, 'field' => 'match_between_mid_line_up_w_mid_line_facial', 'diagnostic' => $diagnostic]); ?>
		<?php echo $__env->renderComponent(); ?>
	</div>
	<div class="col">
		<?php $__env->startComponent('components.app_diagnostic_radios', ['fields' => $fields, 'field' => 'match_between_mid_line_dw_w_mid_line_up', 'diagnostic' => $diagnostic]); ?>
		<?php echo $__env->renderComponent(); ?>
	</div>
</div>
<hr>
<div class="row">
	<div class="col">
		<?php $__env->startComponent('components.app_diagnostic_radios', ['fields' => $fields, 'field' => 'form_upper_arch', 'diagnostic' => $diagnostic]); ?>
		<?php echo $__env->renderComponent(); ?>
	</div>
	<div class="col">
		<?php $__env->startComponent('components.app_diagnostic_radios', ['fields' => $fields, 'field' => 'symmetry_upper_arch', 'diagnostic' => $diagnostic]); ?>
		<?php echo $__env->renderComponent(); ?>
	</div>
</div>
<hr>
<div class="row">
	<div class="col">
		<?php $__env->startComponent('components.app_diagnostic_radios', ['fields' => $fields, 'field' => 'shape_lower_arch', 'diagnostic' => $diagnostic]); ?>
		<?php echo $__env->renderComponent(); ?>
	</div>
	<div class="col">
		<?php $__env->startComponent('components.app_diagnostic_radios', ['fields' => $fields, 'field' => 'symmetry_down_arch', 'diagnostic' => $diagnostic]); ?>
		<?php echo $__env->renderComponent(); ?>
	</div>
</div>
<hr>
<div class="row">
	<div class="col">
		<?php $__env->startComponent('components.app_diagnostic_radios', ['fields' => $fields, 'field' => 'top_crowding', 'diagnostic' => $diagnostic]); ?>
		<?php echo $__env->renderComponent(); ?>
	</div>
	<div class="col">
		<?php $__env->startComponent('components.app_diagnostic_radios', ['fields' => $fields, 'field' => 'top_crowding_type', 'diagnostic' => $diagnostic]); ?>
		<?php echo $__env->renderComponent(); ?>
	</div>
</div>
<hr>
<div class="row">
	<div class="col">
		<?php $__env->startComponent('components.app_diagnostic_radios', ['fields' => $fields, 'field' => 'lower_crowding', 'diagnostic' => $diagnostic]); ?>
		<?php echo $__env->renderComponent(); ?>
	</div>
	<div class="col">
		<?php $__env->startComponent('components.app_diagnostic_radios', ['fields' => $fields, 'field' => 'lower_crowding_type', 'diagnostic' => $diagnostic]); ?>
		<?php echo $__env->renderComponent(); ?>
	</div>
</div>
<hr>
<div class="row">
	<div class="col">
		<?php $__env->startComponent('components.app_diagnostic_radios', ['fields' => $fields, 'field' => 'top_diastema', 'diagnostic' => $diagnostic]); ?>
		<?php echo $__env->renderComponent(); ?>
	</div>
	<div class="col">
		<?php $__env->startComponent('components.app_diagnostic_radios', ['fields' => $fields, 'field' => 'lower_diastema', 'diagnostic' => $diagnostic]); ?>
		<?php echo $__env->renderComponent(); ?>
	</div>
</div>
<hr>
<div class="row">
	<div class="col">
		<div class="form-group"> 
			<label>
				<h5><?php echo app('translator')->getFromJson('app.admin.deltalcase.diagnostic.facial_diagnostic'); ?></h5>
			</label> 
			<p><?php echo app('translator')->getFromJson('app.admin.deltalcase.diagnostic.facial_diagnostic.description'); ?></p>
            <textarea class="form-control" placeholder="<?php echo app('translator')->getFromJson('app.admin.deltalcase.diagnostic.facial_diagnostic.description'); ?>" name="facial_diagnostic" cols="4" rows="5"><?php echo e($diagnostic['facial_diagnostic']); ?></textarea>
        </div>
	</div>
</div>
<hr>
<div class="row">
	<div class="col">
		<div class="form-group"> 
			<label>
				<h5><?php echo app('translator')->getFromJson('app.admin.deltalcase.diagnostic.esqueletic_diagnostic'); ?></h5>
			</label> 
			<p><?php echo app('translator')->getFromJson('app.admin.deltalcase.diagnostic.esqueletic_diagnostic.description'); ?></p>
            <textarea class="form-control" placeholder="<?php echo app('translator')->getFromJson('app.admin.deltalcase.diagnostic.esqueletic_diagnostic.description'); ?>" name="esqueletic_diagnostic" cols="4" rows="5" ><?php echo e($diagnostic['esqueletic_diagnostic']); ?></textarea>
        </div>
	</div>
</div>
<hr>
<div class="row">
	<div class="col">
		<div class="form-group"> 
			<label>
				<h5><?php echo app('translator')->getFromJson('app.admin.deltalcase.diagnostic.oclusal_diagnostic'); ?></h5>
			</label> 
			<p><?php echo app('translator')->getFromJson('app.admin.deltalcase.diagnostic.oclusal_diagnostic.description'); ?></p>
            <textarea class="form-control" placeholder="<?php echo app('translator')->getFromJson('app.admin.deltalcase.diagnostic.oclusal_diagnostic.description'); ?>" name="oclusal_diagnostic" cols="4" rows="5"><?php echo e($diagnostic['oclusal_diagnostic']); ?></textarea>
        </div>
	</div>
</div>
<div class="container">
  	<div class="row">
        <div class="col-md-12 text-center">
          <div class="justify-content-center">
            <br>
            <?php echo e(Form::submit(__('app.admin.deltalcase.diagnostic.save_btn'),['class' => 'btn btn-primary'])); ?>

          </div>
        </div>
    </div>
 </div>
    <?php echo Form::close();; ?>
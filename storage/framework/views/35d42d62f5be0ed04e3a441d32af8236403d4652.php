<?php $__env->startPush('scripts'); ?>
<script src="<?php echo e(asset('js/dentalcases.js')); ?>" ></script>
<?php $__env->stopPush(); ?>

<?php $__env->startSection('content'); ?>

    <?php echo $__env->make('dentalcases.files_partial', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>


    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class="row">
            <div class="col-md-6 bg-secondary">
              <h3 class="my-1">
                <?php echo app('translator')->getFromJson('app.dentalcase.patient.show.trate'); ?> <?php echo e($patient->name); ?><br>
                ID: <?php echo e($dentalCase->case_by_dentist); ?>

              </h3>
            </div>
             <div class="col-md-6 bg-secondary">
                <br>
                <a class="btn btn-danger text-white btn-sm float-sm-right" style="margin: 1px" href="<?php echo e(route('dentalcases.edit', $dentalCase->id)); ?>">
                <?php echo e(__('app.dentalcase.patient.show.edit_case')); ?></a>
                <?php
                /*<a class="btn btn-primary text-white">
                {{ __('app.dentalcase.patient.show.end_case') }}</a>*/
                ?>
                
                <?php if($diagnostic != null && $diagnostic['snplan'] == "S"): ?>
                <a class="btn btn-primary text-white btn-sm float-sm-right" style="margin: 1px" href="<?php echo e(route('treatment.show_read', $dentalCase->id)); ?>">
                    <?php echo e(__('app.admin.deltalcases.treatment.view_btn')); ?>

                  </a>
                <?php endif; ?>
                <?php if($diagnostic != null && $diagnostic['sndiagnostic'] == "S"): ?>
                <a class="btn btn-primary text-white btn-sm float-sm-right" style="margin: 1px" href="<?php echo e(route('diagnostic.show_read', $dentalCase->id)); ?>">
                    <?php echo e(__('app.admin.deltalcases.diagnostic.view_btn')); ?>

                  </a>
                <?php endif; ?>
                <a class="btn btn-warning btn-sm float-sm-right" style="margin: 1px" href="<?php echo e(route('dentalcases.index')); ?>">
                    <?php echo e(__('app.general.back_btn')); ?></a>
            </div>
          </div>
          <br>
          <div class="row">
            <div class="col-md-4 ">
             <table class="table table-striped table-sm">
              <thead>
                <tr>
                  <th scope="col"><?php echo app('translator')->getFromJson('app.dentalcase.patient.name'); ?></th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td><?php echo e($patient->name); ?></td>
                </tr>
              </tbody>
            </table> 
              <table class="table table-striped table-sm">
                <thead>
                  <tr>
                    <th scope="col"><?php echo app('translator')->getFromJson('app.dentalcase.patient.age'); ?></th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td><?php echo e($patient->age); ?></td>
                  </tr>
                </tbody>
              </table>
              <table class="table table-striped table-sm">
                <thead>
                  <tr>
                    <th scope="col"><?php echo app('translator')->getFromJson('app.dentalcase.patient.genere'); ?></th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td><?php echo app('translator')->getFromJson('app.dentalcase.patient.genere.'.$patient->genere ); ?></td>
                  </tr>
                </tbody>
              </table>   
              <?php if($patient->objective_patient != ""): ?>           
              <table class="table table-striped table-sm">
                <thead>
                  <tr>
                    <th scope="col"><?php echo app('translator')->getFromJson('app.dentalcase.patient.objective_patient'); ?></th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td><?php echo e($patient->objective_patient); ?></td>
                  </tr>
                </tbody>
              </table>
              <?php endif; ?>
              <?php if($patient->objective_dentist != ""): ?>           
              <table class="table table-striped table-sm">
                <thead>
                  <tr>
                    <th scope="col"><?php echo app('translator')->getFromJson('app.dentalcase.patient.objective_dentist'); ?></th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td><?php echo e($patient->objective_dentist); ?></td>
                  </tr>
                </tbody>
              </table>
              <?php endif; ?>
              <?php if($dentalCase->begin_date != ""): ?>           
              <table class="table table-striped table-sm">
                <thead>
                  <tr>
                    <th scope="col"><?php echo app('translator')->getFromJson('app.dentalcase.patient.begin_date'); ?></th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td><?php echo e($dentalCase->begin_date); ?></td>
                  </tr>
                </tbody>
              </table>
              <table class="table table-striped table-sm">
                <thead>
                  <tr>
                    <th scope="col"><?php echo app('translator')->getFromJson('app.dentalcase.patient.end_date'); ?></th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td><?php echo e($dentalCase->ended_date); ?></td>
                  </tr>
                </tbody>
              </table>
              <?php endif; ?>
              <?php if($dentalCase->plan != ""): ?>           
              <table class="table table-striped table-sm">
                <thead>
                  <tr>
                    <th scope="col"><?php echo app('translator')->getFromJson('app.dentalcase.payment.plan.title'); ?></th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td><?php echo app('translator')->getFromJson('app.dentalcase.payment.'.$dentalCase->plan); ?></td>
                  </tr>
                </tbody>
              </table>
              <?php endif; ?>
              <a class="btn btn-primary text-white" data-toggle="modal" data-target=".file-list-modal">
              <?php echo e(__('app.dentalcase.patient.show.files')); ?></a>
            </div>
            <div class="col-md-8 ">
              
              <?php echo $__env->make('dentalcases.message_partial', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

            </div>
          </div>
          <br>
          
          
          <hr>
      </div>
    </div>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.dentist_template', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
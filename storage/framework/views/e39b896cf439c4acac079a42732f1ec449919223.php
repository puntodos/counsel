<div class="modal fade file-list-modal" role="dialog">
      <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <h3 class="modal-title"><b><?php echo app('translator')->getFromJson('app.dentalcase.patient.show.files.title'); ?></b></h3>
            <button type="button" class="close" data-dismiss="modal">&times;</button>
          </div>
          <div class="modal-body">
            <div class="container">
              <div class="row">
                <div class="col-md-12 bg-secondary">
                  <h3 class="text-center my-1"><?php echo app('translator')->getFromJson('app.dentalcase.patient.diagnosticimages'); ?></h3>
                </div>
              </div>

              <br>
              <div class="row">
                <div class="col-md-6">
                    <?php $__env->startComponent('components.app_file_preview', ['field' => 'radiography1', 'user_path' => $user_path.'/', 'file' => $patient['radiography1']]); ?>
                    <?php echo $__env->renderComponent(); ?>
                </div>
                <div class="col-md-6">
                    <?php $__env->startComponent('components.app_file_preview', ['field' => 'radiography2', 'user_path' => $user_path.'/', 'file' => $patient['radiography2']]); ?>
                    <?php echo $__env->renderComponent(); ?>
                </div>
              </div>
              <br>
              <div class="row">
                <div class="col-md-6">
                    <?php $__env->startComponent('components.app_file_preview', ['field' => 'radiography3', 'user_path' => $user_path.'/', 'file' => $patient['radiography3']]); ?>
                    <?php echo $__env->renderComponent(); ?>
                </div>
                <div class="col-md-6">
                    <?php $__env->startComponent('components.app_file_preview', ['field' => 'radiography4', 'user_path' => $user_path.'/', 'file' => $patient['radiography4']]); ?>
                    <?php echo $__env->renderComponent(); ?>
                </div>
              </div>
              <br>
              <div class="row">
                <div class="col-md-6">
                    <?php $__env->startComponent('components.app_file_preview', ['field' => 'radiography5', 'user_path' => $user_path.'/', 'file' => $patient['radiography5']]); ?>
                    <?php echo $__env->renderComponent(); ?>
                </div>
                <div class="col-md-6">
                  
                </div>
              </div>
              <br>
              <div class="row">
                <div class="col-md-12 bg-secondary">
                  <h3 class="text-center my-1"><?php echo app('translator')->getFromJson('app.dentalcase.patient.diagnosticimages2'); ?></h3>
                </div>
              </div>
              <br>
              <div class="row">
                <div class="col-md-6">
                    <?php $__env->startComponent('components.app_file_preview', ['field' => 'radiography6', 'user_path' => $user_path.'/', 'file' => $patient['radiography6']]); ?>
                    <?php echo $__env->renderComponent(); ?>
                </div>
                <div class="col-md-6">
                    <?php $__env->startComponent('components.app_file_preview', ['field' => 'radiography7', 'user_path' => $user_path.'/', 'file' => $patient['radiography7']]); ?>
                    <?php echo $__env->renderComponent(); ?>
                </div>
              </div>
              <br>
              <div class="row">
                <div class="col-md-6">
                    <?php $__env->startComponent('components.app_file_preview', ['field' => 'radiography8', 'user_path' => $user_path.'/', 'file' => $patient['radiography8']]); ?>
                    <?php echo $__env->renderComponent(); ?>
                </div>
                <div class="col-md-6">
                  
                </div>
              </div>
              <br>
              <div class="row">
                <div class="col-md-12 bg-secondary">
                  <h3 class="text-center my-1"><?php echo app('translator')->getFromJson('app.dentalcase.patient.diagnosticimages3'); ?></h3>
                </div>
              </div>
              <br>
              <div class="row">
                <div class="col-md-6">
                  <?php $__env->startComponent('components.app_file_preview', ['field' => 'radiography9', 'user_path' => $user_path.'/', 'file' => $patient['radiography9']]); ?>
                    <?php echo $__env->renderComponent(); ?>
                </div>
                <div class="col-md-6">
                  <?php $__env->startComponent('components.app_file_preview', ['field' => 'radiography12', 'user_path' => $user_path.'/', 'file' => $patient['radiography12']]); ?>
                    <?php echo $__env->renderComponent(); ?>
                </div>
              </div>
              <br>
              <div class="row">
                <div class="col-md-6">
                  <?php $__env->startComponent('components.app_file_preview', ['field' => 'radiography10', 'user_path' => $user_path.'/', 'file' => $patient['radiography10']]); ?>
                    <?php echo $__env->renderComponent(); ?>
                  
                </div>
                <div class="col-md-6">
                  <?php $__env->startComponent('components.app_file_preview', ['field' => 'radiography11', 'user_path' => $user_path.'/', 'file' => $patient['radiography11']]); ?>
                    <?php echo $__env->renderComponent(); ?>
                  
                </div>
              </div>
              <br>
              <div class="row">
                <div class="col-md-6">
                  <?php $__env->startComponent('components.app_file_preview', ['field' => 'radiography13', 'user_path' => $user_path.'/', 'file' => $patient['radiography13']]); ?>
                    <?php echo $__env->renderComponent(); ?>
                </div>
                <div class="col-md-6">
                  
                </div>
              </div>
              <br>
              
            </div>
            
          </div>
        </div>
      </div>
    </div>
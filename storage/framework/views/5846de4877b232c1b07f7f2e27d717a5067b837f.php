<?php $__env->startPush('scripts'); ?>
<script src="<?php echo e(asset('js/dentalcases.js').'?time='.date('YmdHis')); ?>" ></script>
<?php $__env->stopPush(); ?>

<?php $__env->startSection('content'); ?>

    <?php echo $__env->make('dentalcases.files_partial', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>


    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class="row">
            <div class="col-md-6 bg-secondary">
              <h3 class="my-1">
                <?php echo app('translator')->getFromJson('app.dentalcase.patient.show.pending_payment.trate'); ?> <?php echo e($patient->name); ?>

              </h3>
            </div>
             <div class="col-md-2 bg-secondary">
              
            </div>
             <div class="col-md-4 bg-secondary">
               
            </div>
          </div>
          <br>
          <div class="row">
            <div class="p-5 text-center bg-secondary col-md-12">
              <div class="container">
                <div class="row">
                  <div class="col-md-2">
                  </div>
                  <div class="col-md-4">
                    <div class="card p-4 m-0">
                      <div class="card-block">
                        <p class="lead"><i><h3><b><?php echo app('translator')->getFromJson('app.dentalcase.payment.plan1.title'); ?></b></h3></i></p>
                      </div>
                      <div class="card-block my-3">
                        <h1><b>$<?php echo app('translator')->getFromJson('app.dentalcase.payment.plan1.price'); ?></b></h1>
                        <hr>
                        <p><?php echo app('translator')->getFromJson('app.dentalcase.payment.plan1.desc'); ?></p>
                        <hr>
                      </div>
                      <div class="card-block">
                        <input type="checkbox" class="check_1">
                        <a id="plan1" data-toggle="modal" data-target="#tyc_payment">
                          <?php echo app('translator')->getFromJson('app.dentalcase.payment.plan1.terms'); ?>
                        </a>
                        <br>
                        <a class="btn btn-secondary disabled plan1" style="color:white"><?php echo app('translator')->getFromJson('app.dentalcase.payment.plan1.cta'); ?></a>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-4">
                    <div class="card p-4 m-0">
                      <div class="card-block">
                        <p class="lead"><i><h3><b><?php echo app('translator')->getFromJson('app.dentalcase.payment.plan2.title'); ?></b></h3></i></p>
                      </div>
                      <div class="card-block my-3">
                        <h1><b>$<?php echo app('translator')->getFromJson('app.dentalcase.payment.plan2.price'); ?></b></h1>
                        <hr>
                        <p><?php echo app('translator')->getFromJson('app.dentalcase.payment.plan2.desc'); ?></p>
                        <hr>
                      </div>
                      <div class="card-block">
                        <input type="checkbox" class="check_2">
                        <a id="plan2" data-toggle="modal" data-target="#tyc_payment">
                          <?php echo app('translator')->getFromJson('app.dentalcase.payment.plan2.terms'); ?>
                        </a>
                        <br>
                        <a class="btn btn-secondary disabled plan2" style="color:white"><?php echo app('translator')->getFromJson('app.dentalcase.payment.plan2.cta'); ?></a>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-2">
                  </div>
                  
                </div>
              </div>
            </div>
          </div>
          <br>
          
          
          <hr>
      </div>
    </div>
    </div>

<div class="modal fade" id="tyc_payment" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalCenterTitle">
          <?php echo app('translator')->getFromJson('app.dentalcase.payment.tyc.title'); ?>
        </h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <?php echo app('translator')->getFromJson('app.dentalcase.payment.tyc.description'); ?>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">
          <?php echo app('translator')->getFromJson('app.general.tyc.close'); ?>
        </button>
      </div>
    </div>
  </div>
</div>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.dentist_template', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
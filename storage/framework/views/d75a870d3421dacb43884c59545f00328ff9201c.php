<label>
  <h5>
    <b> <?php echo app('translator')->getFromJson('app.dentalcase.patient.'.$field); ?></b>
  </h5>
</label> 

<br>
<div class="form-inline">
  <?php echo Form::text(null
                , ''
                , ['class' => 'form-control'
                , 'placeholder' => __('app.dentalcase.patient.'.$field)
                , 'id' => 'attach_'.$field.'_resume'
                , 'style' => 'width:300px'
                , 'readonly' => 'readonly']); ?>

   <?php echo Form::button(__('app.dentalcase.patient.add_file')
                    , ['class' => 'btn btn-secondary attach_btn'
                    , 'data-id' => 'attach_'.$field.'_resume'
                    , 'data-id_file' => 'file_'.$field
                    ]); ?>  &nbsp;
  <?php echo e(Form::file($field, ['id' => 'file_'.$field
                                , 'style' => 'display:none'
                                ])); ?>


</div>
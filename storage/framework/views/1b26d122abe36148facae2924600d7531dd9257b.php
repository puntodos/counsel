<br>  
          <div class="row">
            <div class="col-md-12">
              <br>
              <table class="table table-hover">
                <thead>
                  <tr class="table-active">
                    <th scope="col"><?php echo app('translator')->getFromJson('app.admin.list.users.id'); ?></th>
                    <th scope="col"><?php echo app('translator')->getFromJson('app.admin.list.users.name'); ?></th>
                    <th scope="col"><?php echo app('translator')->getFromJson('app.admin.list.users.email'); ?></th>
                    <th scope="col"><?php echo app('translator')->getFromJson('app.admin.list.users.action'); ?></th>
                  </tr>
                </thead>
                <tbody>
                  <?php $__currentLoopData = $users; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $user): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                  <tr scope="row" data-id="<?php echo e($user->id); ?>" 
                    data-href='<?php echo e(route('dashboards.users.edit', $user->id)); ?>'>
                    <td class="dentist-show"><?php echo e($user->id); ?></td>
                    <td class="dentist-show"> 
                      <?php echo e($user->name); ?>

                    </td>
                    <td class="dentist-show"><?php echo e($user->email); ?></td>
                    <td class="dentist-show">
                      <button type="button" class="btn btn-default btn-sm btn-success">
                        <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span> Editar
                      </button>
                    </td>
                  </tr>
                  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </tbody>
              </table>
            </div>
          
        </div>
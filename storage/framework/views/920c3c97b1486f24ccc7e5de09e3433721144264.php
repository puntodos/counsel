<?php $__env->startPush('scripts'); ?>
<script src="<?php echo e(asset('js/dashboard.js')); ?>" ></script>
<?php $__env->stopPush(); ?>

<?php $__env->startSection('content'); ?>

        <br>  
        <div class="container">
          <div class="row">
            <div class="col-md-12 bg-secondary">
              <h1 class="text-center my-1"><?php echo app('translator')->getFromJson('app.admin.menu.option6'); ?></h1>
            </div>
          </div>

          <?php echo $__env->make('dashboard.dentalcases_table', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
          
        </div>
      
    
    
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.admin_template', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
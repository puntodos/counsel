<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

//DENTIST ROUTES
Route::get('/home', 'HomeController@index')->name('home');

Route::resource('dentist', 'DentistsController');
Route::get('changepassword', 'DentistsController@changepassword')->name('dentist.changepassword');
Route::post('changepasswordupdate', 'DentistsController@changepasswordupdate')->name('dentist.changepassword.update');

Route::resource('dentalcases', 'CasesController');
Route::get('dentalcases/payment/plans/{id}', 'CasesController@paymentplans')->name('dentalcases.payments');
Route::get('dentalcase/mark-as-read/{id}', 'CasesController@mark_as_read')->name('dentalcase.mark_as_read');


Route::resource('messages', 'MessagesController');

//ADMIN ROUTES
Route::post('dashboard/messages/store_admin', 'MessagesController@store_admin')->name('dashboard.messages.store');

Route::resource('dashboard', 'DashboardController');
Route::resource('dashboard/dentalcase/diagnostic', 'DiagnosticController');
Route::resource('dashboard/dentalcase/treatmentplan', 'TreatmentPlanController');
Route::post('dashboard/dentalcase/diagnostic/{dentist}', 'DiagnosticController@store')->name('diagnostic.store');
Route::post('dashboard/dentalcase/treatmentplan/{dentist}', 'TreatmentPlanController@store')->name('treatment.store');


Route::get('dentalcase/diagnostic/{dentist}', 'DiagnosticController@show_read')->name('diagnostic.show_read');
Route::get("dentalcase/diagnostic/pdf/{diagnostic}","DiagnosticController@downloadPDF")->name('diagnostic.pdf');
Route::get("dentalcase/dentist/diagnostic/pdf/{diagnostic}","DiagnosticController@downloadPDFByUser")->name('dentist.diagnostic.pdf');

Route::get('dentalcase/treatmentplan/{dentist}', 'TreatmentPlanController@show_read')->name('treatment.show_read');
Route::get("dentalcase/treatmentplan/pdf/{diagnostic}","TreatmentPlanController@downloadPDF")->name('treatment.pdf');
Route::get("dentalcase/dentist/treatmentplan/pdf/{diagnostic}","TreatmentPlanController@downloadPDFByUser")->name('dentist.treatment.pdf');


Route::get('dashboard/list/dentist', 'DashboardController@list_dentist')->name('dashboard.listdentist');
Route::get('dashboard/list/dentist/{id}', 'DashboardController@view_dentist')->name('dashboard.list.dentist');
Route::resource('dashboard', 'DashboardDentalCasesController')->only([
    'update'
])->names([
    'update' => 'dashboard.dentalcase.changestatus'
]);;
Route::get('dashboard/dentalcase/mark-as-read/{id}', 'DashboardDentalCasesController@mark_as_read')->name('dashboard.dentalcase.mark_as_read');

Route::get('dashboard/list/dentist/cases/{id}', 'DashboardController@list_dentalcases_by_dentist')->name('dashboard.list.dentist.cases');
Route::get('dashboard/list/dentalcases', 'DashboardController@list_dentalcases')->name('dashboard.list.dentalcases');
Route::get('dashboard/list/dentalcases/actives', 'DashboardController@list_dentalcases_actives')->name('dashboard.list.dentalcases.actives');
Route::get('dashboard/list/dentalcases/ended', 'DashboardController@list_dentalcases_ended')->name('dashboard.list.dentalcases.ended');
Route::get('dashboard/list/dentalcases/pending', 'DashboardController@list_dentalcases_pending')->name('dashboard.list.dentalcases.pending');
Route::get('dashboard/list/dentalcases/unread', 'DashboardController@list_dentalcases_unread')->name('dashboard.list.dentalcases.unread');
Route::get('dashboard/list/dentalcases/unread/messages', 'DashboardController@list_dentalcases_unread_messages')->name('dashboard.list.dentalcases.unread.messages');
Route::get('dashboard/dentalcase/{id}', 'DashboardController@show_dentalcase')->name('dashboard.dentalcase');
Route::get('dashboards/users', 'DashboardController@users_list')->name('dashboards.users');
Route::get('dashboards/users/edit/{id}', 'DashboardController@user_edit')->name('dashboards.users.edit');
Route::put('dashboards/users/edit/update/{dentist}', 'DashboardController@user_edit_update')->name('dashboards.users.edit_update');

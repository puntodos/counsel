<?php

return [
    'options' => [
        'dentist_profile' => 'image/general/upload.png',
        'dentist_profile_green' => 'image/general/uploaded.png',
        'dentis_create_background' => 'image/general/chair-2589770_1920.jpg',
        'logo' => 'image/general/logo2.png',
        'logo2' => 'image/general/logo2.png',
        'loading' => 'image/general/Loading.gif',
        'favico' => 'image/general/favicon.ico',
    ]
];
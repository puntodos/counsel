<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Http\Request;
use App\User;

class ResetPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */

    use ResetsPasswords;

    /**
     * Where to redirect users after resetting their password.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    public function reset(Request $request){
        //dd(json_encode($request->all()));

        $email = $request->input('email'); 
        $token = $request->input('token');       
        $user = User::select('id','remember_token')->where([['email', '=', $email]                                            ])->get();
        if(count($user) == 0){
            flash()->error(__('app.general.email.resetpassword.noregister'));
            return redirect()->route('password.reset', ['token' => $token]);
        }
        $user = $user[0];
        if($user->remember_token != $token){
            flash()->error(__('app.general.email.resetpassword.invalidtoken'));
            return redirect()->route('password.reset', ['token' => $token]);
        }

        if($request->input('password') == $request->input('password_confirmation')){
            $user->password = bcrypt($request->input('password'));
            $user->remember_token = 'NA';
            $user->save();
            flash()->success(__('app.general.email.resetpassword.success'));
            return redirect()->route('login');
        }else{
            flash()->error(__('app.general.email.resetpassword.notmatch'));
            return redirect()->route('password.reset', ['token' => $token]);
        }
    }
}

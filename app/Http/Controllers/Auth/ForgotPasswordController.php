<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Support\Facades\Mail;
use Illuminate\Http\Request;
use App\User;

class ForgotPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset emails and
    | includes a trait which assists in sending these notifications from
    | your application to your users. Feel free to explore this trait.
    |
    */

    use SendsPasswordResetEmails;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }


    public function reset(){
        
    }

    /*public function showLinkRequestForm(){
        
    }*/

    public function sendResetLinkEmail(Request $request){
        $data = array();
        $email = $request->input('email'); 
        $token = md5(date("YmdHis").rand ( 1000000 , 9999999));       
        $user = User::select('id','remember_token')->where('email', $email)->get();
        if(count($user) == 0){
            flash()->error(__('app.general.email.resetpassword.noregister'));
            return redirect()->route('password.request');
        }
        $user = $user[0];
        $user->remember_token = $token;
        $user->save();

         \DB::table('password_resets')->insert([
                'email' => $email, 
                'token' => $token
            ]);


        $data['email'] = $email;
        $data['url'] = $request->getHost(). '/password/reset/'. $token;
        //dd(json_encode($_POST));
        Mail::send('emails.resetpassword', $data, function($message) use ($data) {
            $message->to($data['email'], '')->subject(__('app.general.email.resetpassword.subject'));
        });
        flash()->success(__('app.general.email.resetpassword.register'));
        return redirect()->route('password.request');
    }


}

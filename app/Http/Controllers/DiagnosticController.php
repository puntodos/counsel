<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Mail;
use Illuminate\Config;
use PDF;
use App\Dentist;
use App\Dentalcase;
use App\User;
use App\Dentist_speciality;
use App\Country;
use App\Diagnostic;
use App\Message;

class DiagnosticController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('roleadmin', ['only' => ['show', 'store', 'downloadPDF', '']]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $id)
    {
        //
        //dd($request->all());
        
        $dentalCase = Dentalcase::where(['id' => $id])->first();
        $diagnostic = $dentalCase->diagnostic;
        $json = $request->all();
        $json = json_encode($json);
        
        if($diagnostic == null){
            $diagnostic = new Diagnostic();
        }
        // }else{
        //     $diagnostic->fill($request->all());
        // }
        //dd($json);
        $diagnostic->diagnostics = $json;
        $diagnostic->sndiagnostic = 'S';
        $diagnostic->dentalcase_id = $id;
        $diagnostic->save();

        //default message
        $admin = User::find(1);
        $messages = Message::where(['dentalcase_id' => $id, 'type' => 'new_diagnostic'])->get();
        if(count($messages) == 0){
            $message = new Message();
            $message->text = __('app.admin.deltalcase.diagnostic.message');
            $message->user_id = $admin->id;
            $message->dentalcase_id = $dentalCase->id;
            $message->type = 'new_diagnostic';
            $message->save();

            //email
            $dentist = $dentalCase->dentist;
            $dentist->each(function($dentist){
                $dentist->user;
            });
            $user = $dentist->user;
            $data = array();
            $data['email'] = $user->email;
            //dd(json_encode($_POST));
            Mail::send('emails.newdiagnostic', $data, function($message) use ($data) {
                $message->to($data['email'], '')->subject(__('app.admin.deltalcase.diagnostic.email.subject'));
            });
        }

        return redirect()->back()->with('success', __('app.admin.deltalcase.diagnostic.save_success'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $jsonString = file_get_contents(base_path('resources/lang/config/diagnosticv1.json'));
        $jsonString = preg_replace("/(\/\*.?\*\/)/", "", $jsonString);
        $json = json_decode($jsonString, true);
        // echo json_encode($json);
        // dd($json);

        $dentalCase = Dentalcase::where(['id' => $id])->first();
        $dentalCase->status_read = 'read';
        $dentalCase->save();

        $dentist = $dentalCase->dentist;
        $dentist->each(function($dentist){
            $dentist->user;
            $dentist->country;
            $dentist->dentalcases_active;
            $dentist->dentalcases;
        });
        $user = $dentist->user;
        //$dentist = $user->dentist;

        $patient = $dentalCase->patient;
        $messages = $dentalCase->messeges;
        $messages->each(function($messages){
            $messages->user;
        });
        

        $diagnostic = $dentalCase->diagnostic;
        $diagnostic_object = array();
        if($diagnostic == null){
            $diagnostic_object = []; //$this->diagnostic_object($fields);
        }else{
            //$diagnostic_object = $this->diagnostic_object($fields, $diagnostic);
            $diagnostic_object = json_decode($diagnostic->diagnostics, true);
        }
        //dd($diagnostic);
        $data = ['user' => $user
                                        , 'dentist' => $dentist
                                        , 'dentalCase' => $dentalCase
                                        , 'patient' => $patient
                                        , 'messages' => $messages
                                        , 'user_path' => '/storage/'.md5($user->id)
                                        , 'diagnostic'  => $diagnostic_object
                                        , 'diagnostic_json' => $json['diagnostic']];
        return view('dashboard.dentalcase_diagnostic', $data);
    }

    public function show_read($id)
    {
        $jsonString = file_get_contents(base_path('resources/lang/config/diagnosticv1.json'));
        $jsonString = preg_replace("/(\/\*.?\*\/)/", "", $jsonString);
        $json = json_decode($jsonString, true);

        $dentalCase = Dentalcase::where(['id' => $id])->first();
        
        $dentalCase->status_read = 'read';
        $dentalCase->save();

        $dentist = $dentalCase->dentist;
        $dentist->each(function($dentist){
            $dentist->user;
            $dentist->country;
            $dentist->dentalcases_active;
            $dentist->dentalcases;
        });
        $user = $dentist->user;
        

        $patient = $dentalCase->patient;
        $messages = $dentalCase->messeges;
        $messages->each(function($messages){
            $messages->user;
        });
        
        $diagnostic = $dentalCase->diagnostic;
        $diagnostic_object = array();
        if($diagnostic == null){
            $diagnostic_object = []; //$this->diagnostic_object($fields);
        }else{
            //$diagnostic_object = $this->diagnostic_object($fields, $diagnostic);
            $diagnostic_object = json_decode($diagnostic->diagnostics, true);
        }

        $data = ['user' => $user
                                        , 'dentist' => $dentist
                                        , 'dentalCase' => $dentalCase
                                        , 'patient' => $patient
                                        , 'messages' => $messages
                                        , 'user_path' => '/storage/'.md5($user->id)
                                        , 'diagnostic'  => $diagnostic_object
                                        , 'diagnostic_json' => $json['diagnostic']];
        return view('dentalcases.dentalcase_diagnostic', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function downloadPDF($id){
        
        $diagnostic = Diagnostic::where(['id' => $id])->first();
        $dentalCase = $diagnostic->dentalcase;
        $dentist = $dentalCase->dentist;
        $user = $dentist->user;
        $patient = $dentalCase->patient;

        $jsonString = file_get_contents(base_path('resources/lang/config/diagnosticv1.json'));
        $jsonString = preg_replace("/(\/\*.?\*\/)/", "", $jsonString);
        $jsonDiagnostic = json_decode($jsonString, true);

        $jsonString = file_get_contents(base_path('resources/lang/config/treatmentv1.json'));
        $jsonString = preg_replace("/(\/\*.?\*\/)/", "", $jsonString);
        $jsonTreatment = json_decode($jsonString, true);

        $diagnostic_object = array();
        $diagnostic_json = [];
        $plan_json = [];
        if($diagnostic == null){
            $diagnostic_object = []; //$this->diagnostic_object($fields);
        }else{
            //$diagnostic_object = $this->diagnostic_object($fields, $diagnostic);
            $diagnostic_json = json_decode($diagnostic->diagnostics, true);
            $plan_json = json_decode($diagnostic->plan, true);
        }
        
        $data['diagnostic'] = $diagnostic;
        $data['dentalcase'] = $dentalCase;
        $data['dentist'] = $dentist;
        $data['user'] = $user;
        $data['patient'] = $patient;
        $data['jsonDiagnostic'] = $jsonDiagnostic;
        $data['jsonTreatment'] = $jsonTreatment;
        $data['diagnostic_json'] = $diagnostic_json;
        $data['treatment_json'] = $plan_json;
        
        $pdf = PDF::loadView('pdf.diagnostic', $data);
        return $pdf->download('Counsel-diagnostic.pdf');
    }

    public function downloadPDFByUser($id){
        
        $current = Auth::user();

        $diagnostic = Diagnostic::where(['id' => $id])->first();
        $dentalCase = $diagnostic->dentalcase;
        $dentist = $dentalCase->dentist;
        $user = $dentist->user;
        
        if($user->id != $current->id){
            flash()->error(__('app.general.back_error'));
            return redirect()->back()->with('error', __('app.general.back_error'));
        }

        $jsonString = file_get_contents(base_path('resources/lang/config/diagnosticv1.json'));
        $jsonString = preg_replace("/(\/\*.?\*\/)/", "", $jsonString);
        $jsonDiagnostic = json_decode($jsonString, true);
        
        $patient = $dentalCase->patient;

        $diagnostic_object = array();
        $diagnostic_json = [];
        if($diagnostic == null){
            $diagnostic_object = []; //$this->diagnostic_object($fields);
        }else{
            $diagnostic_json = json_decode($diagnostic->diagnostics, true);
        }
        
        $data['diagnostic'] = $diagnostic;
        $data['dentalcase'] = $dentalCase;
        $data['dentist'] = $dentist;
        $data['user'] = $user;
        $data['patient'] = $patient;
        $data['jsonDiagnostic'] = $jsonDiagnostic;
        $data['diagnostic_json'] = $diagnostic_json;
        $pdf = PDF::loadView('pdf.diagnostic', $data);
        return $pdf->download('Counsel-diagnostic.pdf');
    }
}

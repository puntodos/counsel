<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\DB;
use Illuminate\Config;
use App\Dentist;
use App\Dentalcase;
use App\User;
use App\Dentist_speciality;
use App\Country;

class DashboardController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('roleadmin');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        error_log('ejecutando constructor DashboardController');
        $data = array();
        /*Mail::send('emails.test', $data, function($message) {
            $message->to('diego0123@gmail.com', 'Jon Doe')->subject('Welcome to the Laravel 4 Auth App!');
        });*/

        $dentists = Dentist::All();
        $dentists = count($dentists);

        $dentalCases = Dentalcase::All();
        $dentalCases = count($dentalCases);

        $dentalCasesActive = Dentalcase::where('status','active')->get();
        $dentalCasesActive = count($dentalCasesActive);

        $dentalCasesEnded = Dentalcase::where('status','ended')->get();
        $dentalCasesEnded = count($dentalCasesEnded);

        $dentalCasesPending = Dentalcase::where('status','pending')->get();
        $dentalCasesPending = count($dentalCasesPending);

        $dentalCasesRead = Dentalcase::where('status_read','unread')->get();
        $dentalCasesRead = count($dentalCasesRead);

        $dentalCaseswithUnReadMessages = Dentalcase::where('messages_unread_4_admin','>', '0')->get();
        $dentalCaseswithUnReadMessages = count($dentalCaseswithUnReadMessages);

        $users = User::All();
        $users = count($users);

        return view('dashboard.index', ['dentists' => $dentists
                                        , 'dentalCases' => $dentalCases
                                        , 'dentalCasesActive' => $dentalCasesActive
                                        , 'dentalCasesEnded' => $dentalCasesEnded
                                        , 'dentalCasesPending' => $dentalCasesPending
                                        , 'dentalCasesRead' => $dentalCasesRead
                                        , 'dentalCaseswithUnReadMessages' => $dentalCaseswithUnReadMessages
                                        , 'users' => $users]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    
    public function list_dentist()
    {
        $dentists = Dentist::all();

        $dentists->each(function($dentists){
            $dentists->user;
            $dentists->country;
            $dentists->dentalcases_active;
            $dentists->dentalcases_unread;
            $dentists->dentalcases;
        });

        return view('dashboard/list_dentists', ['dentists' => $dentists

                        ]);
    }

    public function view_dentist($id){
        //dd(Auth::user()->id);
        //dd(config('constants.options.dentist_profile'));
        $specialities = Dentist_speciality::all();
        //$countries = DB::table('countries')->where('active', 1)->get();
        $countries = Country::select('id', 'name')->where('active', 1)->get();
        $temp = array();
        foreach ($countries as $country) {
            $temp[$country->id] = $country->name;
        }
        $countries = $temp;
        
        $dentist = Dentist::find($id);

        if($dentist == null){
            flash()->error(__('app.admin.list.dentist.show.status.pending'));
            $this->index();
            return;
        }

        $dentist->each(function($dentist){
            $dentist->user;
            $dentist->country;
            $dentist->dentalcases_active;
            $dentist->dentalcases;
        });
        
        //Dentist::where('user_id', Auth::user()->id)->first();
    
        $dentist_especialities = $dentist->dentist_especialities->all();

        $dentistxSpecialitiesIds = array();
        if($dentist_especialities != null){
            foreach ($dentist_especialities as $dentistxSpeciality) {
                array_push($dentistxSpecialitiesIds, $dentistxSpeciality->id);
           }
        }
        $user = $dentist->user;

        $dentalcases = $dentist->dentalcases;
        
         
        return view('dashboard.view_dentist', ['specialities' => $specialities
                                        , 'countries' => $countries
                                        , 'user' => $user
                                        , 'dentist' => $dentist
                                        , 'dentalcases' => $dentalcases
                                        , 'dentistxSpecialitiesIds' => $dentistxSpecialitiesIds
                                        , 'user_path' => '/storage/'.md5($user->id)
                                        , 'status' => 1
                                        , 'search' => "NA003"
                    ]);
    }

    public function list_dentalcases(Request $request)
    {
        // $dentalcases = Dentalcase::all();

        // $dentalcases->each(function($dentalcases){
        //     $dentalcases->dentist;
        // });
        $where = [];
        $dentalcases = $this->filterDentalCases($where, $request->input('search'));
        return view('dashboard/list_dentalcases', ['dentalcases' => $dentalcases,
                       'status' => '', 'search' => $request->input('search') ]);
    }

    public function list_dentalcases_by_dentist($id)
    {
        $dentist = Dentist::find($id);

        $dentist->each(function($dentist){
            $dentist->dentalcases;
        });
        return view('dashboard/list_dentalcases_by_dentist', ['dentalcases' => $dentist->dentalcases
                        ]);
    }

    public function list_dentalcases_actives(Request $request)
    {
        // $dentalcases = Dentalcase::where('status', 'active')->get();
        // $dentalcases->each(function($dentalcases){
        //     $dentalcases->dentist;
        // });
        $where = [['dentalcases.status', 'active']];
        $dentalcases = $this->filterDentalCases($where, $request->input('search'));
        return view('dashboard/list_dentalcases_active', ['dentalcases' => $dentalcases
                        ,'status' => 'actives', 'search' => $request->input('search')]);
    }

    public function list_dentalcases_ended(Request $request)
    {
        // $dentalcases = Dentalcase::where('status', 'ended')->get();
        // $dentalcases->each(function($dentalcases){
        //     $dentalcases->dentist;
        // });
        $where = [['dentalcases.status', 'ended']];
        $dentalcases = $this->filterDentalCases($where, $request->input('search'));
        return view('dashboard/list_dentalcases_ended', ['dentalcases' => $dentalcases
                        ,'status' => 'ended', 'search' => $request->input('search')]);
    }

    public function list_dentalcases_pending(Request $request)
    {
        //dd($request->all());
        // $dentalcases = Dentalcase::where('status', 'pending')->get();
        // $dentalcases->each(function($dentalcases){
        //     $dentalcases->dentist;
        // });
        $where = [['dentalcases.status', 'pending']];
        $dentalcases = $this->filterDentalCases($where, $request->input('search'));
        return view('dashboard/list_dentalcases_pending', ['dentalcases' => $dentalcases
                        ,'status' => 'pending', 'search' => $request->input('search')]);
    }

    public function list_dentalcases_unread(Request $request)
    {
        // $dentalcases = Dentalcase::where('status_read', 'unread')->get();
        // $dentalcases->each(function($dentalcases){
        //     $dentalcases->dentist;
        // });
        $where = [['dentalcases.status_read', 'unread']];
        $dentalcases = $this->filterDentalCases($where, $request->input('search'));
        return view('dashboard/list_dentalcases_unread', ['dentalcases' => $dentalcases
                        ,'status' => 'unread', 'search' => $request->input('search')]);
    }

    public function list_dentalcases_unread_messages(Request $request)
    {
        // $dentalcases = Dentalcase::where('messages_unread_4_admin', '>','0')->get();
        // $dentalcases->each(function($dentalcases){
        //     $dentalcases->dentist;
        // });
        $where = [['dentalcases.messages_unread_4_admin', '>', '0']];
        $dentalcases = $this->filterDentalCases($where, $request->input('search'));
        return view('dashboard/list_dentalcases_unread_messages', ['dentalcases' => $dentalcases
                        ,'status' => 'unread/messages', 'search' => $request->input('search')]);
    }

    private function filterDentalCases($where = [], $search = ''){
        $dentalcases = [];
        if($search == ''){
            if(count($where) > 0){
                $dentalcases = Dentalcase::where($where)->get();
                $dentalcases->each(function($dentalcases){
                    $dentalcases->dentist;
                });
            }else{
                $dentalcases = Dentalcase::all();
                $dentalcases->each(function($dentalcases){
                    $dentalcases->dentist;
                });
            }
        }else{
            $dentalcases = DB::table('dentalcases')
                ->join('dentists', 'dentalcases.dentist_id', '=', 'dentists.id')
                ->join('users', 'dentists.user_id', '=', 'users.id')
                ->where('users.name', 'LIKE', '%' . $search . '%')
                ->where($where)
                ->get(['dentalcases.id']);
            $elements = [];
            foreach ($dentalcases as $element) {
                $elements[] = $element->id;
            }
            
            $dentalcases = Dentalcase::whereIn('id',$elements)->get();
            $dentalcases->each(function($dentalcases){
                    $dentalcases->dentist;
                });
        }

        return $dentalcases;
    }
    
    public function show_dentalcase($id)
    {
        //
        //$user = Auth::user();
        $dentalCase = Dentalcase::where(['id' => $id])->first();
        $dentalCase->status_read = 'read';
        $dentalCase->save();

        $dentist = $dentalCase->dentist;
        $dentist->each(function($dentist){
            $dentist->user;
            $dentist->country;
            $dentist->dentalcases_active;
            $dentist->dentalcases;
        });
        $user = $dentist->user;
        //$dentist = $user->dentist;
        

        $patient = $dentalCase->patient;
        $messages = $dentalCase->messeges;
        $messages->each(function($messages){
            $messages->user;
        });

        $diagnostic = $dentalCase->diagnostic;
        $diagnostic_exists = false;
        if($diagnostic != null){
            $diagnostic_exists = true;
        }

        $data = ['user' => $user
                                        , 'dentist' => $dentist
                                        , 'dentalCase' => $dentalCase
                                        , 'patient' => $patient
                                        , 'messages' => $messages
                                        , 'diagnostic_exists' => $diagnostic_exists
                                        , 'user_path' => '/storage/'.md5($user->id)
                                        , '' => ''];
                                        //dd($data);
        return view('dashboard.dentalcase', $data);

    }

    public function users_list(){

        $users = User::all();
        
        return view('dashboard.list_users', ['users' => $users]);
    }

    public function user_edit($id){
        $user = User::find($id);

        $data = array();
        $token = md5(date("YmdHis").rand ( 1000000 , 9999999));       
        $email = $user->email;
        $user->remember_token = $token;
        $user->save();

         \DB::table('password_resets')->insert([
                'email' => $email, 
                'token' => $token
            ]);

        $dentist = $user->dentist;
        $user = Auth::user();
        return view('dashboard.user_edit', ['user' => $user
                                        , 'dentist' => $dentist
                                        , 'token' => $token
                    ]);
        
    }

    public function user_edit_update(Request $request, $id){
        
        $newpassword = $_POST['newpassword'];
        $newpassword2 = $_POST['newpassword2'];
        
        $dentist = Dentist::find($id);
        $user = $dentist->user;

        if($newpassword != $newpassword2){
            return $this->sendErrorToChangePassword(__('app.dentist.changepassword.nomatch'));
        }

        $user->password = bcrypt($newpassword);
        $user->save();
       
        flash()->success(__('app.general.email.resetpassword.success'));
        return redirect()->back()->with('success', __('app.general.email.resetpassword.success'));
    }

}

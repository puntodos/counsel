<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Mail;
use Illuminate\Config;
use App\Dentist;
use App\Dentalcase;
use App\User;
use App\Dentist_speciality;
use App\Country;
use App\Message;

class DashboardDentalCasesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('roleadmin');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //dd($request->all());
        $dentalcase = Dentalcase::find($id);
        $status = $request->input('status');
        $date = $request->input('ended_date');
        $begin_date = $request->input('begin_date');
        $plan = $request->input('plan');
        if($status != ''){
            $dentalcase->status = $status;
        }
        if($date != ''){
            $dentalcase->ended_date = $date;
        }
        if($begin_date != ''){
            $dentalcase->begin_date = $begin_date;
        }
        if($plan != ''){
            $dentalcase->plan = $plan;
        }
        $dentalcase->save();

        if($date != ''){
            $data = array();
            $dentist = $dentalcase->dentist;
            $user = $dentist->user;
            $data['email'] = $user->email;
            Mail::send('emails.newcaseactived', $data, function($message) use ($data) {
                $message->to($data['email'], '')->subject(__('app.general.email.newcaseactived.subject'));
            });
        }

        if($status == 'active'){
            $dentalCase = Dentalcase::where(['id' => $id])->first();

            $dentist = $dentalCase->dentist;
            $user = $dentist->user;

            $message = new Message();
            $message->text = __('app.general.email.newcaseactived.message.default');
            
            $message->dentalcase_id = $dentalCase->id;
            $message->user_id = Auth::user()->id;
            $message->save();

            $dentalCase->messages_unread_4_dentist = $dentalCase->messages_unread_4_dentist + 1;
            $dentalCase->save();
        }

        flash()->success(__('app.admin.deltalcases.status.message'));
        return redirect()->route('dashboard.dentalcase', ['dentalcase' => $dentalcase->id]);
    }

    public function mark_as_read($id){
        $dentalcase = Dentalcase::find($id);
        $dentalcase->messages_unread_4_admin = 0;
        $dentalcase->save();
        flash()->success(__('app.admin.deltalcases.mark_as_read.message'));
        return redirect()->route('dashboard.dentalcase', ['dentalcase' => $dentalcase->id]);
    }

   

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

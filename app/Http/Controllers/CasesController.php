<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Mail;
use App\Dentist;
use App\Dentist_speciality;
use App\Country;
use App\Dentalcase;
use App\Patient;
use App\User;


class CasesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $user = Auth::user();
        $dentist = $user->dentist;
        $dentalcases = $dentist->dentalcases;

        $dentalcases->each(function($dentalcases){
            $dentalcases->dentist;
            $dentalcases->patient;
        });
        
        //dd($dentalcases);

        return view('dentalcases.index', ['dentalcases' => $dentalcases
                                        , 'user' => $user
                                        , 'dentist' => $dentist
                                        , 'user_path' => '/storage/'.md5($user->id)
                    ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $specialities = Dentist_speciality::all();
        //$countries = DB::table('countries')->where('active', 1)->get();
        $countries = Country::select('id', 'name')->where('active', 1)->get();
        $temp = array();
        foreach ($countries as $country) {
            $temp[$country->id] = $country->name;
        }
        $countries = $temp;
        $dentist = Auth::user()->dentist;
        
        
        $dentistxSpecialitiesIds = array();
        
        $user = Auth::user();
        
        return view('dentalcases.create', ['specialities' => $specialities
                                        , 'countries' => $countries
                                        , 'user' => $user
                                        , 'dentist' => $dentist
                                        //, 'dentistxSpecialities' => $dentistxSpecialities
                                        , 'dentistxSpecialitiesIds' => $dentistxSpecialitiesIds
                                        , 'user_path' => '/storage/'.md5($user->id)
                    ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|max:255',
            'age' => 'required|numeric',
            'genere' => 'required',
        ]);
        //
        $user = Auth::user();
        $dentist = $user->dentist;
        $dentalCase = new Dentalcase();    
        $dentalCase->dentist_id = $dentist->id;
        //dd(count($dentist->dentalcases));
        $dentalCase->case_by_dentist = count($dentist->dentalcases) + 1001;
        $dentalCase->save();
        
        $patient = new Patient();
        $patient->name = $request->input('name');
        $patient->age = $request->input('age');
        $patient->genere = $request->input('genere');

        for ($i=1; $i <= 13 ; $i++) { 
            if ($request->hasFile('radiography'.$i)) {
                $path = $request->file('radiography'.$i)->store('public/'.md5($user->id));
                $name = explode('/', $path)[2];
                $patient['radiography'.$i] = $name;
            }
        }

        $patient->objective_patient = $request->input('objective_patient');
        $patient->objective_dentist = $request->input('objective_dentist');
        $patient->dentalcase_id = $dentalCase->id;
        $patient->save();

        $data = array();
        $admin = User::find(1);
        $data['email'] = $admin->email;
        //dd(json_encode($_POST));
        Mail::send('emails.newdentalcase', $data, function($message) use ($data) {
            $message->to($data['email'], '')->subject(__('app.general.email.newdentalcase.subject'));
        });

        return redirect('dentalcases');
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $user = Auth::user();
        $dentist = $user->dentist;
        $dentalCase = Dentalcase::where(['id' => $id])->first();

        if($user->id != $dentalCase->dentist->user->id){
            flash()->error(__('app.general.back_error'));
            return redirect()->back()->with('error', __('app.general.back_error'));
        }
        
        $patient = $dentalCase->patient;
        $messages = $dentalCase->messeges;
        $messages->each(function($messages){
            $messages->user;
        });
        $diagnostic = $dentalCase->diagnostic;

        $diagnostic_exists = false;
        if($diagnostic != null){
            $diagnostic_exists = true;
        }
        
        $data = ['user' => $user
                                        , 'dentist' => $dentist
                                        , 'dentalCase' => $dentalCase
                                        , 'patient' => $patient
                                        , 'messages' => $messages
                                        , 'user_path' => '/storage/'.md5($user->id)
                                        , 'diagnostic' => $diagnostic
                                        , 'diagnostic_exists' => $diagnostic_exists
                                        , '' => ''];

        switch ($dentalCase->status) {
            case 'pending':
                return view('dentalcases.show_pending', $data);
                break;

            case 'active':
                return view('dentalcases.show_active', $data);
                break;

            case 'ended':
                return view('dentalcases.show_ended', $data);
                break;
            
            default:
                return redirect()->route('dentalcases.index');
                break;
        }

        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $specialities = Dentist_speciality::all();
        //$countries = DB::table('countries')->where('active', 1)->get();
        $countries = Country::select('id', 'name')->where('active', 1)->get();
        $temp = array();
        foreach ($countries as $country) {
            $temp[$country->id] = $country->name;
        }
        $countries = $temp;

        $dentalCase = Dentalcase::where(['id' => $id])->first();
        //dd($dentalCase);
        $user = Auth::user();
        if($user->id != $dentalCase->dentist->user->id){
            flash()->error(__('app.general.back_error'));
            return redirect()->back()->with('error', __('app.general.back_error'));
        }

        $dentist = $dentalCase->dentist;
        $patient = $dentalCase->patient;
        $dentistxSpecialitiesIds = array();
        
        
        return view('dentalcases.edit', ['specialities' => $specialities
                                        , 'countries' => $countries
                                        , 'user' => $user
                                        , 'dentist' => $dentist
                                        , 'dentalCase' => $dentalCase
                                        , 'patient' => $patient
                                        //, 'dentistxSpecialities' => $dentistxSpecialities
                                        , 'dentistxSpecialitiesIds' => $dentistxSpecialitiesIds
                                        , 'user_path' => '/storage/'.md5($user->id)
                    ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'required|max:255',
            'age' => 'required|numeric',
            'genere' => 'required',
        ]);
        //
        $user = Auth::user();

        $dentalCase = Dentalcase::find($id);
        
        $patient = $dentalCase->patient;
        $patient->name = $request->input('name');
        $patient->age = $request->input('age');
        $patient->genere = $request->input('genere');

        for ($i=1; $i <= 13 ; $i++) { 
            if ($request->hasFile('radiography'.$i)) {
                $path = $request->file('radiography'.$i)->store('public/'.md5($user->id));
                $name = explode('/', $path)[2];
                $patient['radiography'.$i] = $name;
            }
        }

        $patient->objective_patient = $request->input('objective_patient');
        $patient->objective_dentist = $request->input('objective_dentist');
        $patient->save();
        flash()->success(__('app.dentalcase.patient.edit.success'));
        return redirect()->route('dentalcases.show', ['dentalcase' => $dentalCase->id]);
        //return redirect()->route('dentalcases.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function paymentplans($id)
    {
        $user = Auth::user();
        $dentist = $user->dentist;
        $dentalCase = Dentalcase::where(['id' => $id])->first();
        
        $patient = $dentalCase->patient;
        $messages = $dentalCase->messeges;
        $messages->each(function($messages){
            $messages->user;
        });

        $data = ['user' => $user
                                        , 'dentist' => $dentist
                                        , 'dentalCase' => $dentalCase
                                        , 'patient' => $patient
                                        , 'messages' => $messages
                                        , 'user_path' => '/storage/'.md5($user->id)
                                        , '' => ''];

        
        return view('dentalcases.payment_plans', $data);
               
    }

     public function mark_as_read($id){
        $dentalcase = Dentalcase::find($id);
        $dentalcase->messages_unread_4_dentist = 0;
        $dentalcase->save();
        flash()->success(__('app.admin.deltalcases.mark_as_read.message'));
        return redirect()->route('dentalcases.show', ['dentalcase' => $dentalcase->id]);
    }



}

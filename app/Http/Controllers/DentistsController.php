<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Hash;
use Illuminate\Config;
use App\Dentist;
use App\Dentist_speciality;
use App\Country;
use App\User;


class DentistsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(Auth::user()->type != 'dentist'){
            return redirect('home');
        }
        //dd('voy');
        $dentist = Dentist::where('user_id', Auth::user()->id)->first();
        if($dentist == null){
            return redirect('dentist/create');
        }else{
            if($dentist->status != "complete"){
                return redirect('dentist/create');
            }else{
                return redirect('dentalcases');
            }
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //dd(Auth::user()->id);
        //dd(config('constants.options.dentist_profile'));
        $specialities = Dentist_speciality::all();
        //$countries = DB::table('countries')->where('active', 1)->get();
        $countries = Country::select('code','name', 'id')->where('active', 1)->get();
        $phone_countries = array();
        foreach ($countries as $country) {
            $phone_countries[$country->id] = $country->name .' (+'.$country->code.')';
        }
        $countries = Country::where('active', 1)->pluck('name', 'id');
        $dentist = Dentist::where('user_id', Auth::user()->id)->first();
        if($dentist != null){
            return redirect('dentist/edit');
        }
        
        $dentistxSpecialitiesIds = array();
        
        $user = Auth::user();
        flash()->success(__('app.dentist.createdaccount'));
        return view('dentist.create', ['specialities' => $specialities
                                        , 'countries' => $countries
                                        , 'phone_countries' => $phone_countries
                                        , 'user' => $user
                                        , 'dentist' => $dentist
                                        //, 'dentistxSpecialities' => $dentistxSpecialities
                                        , 'dentistxSpecialitiesIds' => $dentistxSpecialitiesIds
                                        , 'user_path' => '/storage/'.md5($user->id)
                    ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        
        //dd($request->input('tyc'));
        //we run the validations for all fields
        $request->validate([
            'name' => 'required|max:255',
            'cell_phone' => 'required|max:20|min:7',
            'country' => 'required',
            'code_phone_id' => 'required',
            'email' => 'unique:users',
            'city' => 'required',
            'tyc' => 'required',
            'university' => 'required',
            'file_thumb' => 'nullable|image|mimes:jpeg,png,jpg,gif|max:5120',
            'file_document' => 'nullable|mimes:jpeg,png,jpg,gif,pdf|max:15360',
        ]);

        //the unique field that can be modified is name, here we update that
        $user = Auth::user();
        $user->name = $request->name;
        $user->save();

        //we create dentist
        //the same for the dentist x specialities
        $dentist = new Dentist();
        //update or create for the dentist 
        $dentist->code_phone_id = $request->input('code_phone_id');
        $dentist->cell_phone = $request->input('cell_phone');
        $dentist->country_id = $request->input('country');
        $dentist->city = $request->input('city');
        $dentist->company = $request->input('company');
        $dentist->document_number = $request->input('document_number');
        $dentist->associations = $request->input('associations');
        $dentist->university = $request->input('university');
        if($request->input('tyc') == 'on'){
            $dentist->tyc = 1;
        }
        $dentist->user_id = $user->id;
        $dentist->status = 'complete';
        $dentist->supports_institution = $request->input('supports_institution');

        if ($request->hasFile('file_thumb')) {
            $path = $request->file('file_thumb')->store('public/'.md5($user->id));
            $name = explode('/', $path)[2];
            $dentist->file_thumb = $name;
        }

        if ($request->hasFile('file_document')) {
            $path = $request->file('file_document')->store('public/'.md5($user->id));
            $name = explode('/', $path)[2];
            $dentist->file_document = $name;
        }

        $dentist->save();

        $dentist->dentist_especialities()->detach();
        $dentist->dentist_especialities()->attach($request->input('specialities'));
  
        if($dentist->status == 'complete'){
            $data = array();
            $admin = User::find(1);
            $data['email'] = $admin->email;
            //dd(json_encode($_POST));
            Mail::send('emails.newaccount', $data, function($message) use ($data) {
                $message->to($data['email'], '')->subject(__('app.general.email.newaccount.subject'));
            });
            return redirect('dentist');
        }else{
            return redirect('dentist/create');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        
        //dd(Auth::user()->id);
        //dd(config('constants.options.dentist_profile'));
        $specialities = Dentist_speciality::all();
        //$countries = DB::table('countries')->where('active', 1)->get();
        $countries = Country::select('code','name', 'id')->where('active', 1)->get();
        $phone_countries = array();
        foreach ($countries as $country) {
            $phone_countries[$country->id] = $country->name .' (+'.$country->code.')';
        }
        $countries = Country::where('active', 1)->pluck('name', 'id');
        
        $dentist = Auth::user()->dentist;
        
        //Dentist::where('user_id', Auth::user()->id)->first();
        
        $dentist_especialities = $dentist->dentist_especialities->all();

        $dentistxSpecialitiesIds = array();
        if($dentist_especialities != null){
            foreach ($dentist_especialities as $dentistxSpeciality) {
                array_push($dentistxSpecialitiesIds, $dentistxSpeciality->id);
           }
        }
        $user = Auth::user();
        
        return view('dentist.edit', ['specialities' => $specialities
                                        , 'countries' => $countries
                                        , 'phone_countries' => $phone_countries
                                        , 'user' => $user
                                        , 'dentist' => $dentist
                                        //, 'dentistxSpecialities' => $dentistxSpecialities
                                        , 'dentistxSpecialitiesIds' => $dentistxSpecialitiesIds
                                        , 'user_path' => '/storage/'.md5($user->id)
                    ]);
                    
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
         //
        //dd($request->input('tyc'));
        //we run the validations for all fields
        $request->validate([
            'name' => 'required|max:255',
            'cell_phone' => 'required|max:20|min:7',
            'country' => 'required',
            'code_phone_id' => 'required',
            'email' => 'unique:users',
            'city' => 'required',
            'tyc' => 'required',
            'university' => 'required',
            'file_thumb' => 'nullable|image|mimes:jpeg,png,jpg,gif|max:5120',
            'file_document' => 'nullable|mimes:jpeg,png,jpg,gif,pdf|max:15360',
        ]);

        //the unique field that can be modified is name, here we update that
        $user = Auth::user();
        $user->name = $request->name;
        $user->save();

        //we create dentist
        //the same for the dentist x specialities
        $dentist = Auth::user()->dentist;
        //update or create for the dentist 
        $dentist->code_phone_id = $request->input('code_phone_id');
        $dentist->cell_phone = $request->input('cell_phone');
        $dentist->country_id = $request->input('country');
        $dentist->city = $request->input('city');
        $dentist->company = $request->input('company');
        $dentist->document_number = $request->input('document_number');
        $dentist->associations = $request->input('associations');
        $dentist->university = $request->input('university');
        if($request->input('tyc') == 'on'){
            $dentist->tyc = 1;
        }
        $dentist->user_id = $user->id;
        $dentist->status = 'complete';
        $dentist->supports_institution = $request->input('supports_institution');

        if ($request->hasFile('file_thumb')) {
            $path = $request->file('file_thumb')->store('public/'.md5($user->id));
            $name = explode('/', $path)[2];
            $dentist->file_thumb = $name;
        }

        if ($request->hasFile('file_document')) {
            $path = $request->file('file_document')->store('public/'.md5($user->id));
            $name = explode('/', $path)[2];
            $dentist->file_document = $name;
        }

        $dentist->save();
        
        $dentist->dentist_especialities()->detach();
        $dentist->dentist_especialities()->attach($request->input('specialities'));
  
        
        if($dentist->status == 'complete'){
            return redirect('dentist');
        }else{
            return redirect('dentist/create');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function changepassword(){
        
        $user = Auth::user();

        $data = array();
        $email = $user->email;; 
        $token = md5(date("YmdHis").rand ( 1000000 , 9999999));       
        
        $user->remember_token = $token;
        $user->save();

         \DB::table('password_resets')->insert([
                'email' => $email, 
                'token' => $token
            ]);

        $dentist = $user->dentist;
        return view('dentist.changepassword', ['user' => $user
                                        , 'dentist' => $dentist
                                        , 'token' => $token
                    ]);
    }

    public function changepasswordupdate(Request $request){
        //dd(json_encode($request->all()));

        $currentpassword = $request->input('currentpassword');
        $newpassword = $request->input('newpassword');
        $newpassword2 = $request->input('newpassword2');
        $user = Auth::user();

        //if($user->password != bcrypt($currentpassword)){
        if(!(Hash::check($currentpassword, Auth::user()->password))){
            return $this->sendErrorToChangePassword(__('app.dentist.changepassword.badpassword'));
        }

        if($newpassword != $newpassword2){
            return $this->sendErrorToChangePassword(__('app.dentist.changepassword.nomatch'));
        }

        $user->password = bcrypt($newpassword);
        $user->save();
       
        flash()->success(__('app.general.email.resetpassword.success'));
        //return redirect()->route('dentist.index');
        return $this->index();
        
    }

    private function sendErrorToChangePassword($message){
        flash()->error($message);
        return $this->changepassword();
    }
}

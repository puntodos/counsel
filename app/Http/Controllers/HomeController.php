<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Dentists;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        //dd(Auth::user());
        if(Auth::user()->type == 'admin'){
            return redirect('dashboard');
        }elseif (Auth::user()->type == 'assistant') {
            # code...
        }else{
            return redirect('dentist');
        }
        //return view('home');
    }
}

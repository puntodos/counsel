<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use App\Dentist;
use App\Dentist_speciality;
use App\Message;
use App\Dentalcase;
use App\User;

class MessagesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $request->validate([
            'text' => 'required',
        ]);

        $user = Auth::user();
        $dentist = $user->dentist;
        $dentalCase = Dentalcase::where(['id' => $request->input('dentalcase_id')])->first();

        $message = new Message();
        $message->text = $request->input('text');
        $message->user_id = $user->id;
        $message->dentalcase_id = $dentalCase->id;

        if ($request->hasFile('file_attach')) {
            $path = $request->file('file_attach')->store('public/'.md5($user->id));
            $name = explode('/', $path)[2];
            $message->file_attach = $name;
        }
        $message->save();

        $dentalCase->messages_unread_4_admin = $dentalCase->messages_unread_4_admin + 1;
        $dentalCase->save();

        $data = array();
        $admin = User::find(1);
        $data['email'] = $admin->email;
        //dd(json_encode($_POST));
        Mail::send('emails.newmessagereceived', $data, function($message) use ($data) {
            $message->to($data['email'], '')->subject(__('app.general.email.newmessagereceived.subject'));
        });

        return redirect()->route('dentalcases.show', ['dentalcase' => $dentalCase->id]);
    }

    public function store_admin(Request $request)
    {
        //
        $request->validate([
            'text' => 'required',
        ]);
        
        $dentalCase = Dentalcase::where(['id' => $request->input('dentalcase_id')])->first();

        $dentist = $dentalCase->dentist;
        $user = $dentist->user;

        $message = new Message();
        $message->text = $request->input('text');
        
        $message->dentalcase_id = $dentalCase->id;

        if ($request->hasFile('file_attach')) {
            $path = $request->file('file_attach')->store('public/'.md5($user->id));
            $name = explode('/', $path)[2];
            $message->file_attach = $name;
        }
        $message->user_id = Auth::user()->id;
        $message->save();

        $dentalCase->messages_unread_4_dentist = $dentalCase->messages_unread_4_dentist + 1;
        $dentalCase->save();

        $data = array();
        
        $data['email'] = $user->email;
        //dd(json_encode($_POST));
        Mail::send('emails.newresponsemessage', $data, function($message) use ($data) {
            $message->to($data['email'], '')->subject(__('app.general.email.newresponsemessage.subject'));
        });

        return redirect()->route('dashboard.dentalcase', ['dentalcase' => $dentalCase->id]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

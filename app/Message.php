<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
    protected $table = "messages";

    public function dentalcase(){
        return $this->belongsTo('App\Case');
    }

    public function user(){
        return $this->belongsTo('App\User');
    }
}

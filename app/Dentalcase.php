<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Dentalcase extends Model
{
    protected $table = "dentalcases";


    public function dentist(){
        return $this->belongsTo('App\Dentist');
    }

    public function patient(){
        return $this->hasOne('App\Patient');
    }

    public function messeges(){
        return $this->hasMany('App\Message');
    }

    public function payments(){
        return $this->hasMany('App\Payment');
    }

    public function diagnostic(){
        return $this->hasOne('App\Diagnostic');
    }
}

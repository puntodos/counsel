<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Diagnostic extends Model
{
	protected $table = "diagnostics";

	protected $fillable = [
        'id',
'facial_morphology',
'type_of_smile',
'fifth_facials',
'thirds_facials',
'facial_type',
'angle_nasolabial',
'angle_mentolabial',
'upper_lip',
'lower_lip',
'lip_competition',
'chin',
'relationship_canine_right',
'relationship_canine_left',
'molar_relationship_right',
'overjet',
'molar_relationship_left',
'cross_bite',
'cross_bite_type',
'overbite',
'match_between_mid_line_up_w_mid_line_facial',
'match_between_mid_line_dw_w_mid_line_up',
'form_upper_arch',
'symmetry_upper_arch',
'shape_lower_arch', 
'symmetry_down_arch',
'top_crowding',
'top_crowding_type',
'lower_crowding',
'lower_crowding_type',
'top_diastema',
'lower_diastema',
'facial_diagnostic',
'esqueletic_diagnostic',
'oclusal_diagnostic',
'dentalcase_id'
    ];

    public function dentalcase(){
        return $this->belongsTo('App\Dentalcase');
    }
}

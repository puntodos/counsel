<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    protected $table = "payments";

    public function dentalcase(){
        return $this->belongsTo('App\Case');
    }
}

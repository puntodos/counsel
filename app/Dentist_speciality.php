<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Dentist_speciality extends Model
{
    protected $table = "dentist_specialities";


    public function dentists(){
        return $this->belongsToMany('App\Dentist');
    }
}

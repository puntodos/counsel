<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Blade;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Blade::component('components.app_label', 'app_label');
        Blade::component('components.app_label_error_alone', 'app_label_error_alone');
        Blade::component('components.app_file_w_modal_help', 'app_file_w_modal_help');
        Blade::component('components.app_file_w_modal', 'app_file_w_modal');
        Blade::component('components.app_errors_modal', 'app_errors_modal');
        Blade::component('components.app_file_preview', 'app_file_preview');
        Blade::component('components.app_messages', 'app_messages');
        Blade::component('components.app_diagnostic_radios', 'app_diagnostic_radios');
        Blade::component('components.app_diagnostic_radios_new', 'app_diagnostic_radios_new');
        Blade::component('components.app_diagnostic_select', 'app_diagnostic_select');
        Blade::component('components.app_diagnostic_check', 'app_diagnostic_check');
        Blade::component('components.app_diagnostic_radios_read', 'app_diagnostic_radios_read');
        Blade::component('components.app_pdf_field', 'app_pdf_field');
        
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    protected $table = "countries";


    public function dentists(){
        return $this->hasMany('App\Dentist');
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Dentist extends Model
{
    protected $table = "dentists";



    public function user(){
        return $this->belongsTo('App\User');
    }

    public function country(){
        return $this->belongsTo('App\Country');
    }

    public function dentalcases(){
        return $this->hasMany('App\Dentalcase');
    }

    public function dentist_especialities(){
        return $this->belongsToMany('App\Dentist_speciality');
    }

    /*****************************/

    public function dentalcases_active(){
        return $this->hasMany('App\Dentalcase')->where('status', 'active');
    }

    public function dentalcases_unread(){
        return $this->hasMany('App\Dentalcase')->where('status_read', 'unread');
    }

    
}

<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'remember_token', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     *
     protected $hidden = [
        'password', 'remember_token',
    ];
    */
    protected $hidden = [
       
    ];


    public function dentist(){
        return $this->hasOne('App\Dentist');
    }

    public function messages(){
        return $this->hasMany('App\Message');
    }
}

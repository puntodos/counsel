@extends('layouts.dentist_template')

@push('scripts')
<script src="{{ asset('js/dentalcases.js') }}" ></script>
@endpush

@section('content')

    @include('dentalcases.files_partial')


    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class="row">
            <div class="col-md-6 bg-secondary">
              <h3 class="my-1">
                @lang('app.dentalcase.patient.show.trate') {{ $patient->name }}<br>
                ID: {{ $dentalCase->case_by_dentist }}
              </h3>
            </div>
             <div class="col-md-6 bg-secondary">
                <br>
                <a class="btn btn-danger text-white btn-sm float-sm-right" style="margin: 1px" href="{{ route('dentalcases.edit', $dentalCase->id) }}">
                {{ __('app.dentalcase.patient.show.edit_case') }}</a>
                @php
                /*<a class="btn btn-primary text-white">
                {{ __('app.dentalcase.patient.show.end_case') }}</a>*/
                @endphp
                
                @if($diagnostic != null && $diagnostic['snplan'] == "S")
                <a class="btn btn-primary text-white btn-sm float-sm-right" style="margin: 1px" href="{{ route('treatment.show_read', $dentalCase->id) }}">
                    {{ __('app.admin.deltalcases.treatment.view_btn') }}
                  </a>
                @endif
                @if($diagnostic != null && $diagnostic['sndiagnostic'] == "S")
                <a class="btn btn-primary text-white btn-sm float-sm-right" style="margin: 1px" href="{{ route('diagnostic.show_read', $dentalCase->id) }}">
                    {{ __('app.admin.deltalcases.diagnostic.view_btn') }}
                  </a>
                @endif
                <a class="btn btn-warning btn-sm float-sm-right" style="margin: 1px" href="{{ route('dentalcases.index') }}">
                    {{ __('app.general.back_btn') }}</a>
            </div>
          </div>
          <br>
          <div class="row">
            <div class="col-md-4 ">
             <table class="table table-striped table-sm">
              <thead>
                <tr>
                  <th scope="col">@lang('app.dentalcase.patient.name')</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>{{ $patient->name }}</td>
                </tr>
              </tbody>
            </table> 
              <table class="table table-striped table-sm">
                <thead>
                  <tr>
                    <th scope="col">@lang('app.dentalcase.patient.age')</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td>{{ $patient->age }}</td>
                  </tr>
                </tbody>
              </table>
              <table class="table table-striped table-sm">
                <thead>
                  <tr>
                    <th scope="col">@lang('app.dentalcase.patient.genere')</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td>@lang('app.dentalcase.patient.genere.'.$patient->genere )</td>
                  </tr>
                </tbody>
              </table>   
              @if($patient->objective_patient != "")           
              <table class="table table-striped table-sm">
                <thead>
                  <tr>
                    <th scope="col">@lang('app.dentalcase.patient.objective_patient')</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td>{{ $patient->objective_patient}}</td>
                  </tr>
                </tbody>
              </table>
              @endif
              @if($patient->objective_dentist != "")           
              <table class="table table-striped table-sm">
                <thead>
                  <tr>
                    <th scope="col">@lang('app.dentalcase.patient.objective_dentist')</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td>{{ $patient->objective_dentist}}</td>
                  </tr>
                </tbody>
              </table>
              @endif
              @if($dentalCase->begin_date != "")           
              <table class="table table-striped table-sm">
                <thead>
                  <tr>
                    <th scope="col">@lang('app.dentalcase.patient.begin_date')</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td>{{ $dentalCase->begin_date}}</td>
                  </tr>
                </tbody>
              </table>
              <table class="table table-striped table-sm">
                <thead>
                  <tr>
                    <th scope="col">@lang('app.dentalcase.patient.end_date')</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td>{{ $dentalCase->ended_date}}</td>
                  </tr>
                </tbody>
              </table>
              @endif
              @if($dentalCase->plan != "")           
              <table class="table table-striped table-sm">
                <thead>
                  <tr>
                    <th scope="col">@lang('app.dentalcase.payment.plan.title')</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td>@lang('app.dentalcase.payment.'.$dentalCase->plan)</td>
                  </tr>
                </tbody>
              </table>
              @endif
              <a class="btn btn-primary text-white" data-toggle="modal" data-target=".file-list-modal">
              {{ __('app.dentalcase.patient.show.files') }}</a>
            </div>
            <div class="col-md-8 ">
              
              @include('dentalcases.message_partial')

            </div>
          </div>
          <br>
          
          
          <hr>
      </div>
    </div>

@endsection
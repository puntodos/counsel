@extends('layouts.dentist_template')

@push('scripts')
<script src="{{ asset('js/dentalcases.js') }}" ></script>
@endpush

@section('content')

    @include('dentalcases.files_partial')


    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class="row">
            <div class="col-md-6 bg-secondary">
              <h3 class="my-1">
                @lang('app.dentalcase.patient.show.trate') {{ $patient->name }}<br>
                ID: {{ $dentalCase->case_by_dentist }}
              </h3>
            </div>
             <div class="col-md-2 bg-secondary">
              
            </div>
             <div class="col-md-4 bg-secondary">
                <br>
               
                  <a class="btn btn-warning btn-sm float-sm-right" href="{{ route('dentalcases.show', $dentalCase->id) }}">
                    {{ __('app.general.back_btn') }}
                  </a>

                  <a class="btn btn-dark btn-sm float-sm-right" href="{{ route('dentist.treatment.pdf', $dentalCase['diagnostic']['id']) }}">
                    {{ __('app.admin.deltalcase.diagnostic.pdf.btn') }}
                  </a>
            </div>
          </div>
          <br>
          <div class="row">
            <div class="col-md-3 ">
             <table class="table table-striped table-sm">
              <thead>
                <tr>
                  <th scope="col">@lang('app.dentalcase.patient.name')</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>{{ $patient->name }}</td>
                </tr>
              </tbody>
            </table> 
              <table class="table table-striped table-sm">
                <thead>
                  <tr>
                    <th scope="col">@lang('app.dentalcase.patient.age')</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td>{{ $patient->age }}</td>
                  </tr>
                </tbody>
              </table>
              <table class="table table-striped table-sm">
                <thead>
                  <tr>
                    <th scope="col">@lang('app.dentalcase.patient.genere')</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td>@lang('app.dentalcase.patient.genere.'.$patient->genere )</td>
                  </tr>
                </tbody>
              </table>   
              @if($patient->objective_patient != "")           
              <table class="table table-striped table-sm">
                <thead>
                  <tr>
                    <th scope="col">@lang('app.dentalcase.patient.objective_patient')</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td>{{ $patient->objective_patient}}</td>
                  </tr>
                </tbody>
              </table>
              @endif
              @if($patient->objective_dentist != "")           
              <table class="table table-striped table-sm">
                <thead>
                  <tr>
                    <th scope="col">@lang('app.dentalcase.patient.objective_dentist')</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td>{{ $patient->objective_dentist}}</td>
                  </tr>
                </tbody>
              </table>
              @endif
              <a class="btn btn-primary text-white" data-toggle="modal" data-target=".file-list-modal">
              {{ __('app.dentalcase.patient.show.files') }}</a>
            </div>
            <div class="col-md-9 ">
              
              <br>
              <div class="row">
                <div class="col-md-12 bg-secondary text-center">
                  <h3 class="my-1">
                    @lang('app.admin.deltalcase.treatment.title')
                  </h3>
                </div>            
              </div>
              <br>
              <div class="row">
                <div class="col-md-12 ">
                  
                  @include('dentalcases.dentalcase_treatment_fields')

                </div>
              </div>

            </div>
          </div>
          <br>
          
          
          <hr>
      </div>
    </div>

@endsection
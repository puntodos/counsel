<div class="modal fade file-list-modal" role="dialog">
      <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <h3 class="modal-title"><b>@lang('app.dentalcase.patient.show.files.title')</b></h3>
            <button type="button" class="close" data-dismiss="modal">&times;</button>
          </div>
          <div class="modal-body">
            <div class="container">
              <div class="row">
                <div class="col-md-12 bg-secondary">
                  <h3 class="text-center my-1">@lang('app.dentalcase.patient.diagnosticimages')</h3>
                </div>
              </div>

              <br>
              <div class="row">
                <div class="col-md-6">
                    @app_file_preview(['field' => 'radiography1', 'user_path' => $user_path.'/', 'file' => $patient['radiography1']])
                    @endapp_file_preview
                </div>
                <div class="col-md-6">
                    @app_file_preview(['field' => 'radiography2', 'user_path' => $user_path.'/', 'file' => $patient['radiography2']])
                    @endapp_file_preview
                </div>
              </div>
              <br>
              <div class="row">
                <div class="col-md-6">
                    @app_file_preview(['field' => 'radiography3', 'user_path' => $user_path.'/', 'file' => $patient['radiography3']])
                    @endapp_file_preview
                </div>
                <div class="col-md-6">
                    @app_file_preview(['field' => 'radiography4', 'user_path' => $user_path.'/', 'file' => $patient['radiography4']])
                    @endapp_file_preview
                </div>
              </div>
              <br>
              <div class="row">
                <div class="col-md-6">
                    @app_file_preview(['field' => 'radiography5', 'user_path' => $user_path.'/', 'file' => $patient['radiography5']])
                    @endapp_file_preview
                </div>
                <div class="col-md-6">
                  
                </div>
              </div>
              <br>
              <div class="row">
                <div class="col-md-12 bg-secondary">
                  <h3 class="text-center my-1">@lang('app.dentalcase.patient.diagnosticimages2')</h3>
                </div>
              </div>
              <br>
              <div class="row">
                <div class="col-md-6">
                    @app_file_preview(['field' => 'radiography6', 'user_path' => $user_path.'/', 'file' => $patient['radiography6']])
                    @endapp_file_preview
                </div>
                <div class="col-md-6">
                    @app_file_preview(['field' => 'radiography7', 'user_path' => $user_path.'/', 'file' => $patient['radiography7']])
                    @endapp_file_preview
                </div>
              </div>
              <br>
              <div class="row">
                <div class="col-md-6">
                    @app_file_preview(['field' => 'radiography8', 'user_path' => $user_path.'/', 'file' => $patient['radiography8']])
                    @endapp_file_preview
                </div>
                <div class="col-md-6">
                  
                </div>
              </div>
              <br>
              <div class="row">
                <div class="col-md-12 bg-secondary">
                  <h3 class="text-center my-1">@lang('app.dentalcase.patient.diagnosticimages3')</h3>
                </div>
              </div>
              <br>
              <div class="row">
                <div class="col-md-6">
                  @app_file_preview(['field' => 'radiography9', 'user_path' => $user_path.'/', 'file' => $patient['radiography9']])
                    @endapp_file_preview
                </div>
                <div class="col-md-6">
                  @app_file_preview(['field' => 'radiography12', 'user_path' => $user_path.'/', 'file' => $patient['radiography12']])
                    @endapp_file_preview
                </div>
              </div>
              <br>
              <div class="row">
                <div class="col-md-6">
                  @app_file_preview(['field' => 'radiography10', 'user_path' => $user_path.'/', 'file' => $patient['radiography10']])
                    @endapp_file_preview
                  
                </div>
                <div class="col-md-6">
                  @app_file_preview(['field' => 'radiography11', 'user_path' => $user_path.'/', 'file' => $patient['radiography11']])
                    @endapp_file_preview
                  
                </div>
              </div>
              <br>
              <div class="row">
                <div class="col-md-6">
                  @app_file_preview(['field' => 'radiography13', 'user_path' => $user_path.'/', 'file' => $patient['radiography13']])
                    @endapp_file_preview
                </div>
                <div class="col-md-6">
                  
                </div>
              </div>
              <br>
              
            </div>
            
          </div>
        </div>
      </div>
    </div>
@extends('layouts.dentist_template')

@section('loading_text')
@lang('app.dentalcase.create.loading')
@endsection

@section('content')

    {!! Form::open(['route' => ['dentalcases.update', $dentalCase], 'method' => 'PUT', 'files' => true, 'class' => 'form-2-loading']) !!}
      @csrf
      
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class="row">
            <div class="col-md-12 bg-secondary">
              <h1 class="text-center my-1">@lang('app.dentalcase.case_profile_title.edit')</h1>
            </div>
          </div>
          <br>
          <div class="row">
            <div class="col-md-4">
              <div class="form-group"> 
                @app_label(['field' => 'name', 'namespace' => 'app.dentalcase.patient'])
                @endapp_label
                  {!! Form::text('name'
                              , $patient->name
                              , ['class' => 'form-control'
                              , 'placeholder' => __('app.dentalcase.patient.name')
                              , 'required'])
                 !!}
                </div>
              </div>
            <div class="col-md-4">
              <div class="form-group"> 
                @app_label(['field' => 'age', 'namespace' => 'app.dentalcase.patient'])
                @endapp_label
                  {!! Form::number('age'
                              , $patient->age
                              , ['class' => 'form-control'
                              , 'placeholder' => __('app.dentalcase.patient.age')
                              , 'required'])
                 !!}
                </div>
              </div>
            <div class="col-md-4">
              <div class="form-group"> 
                @app_label(['field' => 'genere', 'namespace' => 'app.dentalcase.patient'])
                @endapp_label
                  {!! Form::select('genere'
                                , ['m' => __('app.dentalcase.patient.genere.m')
                                , 'f' => __('app.dentalcase.patient.genere.f')]
                                , $patient->genere
                                , ['placeholder' => __('app.dentalcase.patient.genere.select'),
                                  'class' => 'form-control'
                                , 'required'])
                  !!}
                </div>
              </div>
            
          </div>
          <div class="row">
            <div class="col-md-12 bg-secondary">
              <h1 class="text-center my-1">@lang('app.dentalcase.patient.diagnosticimages')</h1>
            </div>
          </div>

          <br>
          <div class="row">
            <div class="col-md-6">
                @app_file_w_modal_help(['field' => 'radiography1'
                                , 'patient' => $patient
                                , 'user_path' => $user_path])
                @endapp_file_w_modal_help
            </div>
            <div class="col-md-6">
                @app_file_w_modal_help(['field' => 'radiography2'
                                , 'patient' => $patient
                                , 'user_path' => $user_path])
                @endapp_file_w_modal_help
            </div>
          </div>
          <br>
          <div class="row">
            <div class="col-md-6">
                @app_file_w_modal_help(['field' => 'radiography3'
                                , 'patient' => $patient
                                , 'user_path' => $user_path])
                @endapp_file_w_modal_help
            </div>
            <div class="col-md-6">
                @app_file_w_modal_help(['field' => 'radiography4'
                                , 'patient' => $patient
                                , 'user_path' => $user_path])
                @endapp_file_w_modal_help
            </div>
          </div>
          <br>
          <div class="row">
            <div class="col-md-6">
                @app_file_w_modal_help(['field' => 'radiography5'
                                , 'patient' => $patient
                                , 'user_path' => $user_path])
                @endapp_file_w_modal_help
            </div>
            <div class="col-md-6">
              
            </div>
          </div>
          <br>
          <div class="row">
            <div class="col-md-12 bg-secondary">
              <h1 class="text-center my-1">@lang('app.dentalcase.patient.diagnosticimages2')</h1>
            </div>
          </div>
          <br>
          <div class="row">
            <div class="col-md-6">
                 @app_file_w_modal_help(['field' => 'radiography6'
                                , 'patient' => $patient
                                , 'user_path' => $user_path])
                @endapp_file_w_modal_help
            </div>
            <div class="col-md-6">
                 @app_file_w_modal_help(['field' => 'radiography7'
                                , 'patient' => $patient
                                , 'user_path' => $user_path])
                @endapp_file_w_modal_help
            </div>
          </div>
          <br>
          <div class="row">
            <div class="col-md-6">
                 @app_file_w_modal_help(['field' => 'radiography8'
                                , 'patient' => $patient
                                , 'user_path' => $user_path])
                @endapp_file_w_modal_help
            </div>
            <div class="col-md-6">
              
            </div>
          </div>
          <br>
          <div class="row">
            <div class="col-md-12 bg-secondary">
              <h1 class="text-center my-1">@lang('app.dentalcase.patient.diagnosticimages3')</h1>
            </div>
          </div>
          <br>
          <div class="row">
            <div class="col-md-6">
              @app_file_w_modal_help(['field' => 'radiography9'
                                , 'patient' => $patient
                                , 'user_path' => $user_path])
                @endapp_file_w_modal_help
            </div>
            <div class="col-md-6">
              @app_file_w_modal_help(['field' => 'radiography10'
                                , 'patient' => $patient
                                , 'user_path' => $user_path])
                @endapp_file_w_modal_help
            </div>
          </div>
          <br>
          <div class="row">
            <div class="col-md-6">
              @app_file_w_modal_help(['field' => 'radiography11'
                                , 'patient' => $patient
                                , 'user_path' => $user_path])
                @endapp_file_w_modal_help
            </div>
            <div class="col-md-6">
              @app_file_w_modal_help(['field' => 'radiography12'
                                , 'patient' => $patient
                                , 'user_path' => $user_path])
                @endapp_file_w_modal_help
            </div>
          </div>
          <br>
          <div class="row">
            <div class="col-md-6">
              @app_file_w_modal_help(['field' => 'radiography13'
                                , 'patient' => $patient
                                , 'user_path' => $user_path])
                @endapp_file_w_modal_help
            </div>
            <div class="col-md-6">
              
            </div>
          </div>
          <br>
          <div class="row">
            <div class="col-md-12 bg-secondary">
              <h1 class="text-center my-1">@lang('app.dentalcase.patient.objective_title')</h1>
            </div>
          </div>
          <br>
          <div class="row">
            <div class="col-md-12">
             <div class="form-group"> <label>@lang('app.dentalcase.patient.objective_dentist')</label> 
                {{ Form::textarea('objective_dentist'
                                  ,  $patient->objective_dentist
                                  , ['size' => '4x5'
                                  , 'class' => 'form-control'
                                  , 'placeholder' => __('app.dentalcase.patient.objective_dentist')]) }}
              </div>
            </div>
          </div>
          <br>
          <div class="row">
            <div class="col-md-12">
             <div class="form-group"> <label>@lang('app.dentalcase.patient.objective_patient')</label> 
                {{ Form::textarea('objective_patient'
                                  ,  $patient->objective_patient
                                  , ['size' => '4x5'
                                  , 'class' => 'form-control'
                                  , 'placeholder' => __('app.dentalcase.patient.objective_patient')]) }}
              </div>
            </div>
           
          </div>
          
          <hr>
      </div>
    </div>


      <div class="container">
          <div class="row">
                <div class="col-md-12 text-center">
                  <div class="justify-content-center">
                    <br>
                    <a class="btn btn-danger text-white" href="{{ route('dentalcases.show', $dentalCase->id) }}">
                    {{ __('app.dentalcase.patient.show.come_back_to_show') }}</a>
                    {{ Form::button(__('app.dentalcase.patient.edit_btn'),['class' => 'btn btn-primary submit-btn', 'id' => 'submit-btn']) }}
                    <script type="text/javascript">
                      $('#submit-btn').on("click", function () {
                        $('#loading').modal('show');
                        $('.form-2-loading').submit();
                      });
                    </script>
                  </div>
                </div>
            </div>
          </div>
      </div>
    {!! Form::close(); !!}
@endsection
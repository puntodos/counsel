<div class="row">
  <div class="col-md-12 bg-secondary">
    <h3 class="text-center my-1">@lang('app.dentalcase.patient.show.conversation.title')</h3>
  </div>
</div>
<br>
<div class="py-5 text-center" >
    <div class="container py-5">
      <div class="row">
        <div class="col-md-12">
          <p class="lead mb-5">
          	@lang('app.dentalcase.patient.show.pending_payment.text')
          </p>
          <a class="btn btn-danger text-white" href="{{ route('dentalcases.payments', $dentalCase->id) }}">
                {{ __('app.dentalcase.patient.show.pending_payment.cta') }}</a>
        </div>
      </div>
    </div>
  </div>
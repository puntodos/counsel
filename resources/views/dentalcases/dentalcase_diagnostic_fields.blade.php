@foreach($diagnostic_json as $group)
		@php
			if($group['type'] == 'break'){
				continue;
			}
			$class = 'col';
			if($group['type'] == 'title'){
				$class = 'col-sm-12';
			}
			$cols = 0;
		@endphp

		<div class="row">
			<div class="{{ $class }}">
				<h3>
					<b>
						{{ $group['title'] }}
					</b>
				</h3>
			</div>
		</div>
		@if(isset($group['fields']) && count($group['fields']) > 0)
			<div class="row">
				@php
					$cols = 0;
				@endphp
			@foreach($group['fields'] as $field)
				@php
					if($field['type'] == 'break'){
						continue;
					}
					$class = 'col';
					if(isset($field['width'])){
						$class = 'col-sm-'. $field['width'];
						$cols += $field['width'];
					}
				@endphp
				<div class="col-sm-6">
					@if($field['type'] == 'radio')
						@app_diagnostic_radios_read(['field' => $field, 'diagnostic' => $diagnostic])
						@endapp_diagnostic_radios_read
					@endif
					@if($field['type'] == 'number')
						@php
						$value = '';
						if(isset($diagnostic[$field['name']])){
							$value = $diagnostic[$field['name']];
						}
						@endphp
						<h5><b>{{ $field['title'] }}</b></h5>
						<div class="form-group"> 
				            <label style="">{{ $value }}</label>
				        </div>
					@endif
					@if($field['type'] == 'check')
						<h5><b>{{ $field['title'] }}</b></h5>
						@php
						$value = 'NA';
						if(isset($diagnostic[$field['name']])){
							$value = $diagnostic[$field['name']];
							$value = '<ul><li>'.implode("</li><li>", $value).'</li></ul>';
						}
						@endphp
						
						@php
							echo $value;
						@endphp
					@endif

					@if(isset($field['fields']) && count($field['fields']) > 0)
						@foreach($field['fields'] as $intField)
							@if($intField['type'] == 'text')
								@php
								$value = '';
								if(isset($diagnostic[$intField['name']])){
									$value = $diagnostic[$intField['name']];
								}
								@endphp
								<br>
								<label><b>{{ $intField['title'] }}:</b>&nbsp;
									{{ $value }}
								</label>
							@endif
						@endforeach
					@endif
				</div>
				@if($cols == 12)
					@php	
						$cols = 0;
					@endphp
					</div>
					<hr>
					<div class="row">
				@endif
			@endforeach
			</div>
		@endif
		@if($cols > 0)
		<hr>
		@endif
	@endforeach

    
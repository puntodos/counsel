@extends('layouts.dentist_template')

@push('scripts')
<script src="{{ asset('js/dentalcases.js') }}" ></script>
@endpush

@section('content')
    @php
    //dd(session()->all());
    @endphp
    @if(count($dentalcases) == 0)
      <div class="py-5 text-center filter-fade-in" style="background-image: url(&quot;{{ asset(config('constants.options.dentis_create_background')) }}&quot;);">
        <div class="container py-5">
          <div class="row">
            <div class="col-md-12">
              <h1 class="display-3 mb-4 text-primary">@lang('app.deltalcases.welcome')</h1>
              <p class="lead mb-5">@lang('app.deltalcases.description')</p>
              <a href="{{ route('dentalcases.create') }}" class="btn btn-lg mx-1 btn-secondary">@lang('app.deltalcases.newcase')</a>
            </div>
          </div>
        </div>
      </div>
    @else
        <div class="container">
          <div class="row">
            <div class="col-md-12">
              <a href="{{ route('dentalcases.create') }}" class="btn btn-lg mx-1 btn-secondary">@lang('app.deltalcases.newcase')</a>
              &nbsp;
              <span class="badge badge-pill badge-danger">&nbsp;</span> @lang('app.dentalcase.list.unreadmessages')
            </div>
          </div>
          <div class="row">
            <div class="col-md-12">
              <br>
              <table class="table table-hover">
                <thead>
                  <tr class="table-active">
                    <th scope="col">@lang('app.dentalcase.table.id')</th>
                    <th scope="col">@lang('app.dentalcase.table.name')</th>
                    <th scope="col">@lang('app.dentalcase.table.date_begin')</th>
                    <th scope="col">@lang('app.dentalcase.table.dentist')</th>
                    <th scope="col">@lang('app.dentalcase.table.status')</th>
                  </tr>
                </thead>
                <tbody>
                  @foreach ($dentalcases as $dentalcase)
                  <tr scope="row" data-id="{{ $dentalcase->id }}">
                    <td class="dentalcase-show">{{ $dentalcase->case_by_dentist }}</td>
                    <td class="dentalcase-show">
                      @if($dentalcase->messages_unread_4_dentist > 0)
                          <span class="badge badge-pill badge-danger">
                            {{ $dentalcase->messages_unread_4_dentist }}
                          </span>
                      @endif
                      {{ $dentalcase->patient->name }}
                    </td>
                    <td class="dentalcase-show">{{ $dentalcase->created_at }}</td>
                    <td class="dentalcase-show">{{ $dentalcase->dentist->user->name }}</td>
                    <td class="dentalcase-show">
                      @if($dentalcase->status == 'pending')
                        <a class="btn btn-danger btn-sm text-white" 
                        {{-- href="{{ route('dentalcases.payments', $dentalcase->id) }}" --}}
                        > @lang('app.deltalcases.status.'.$dentalcase->status)</a>
                      @else
                        @lang('app.deltalcases.status.'.$dentalcase->status)
                      @endif
                    </td>
                  </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
          </div>
        </div>
      
    @endif
@endsection
<div class="row">
  <div class="col-md-12 bg-secondary">
    <h3 class="text-center my-1">@lang('app.dentalcase.patient.show.conversation.title')</h3>
  </div>
</div>
<div class="row">
  <div class="col-md-12 message_time_line" style="overflow-y: scroll; height: 500px;">
    <!-- Mensajes Begin-->
    @app_messages(['messages' => $messages, 'user_path' => $user_path.'/'])
    @endapp_messages
    <!-- Mensajes End -->
  </div>
</div>
<hr>

@if($dentalCase->status == 'active')

<div class="row">
  <div class="col-md-12 ">
   <a class="btn btn-secondary text-white" data-toggle="modal" data-target=".modal-add-message">
{{ __('app.dentalcase.patient.show.message') }}</a>
  @if($dentalCase->messages_unread_4_dentist > 0)
&nbsp;
<a class="btn btn-primary text-white" href="{{ route('dentalcase.mark_as_read', $dentalCase->id) }}">
                    {{ __('app.admin.deltalcases.mark_as_read') }}
                  </a>
  @endif
  </div>
</div>

</div>

{!! Form::open(['route' =>  ['messages.store', 'dentalcase_id' => $dentalCase->id], 'method' => 'POST', 'files' => true]) !!}
      @csrf
<div class="modal fade modal-add-message"  tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">@lang('app.dentalcase.patient.show.message.new')</h5>
        <button type="button" class="close message_discard" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        
          <div class="form-group">
            <label for="message-text" class="col-form-label">
              @lang('app.dentalcase.patient.show.message.field_message')
            </label>
            <textarea class="form-control" name="text" cols="4" rows="5" id="text"></textarea>
            <br>
             @app_file_w_modal(['field' => 'file_attach'])
                @endapp_file_w_modal
          </div>
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary message_discard" >
          @lang('app.dentalcase.patient.show.message.field_discard')
        </button>
        <script type="text/javascript">
          $('.message_discard').on('click' , function(){
            $('#text').val('');
            $('#attach_file_attach_resume').val('');
            $('.modal-add-message').modal('hide')
          });
        </script>
        <input type="submit" class="btn btn-primary" value="@lang('app.dentalcase.patient.show.message.field_send')">
      </div>
    </div>
  </div>
</div>
{!! Form::close(); !!}

@endif

@extends('layouts.admin_template')

@push('scripts')
<script src="{{ asset('js/bootstrap-datepicker.min.js') }}" ></script>
<script src="{{ asset('js/dashboard.js?time=').date('Ymdhis') }}" ></script>
@endpush

@section('content')

    @include('dentalcases.files_partial')



    <div class="modal fade" id="active" role="dialog">
      <div class="modal-dialog modal-dialog-centered">
        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <h3 class="modal-title"><b>@lang('app.admin.deltalcases.status.action.active')</b></h3>
            <button type="button" class="close" data-dismiss="modal">&times;</button>
          </div>
          <div class="modal-body">
            <div class="container">
              <div class="row">
                {!! Form::open(['route' => ['dashboard.dentalcase.changestatus', $dentalCase], 'method' => 'PUT']) !!}
                @csrf
                <div class="form-group">
                  <label for="message-text" class="col-form-label">
                    @lang('app.dentalcase.payment.plan.select')
                  </label>
                  {{-- 'plan3' => __('app.dentalcase.payment.plan3') --}}
                  {!! Form::select('plan'
                                , ['plan1' => __('app.dentalcase.payment.plan1')
                                ,  'plan2' => __('app.dentalcase.payment.plan2')
                                ,  ]
                                , $dentalCase->plan
                                , ['placeholder' => __('app.dentalcase.payment.plan.select'),
                                  'class' => 'form-control'
                                , 'required'])
                  !!}
                  <label for="message-text" class="col-form-label">
                    @lang('app.admin.deltalcases.status.action.active.date_begin')
                  </label>
                  <div class="input-group date datepicker" data-provide="datepicker">
                      <input type="text" class="form-control datepicker" id="begin_date" name="begin_date"
                        placeholder="{{ __('app.admin.deltalcases.status.action.active.date_begin') }}" >
                      <div class="input-group-addon">
                          <span class="glyphicon glyphicon-th"></span>
                      </div>
                  </div>
                  <br>
                  <label for="message-text" class="col-form-label">
                    @lang('app.admin.deltalcases.status.action.active.date')
                  </label>
                  <input type="hidden" name="status" value="active">
                  <div class="input-group date datepicker" data-provide="datepicker">
                      <input type="text" class="form-control datepicker" id="ended_date" name="ended_date"
                        placeholder="{{  __('app.admin.deltalcases.status.action.active.date') }}" >
                      <div class="input-group-addon">
                          <span class="glyphicon glyphicon-th"></span>
                      </div>
                  </div>
                  <br>
                  <input type="submit" class="btn btn-primary" value="@lang('app.admin.deltalcases.status.action.active.action')">
                   
                </div>
                {!! Form::close() !!}
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="modal fade" id="pending" role="dialog">
      <div class="modal-dialog modal-dialog-centered">
        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <h3 class="modal-title"><b>@lang('app.admin.deltalcases.status.action.pending')</b></h3>
            <button type="button" class="close" data-dismiss="modal">&times;</button>
          </div>
          <div class="modal-body">
            <div class="container">
              <div class="row">
                {!! Form::open(['route' => ['dashboard.dentalcase.changestatus', $dentalCase], 'method' => 'PUT']) !!}
                @csrf
                <div class="form-group">
                  <label for="message-text" class="col-form-label">
                    @lang('app.admin.deltalcases.status.pending.confirm')
                  </label>
                  <input type="hidden" name="status" value="pending">
                  <br>
                  <input type="submit" class="btn btn-primary" value="@lang('app.admin.deltalcases.status.action.pending')">
                   
                </div>
                {!! Form::close() !!}
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="modal fade" id="ended" role="dialog">
      <div class="modal-dialog modal-dialog-centered">
        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <h3 class="modal-title"><b>@lang('app.admin.deltalcases.status.action.ended')</b></h3>
            <button type="button" class="close" data-dismiss="modal">&times;</button>
          </div>
          <div class="modal-body">
            <div class="container">
              <div class="row">
                {!! Form::open(['route' => ['dashboard.dentalcase.changestatus', $dentalCase], 'method' => 'PUT']) !!}
                @csrf
                <div class="form-group">
                  <label for="message-text" class="col-form-label">
                    @lang('app.admin.deltalcases.status.ended.confirm')
                  </label>
                  <input type="hidden" name="status" value="ended">
                  <br>
                  <input type="submit" class="btn btn-primary" value="@lang('app.admin.deltalcases.status.action.ended')">
                   
                </div>
                {!! Form::close() !!}
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class="row">
            <div class="col-md-6 bg-secondary">
              <h3 class="my-1">
                @lang('app.dentalcase.patient.show.trate') {{ $patient->name }} 
                (@lang('app.admin.deltalcases.status.'.$dentalCase->status))<br>
                ID: {{ $dentalCase->case_by_dentist }}
              </h3>
            </div>
             
             <div class="col-md-6 bg-secondary">
                <br>
                @if($dentalCase->status == 'pending' || $dentalCase->status == 'ended')
                  <a class="btn btn-primary text-white btn-sm float-sm-right" data-toggle="modal" data-target="#active" style="margin: 1px">
                    {{ __('app.dentalcase.patient.show.active_case') }}
                  </a>
                @endif
                @if($dentalCase->status == 'active')
                  <a class="btn btn-primary text-white btn-sm float-sm-right" data-toggle="modal" data-target="#ended" style="margin: 1px">
                    {{ __('app.dentalcase.patient.show.ended_case') }}
                  </a>
                @endif
                @if($dentalCase->status != 'pending')
                  <a class="btn btn-primary text-white btn-sm float-sm-right" data-toggle="modal" data-target="#pending" style="margin: 1px">
                    {{ __('app.dentalcase.patient.show.pending_case') }}
                  </a>
                @endif

                 @if($dentalCase->status == 'active' || $diagnostic_exists)
                  <a class="btn btn-primary text-white btn-sm float-sm-right" href="{{ route('treatmentplan.show', $dentalCase->id) }}" style="margin: 1px" target="_blank">
                    {{ __('app.dentalcase.patient.show.make_treatmentplan') }}
                  </a>
                @endif
                
                @if($dentalCase->status == 'active' || $diagnostic_exists)
                  <a class="btn btn-primary text-white btn-sm float-sm-right" href="{{ route('diagnostic.show', $dentalCase->id) }}" style="margin: 1px" target="_blank">
                    {{ __('app.dentalcase.patient.show.make_diagnostic') }}
                  </a>
                @endif
                
            </div>
          </div>
          <br>
          <div class="row">
            <div class="col-md-4 ">
             <table class="table table-striped table-sm">
              <thead>
                <tr>
                  <th scope="col">@lang('app.dentalcase.patient.name')</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>{{ $patient->name }}</td>
                </tr>
              </tbody>
            </table> 
              <table class="table table-striped table-sm">
                <thead>
                  <tr>
                    <th scope="col">@lang('app.dentalcase.patient.age')</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td>{{ $patient->age }}</td>
                  </tr>
                </tbody>
              </table>
              <table class="table table-striped table-sm">
                <thead>
                  <tr>
                    <th scope="col">@lang('app.dentalcase.patient.genere')</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td>@lang('app.dentalcase.patient.genere.'.$patient->genere )</td>
                  </tr>
                </tbody>
              </table>   
              @if($patient->objective_patient != "")           
              <table class="table table-striped table-sm">
                <thead>
                  <tr>
                    <th scope="col">@lang('app.dentalcase.patient.objective_patient')</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td>{{ $patient->objective_patient}}</td>
                  </tr>
                </tbody>
              </table>
              @endif
              @if($patient->objective_dentist != "")           
              <table class="table table-striped table-sm">
                <thead>
                  <tr>
                    <th scope="col">@lang('app.dentalcase.patient.objective_dentist')</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td>{{ $patient->objective_dentist}}</td>
                  </tr>
                </tbody>
              </table>
              @endif
              @if($dentalCase->begin_date != "")           
              <table class="table table-striped table-sm">
                <thead>
                  <tr>
                    <th scope="col">@lang('app.dentalcase.patient.begin_date')</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td>{{ $dentalCase->begin_date}}</td>
                  </tr>
                </tbody>
              </table>
              <table class="table table-striped table-sm">
                <thead>
                  <tr>
                    <th scope="col">@lang('app.dentalcase.patient.end_date')</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td>{{ $dentalCase->ended_date}}</td>
                  </tr>
                </tbody>
              </table>
              @endif
              @if($dentalCase->plan != "")           
              <table class="table table-striped table-sm">
                <thead>
                  <tr>
                    <th scope="col">@lang('app.dentalcase.payment.plan.title')</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td>@lang('app.dentalcase.payment.'.$dentalCase->plan)</td>
                  </tr>
                </tbody>
              </table>
              @endif
              <a class="btn btn-primary text-white" data-toggle="modal" data-target=".file-list-modal">
              {{ __('app.dentalcase.patient.show.files') }}</a>
            </div>
            <div class="col-md-8 ">
              
              @include('dashboard.message_partial')

            </div>
          </div>
          <br>
          
          
          <hr>
      </div>
    </div>

@endsection
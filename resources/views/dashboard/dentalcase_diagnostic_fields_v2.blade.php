{!! Form::open(['route' => ['diagnostic.store', $dentalCase], 'method' => 'POST', 'files' => true]) !!}
@csrf

	@foreach($diagnostic_json as $group)
		@php
			if($group['type'] == 'break'){
				continue;
			}
			$class = 'col';
			if($group['type'] == 'title'){
				$class = 'col-sm-12';
			}
			$cols = 0;
		@endphp

		<div class="row">
			<div class="{{ $class }}">
				<h3>
					<b>
						{{ $group['title'] }}
					</b>
				</h3>
			</div>
		</div>
		@if(isset($group['fields']) && count($group['fields']) > 0)
			<div class="row">
				@php
					$cols = 0;
				@endphp
			@foreach($group['fields'] as $field)
				@php
					if($field['type'] == 'break'){
						continue;
					}
					$class = 'col';
					if(isset($field['width'])){
						$class = 'col-sm-'. $field['width'];
						$cols += $field['width'];
					}
				@endphp
				<div class="col-sm-6">
					@if($field['type'] == 'radio')
						@app_diagnostic_radios_new(['field' => $field, 'diagnostic' => $diagnostic])
						@endapp_diagnostic_radios_new
					@endif
					@if($field['type'] == 'number')
						@php
						$value = '';
						if(isset($diagnostic[$field['name']])){
							$value = $diagnostic[$field['name']];
						}
						@endphp
						<h5><b>{{ $field['title'] }}</b></h5>
						<p>{{ $field['description'] }}</p>
						<div class="form-group"> 
				            <label style="">{{ $field['description2'] }}</label>
				            <input class="form-control" placeholder="{{ $field['placeholder'] }}" required="" name="{{ $field['name'] }}" type="number" step="{{ $field['step'] }}" value="{{ $value }}">
				        </div>
					@endif
					@if($field['type'] == 'check')
						@app_diagnostic_check(['field' => $field, 'diagnostic' => $diagnostic])
						@endapp_diagnostic_check
					@endif

					@if(isset($field['fields']) && count($field['fields']) > 0)
						@foreach($field['fields'] as $intField)
							@if($intField['type'] == 'text')
								@php
								$value = '';
								if(isset($diagnostic[$intField['name']])){
									$value = $diagnostic[$intField['name']];
								}
								@endphp
								<div class="form-group"> 
						            <input class="form-control" placeholder="{{ $intField['placeholder'] }}" name="{{ $intField['name'] }}" type="text" value="{{ $value }}">
						        </div>
							@endif
						@endforeach
					@endif
				</div>
				@if($cols == 12)
					@php	
						$cols = 0;
					@endphp
					</div>
					<hr>
					<div class="row">
				@endif
			@endforeach
			</div>
		@endif
		@if($cols > 0)
		<hr>
		@endif
	@endforeach

	<div class="container">
	  	<div class="row">
	        <div class="col-md-12 text-center">
	          <div class="justify-content-center">
	            <br>
	            {{ Form::submit(__('app.admin.deltalcase.diagnostic.save_btn'),['class' => 'btn btn-primary']) }}
	          </div>
	        </div>
	    </div>
	 </div>
    {!! Form::close(); !!}

@extends('layouts.admin_template')

@push('scripts')
<script src="{{ asset('js/dashboard.js') }}" ></script>
@endpush

@section('content')
@include('flash::message')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">@lang('app.dentist.changepassword')</div>

                <div class="card-body">
                    {!! Form::open(['route' => ['dashboards.users.edit_update', $dentist], 'method' => 'PUT', 'files' => true]) !!}
                        @csrf

                        <input type="hidden" name="token" value="{{ $token }}">

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('app.dentist.newpassword') }}</label>

                            <div class="col-md-6">
                                <input id="newpassword" type="password" class="form-control{{ $errors->has('newpassword') ? ' is-invalid' : '' }}" name="newpassword" required>

                                @if ($errors->has('newpassword'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('newpassword') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password-confirm" class="col-md-4 col-form-label text-md-right">{{ __('app.dentist.newpassword2') }}</label>
                            <div class="col-md-6">
                                <input id="newpassword2" type="password" class="form-control{{ $errors->has('newpassword2') ? ' is-invalid' : '' }}" name="newpassword2" required>

                                @if ($errors->has('newpassword2'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('newpassword2') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('app.dentist.changepassword') }}
                                </button>
                            </div>
                        </div>
                    {!! Form::close(); !!}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection


@extends('layouts.admin_template')

@push('scripts')
<script src="{{ asset('js/dashboard.js') }}" ></script>
@endpush

@section('content')
 
        <div class="container">
          <div class="row">
            <div class="col-md-12 bg-secondary">
              <h1 class="text-center my-1">@lang('app.admin.menu.option2')</h1>
            </div>
          </div>
          <br>
          <div class="row">
            <div class="col-md-12">
              <span class="badge badge-pill badge-danger">&nbsp;</span> @lang('app.admin.list.dentist.unreadcases')
            </div>
          </div>
          <div class="row">
            <div class="col-md-12">
              <br>
              <table class="table table-hover">
                <thead>
                  <tr class="table-active">
                    <th scope="col">@lang('app.admin.list.dentist.id')</th>
                    <th scope="col">@lang('app.admin.list.dentist.dentist')</th>
                    <th scope="col">@lang('app.admin.list.dentist.email')</th>
                    <th scope="col">@lang('app.admin.list.dentist.country')</th>
                    <th scope="col">@lang('app.admin.list.dentist.cases')</th>
                  </tr>
                </thead>
                <tbody>
                  @foreach ($dentists as $dentist)
                  <tr scope="row" data-id="{{ $dentist->id }}" data-href="{{ route('dashboard.list.dentist' , $dentist->id) }}">
                    <td class="dentist-show">{{ $dentist->id }}</td>
                    <td class="dentist-show">
                      @if(count($dentist->dentalcases_unread) > 0)
                          <span class="badge badge-pill badge-danger">{{ count($dentist->dentalcases_unread) }}</span>
                      @endif
                      {{ $dentist->user->name }}
                    </td>
                    <td class="dentist-show">{{ $dentist->user->email }}</td>
                    <td class="dentist-show">{{ $dentist->country->name }}</td>
                    <td class="dentist-show">{{ count($dentist->dentalcases) }}</td>
                  </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
          </div>
        </div>
      
    
    
@endsection
@extends('layouts.admin_template')


@section('content')

   <div class="main-section">
    
    <a href="{{ route('dashboard.listdentist') }}">
    <div class="dashbord dashbord-green">
      <div class="icon-section">
        <i class="fa fa-stethoscope" aria-hidden="true"></i><br>
        <small>@lang('app.admin.menu.option2')</small>
        <p>{{ $dentists }}</p>
      </div>
      <div class="detail-section">
        @lang('app.admin.menu.seemore')
      </div>
    </div>
    </a>

    <a href="{{ route('dashboard.list.dentalcases') }}">
    <div class="dashbord dashbord-orange">
      <div class="icon-section">
        <i class="fa fa-bell" aria-hidden="true"></i><br>
        <small>@lang('app.admin.menu.option3')</small>
        <p>{{ $dentalCases }}</p>
      </div>
      <div class="detail-section">
        @lang('app.admin.menu.seemore')
      </div>
    </div>
    </a>

    <a href="{{ route('dashboard.list.dentalcases.actives') }}">
    <div class="dashbord dashbord-blue">
      <div class="icon-section">
        <i class="fa fa-tasks" aria-hidden="true"></i><br>
        <small>@lang('app.admin.menu.option4')</small>
        <p>{{ $dentalCasesActive }}</p>
      </div>
      <div class="detail-section">
        @lang('app.admin.menu.seemore')
      </div>
    </div>
    </a>

    <a href="{{ route('dashboard.list.dentalcases.ended') }}">
    <div class="dashbord dashbord-red">
      <div class="icon-section">
        <i class="fa fa-tasks" aria-hidden="true"></i><br>
        <small>@lang('app.admin.menu.option5')</small>
        <p>{{ $dentalCasesEnded }}</p>
      </div>
      <div class="detail-section">
        @lang('app.admin.menu.seemore')
      </div>
    </div>
    </a>

    <a href="{{ route('dashboard.list.dentalcases.pending') }}">
    <div class="dashbord dashbord-skyblue">
      <div class="icon-section">
        <i class="fa fa-tasks" aria-hidden="true"></i><br>
        <small>@lang('app.admin.menu.option8')</small>
        <p>{{ $dentalCasesPending }}</p>
      </div>
      <div class="detail-section">
        @lang('app.admin.menu.seemore')
      </div>
    </div>
    </a>

    <a href="{{ route('dashboard.list.dentalcases.unread') }}">
    <div class="dashbord">
      <div class="icon-section">
        <i class="fa fa-tasks" aria-hidden="true"></i><br>
        <small>@lang('app.admin.menu.option6')</small>
        <p>{{ $dentalCasesRead }}</p>
      </div>
      <div class="detail-section">
        @lang('app.admin.menu.seemore')
      </div>
    </div>
    </a>

    <a href="{{ route('dashboard.list.dentalcases.unread.messages') }}">
    <div class="dashbord dashbord-green">
      <div class="icon-section">
        <i class="fa fa-comments" aria-hidden="true"></i><br>
        <small>@lang('app.admin.menu.option9')</small>
        <p>{{ $dentalCaseswithUnReadMessages }}</p>
      </div>
      <div class="detail-section">
        @lang('app.admin.menu.seemore')
      </div>
    </div>
    </a>

    <a href="{{ route('dashboards.users') }}">
    <div class="dashbord dashbord-orange">
      <div class="icon-section">
        <i class="fa fa-user" aria-hidden="true"></i><br>
        <small>@lang('app.admin.menu.option7')</small>
        <p>{{ $users }}</p>
      </div>
      <div class="detail-section">
        @lang('app.admin.menu.seemore')
      </div>
    </div>
    </a>
    
  </div>
    
@endsection
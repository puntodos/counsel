<br>  

          <div class="row">
            <div class="col-md-12">
              <span class="badge badge-pill badge-danger">&nbsp;</span> @lang('app.admin.list.dentist.unreadcases')<br>
            </div>
          </div>
          @if($search != 'NA003')
          {!! Form::open(['url' => 'dashboard/list/dentalcases/'.$status, 'method' => 'GET', 'class' => 'form-2-loading']) !!}
          @csrf
          <div class="row">
            <div class="col-md-4">
                <input type="text"  name="search" placeholder="{{ __('app.admin.list.dentist.search.title') }}">
              <button type="submit" class="btn btn-primary">
                  {{ __('app.admin.list.dentist.search') }}
              </button>
            </div>
            @if($search != '')
            <div class="col-md-8">
                <p>
                  <h5 style="font-weight: bold;">{{ __('app.admin.list.dentist.search.results') }} &quot;{{ $search }}&quot;</h5>
                </p>
            </div>
            @endif
          </div>
          {!! Form::close(); !!}
          @endif
          
          <div class="row">
            <div class="col-md-12">
              <br>
              <table class="table table-hover">
                <thead>
                  <tr class="table-active">
                    <th scope="col">@lang('app.admin.list.dentist.show.cases.patient.id')</th>
                    <th scope="col">@lang('app.admin.list.dentist.show.cases.patient.name')</th>
                    <th scope="col">@lang('app.admin.list.dentist.show.cases.patient.date_begin')</th>
                    <th scope="col">@lang('app.admin.list.dentist.show.cases.patient.dentist_name')</th>
                    <th scope="col">@lang('app.admin.list.dentist.show.cases.patient.status')
                    </th>

                  </tr>
                </thead>
                <tbody>
                  @foreach ($dentalcases as $dentalcase)
                  <tr scope="row" data-id="{{ $dentalcase->id }}" 
                    data-href='{{ route('dashboard.dentalcase', $dentalcase->id) }}'>
                    <td class="dentist-show">{{ $dentalcase->id }}</td>
                    <td class="dentist-show"> 
                        @if($dentalcase->status_read == 'unread')
                          <span class="badge badge-pill badge-danger">&nbsp;</span>
                        @elseif($dentalcase->messages_unread_4_admin > 0)
                          <span class="badge badge-pill badge-danger">&nbsp;</span>
                        @endif
                      {{ $dentalcase->patient->name }}
                    </td>
                    <td class="dentist-show">{{ $dentalcase->patient->created_at }}</td>
                    <td class="dentist-show">{{ $dentalcase->dentist->user->name }}</td>
                    <td class="dentist-show">
                      @lang('app.deltalcases.status.admin.'.$dentalcase->status)
                    </td>
                  </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
          
        </div>
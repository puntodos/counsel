
@extends('layouts.admin_template')

@push('scripts')
<script src="{{ asset('js/dashboard.js') }}" ></script>
@endpush

@section('content')

    @include('dentalcases.files_partial')

    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class="row">
            <div class="col-md-6 bg-secondary">
              <h3 class="my-1">
                @lang('app.dentalcase.patient.show.trate') {{ $patient->name }} 
                (@lang('app.admin.deltalcases.status.'.$dentalCase->status))<br>
                ID: {{ $dentalCase->case_by_dentist }}
              </h3>
            </div>
             <div class="col-md-2 bg-secondary">
              
            </div>
             <div class="col-md-4 bg-secondary">
                <br>
               
                  <a class="btn btn-warning btn-sm float-sm-right" style="margin: 1px" href="{{ route('dashboard.dentalcase', $dentalCase->id) }}">
                    {{ __('app.general.back_btn') }}
                  </a>
                  @if(count($diagnostic) > 0 && $dentalCase['diagnostic']['sndiagnostic'] != "N")
                  <a class="btn btn-dark btn-sm float-sm-right" style="margin: 1px" href="{{ route('diagnostic.pdf', $dentalCase['diagnostic']['id']) }}">
                    {{ __('app.admin.deltalcase.diagnostic.pdf.btn') }}
                  </a>
                  @endif
                
                
            </div>
          </div>
          <br>
          <div class="row">
            <div class="col-md-12 bg-secondary text-center">
              <h3 class="my-1">
                @lang('app.admin.deltalcase.diagnostic.title')
              </h3>
            </div>            
          </div>
          <br>
          <div class="row">
            <div class="col-md-12 ">
              
              @include('dashboard.dentalcase_diagnostic_fields_v2')

            </div>
          </div>
          <br>
          
          
          <hr>
      </div>
    </div>
  </div>
@endsection
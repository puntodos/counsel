<br>  
          <div class="row">
            <div class="col-md-12">
              <br>
              <table class="table table-hover">
                <thead>
                  <tr class="table-active">
                    <th scope="col">@lang('app.admin.list.users.id')</th>
                    <th scope="col">@lang('app.admin.list.users.name')</th>
                    <th scope="col">@lang('app.admin.list.users.email')</th>
                    <th scope="col">@lang('app.admin.list.users.action')</th>
                  </tr>
                </thead>
                <tbody>
                  @foreach ($users as $user)
                  <tr scope="row" data-id="{{ $user->id }}" 
                    data-href='{{ route('dashboards.users.edit', $user->id) }}'>
                    <td class="dentist-show">{{ $user->id }}</td>
                    <td class="dentist-show"> 
                      {{ $user->name }}
                    </td>
                    <td class="dentist-show">{{ $user->email }}</td>
                    <td class="dentist-show">
                      <button type="button" class="btn btn-default btn-sm btn-success">
                        <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span> Editar
                      </button>
                    </td>
                  </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
          
        </div>
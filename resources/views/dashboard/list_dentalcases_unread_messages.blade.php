@extends('layouts.admin_template')

@push('scripts')
<script src="{{ asset('js/dashboard.js') }}" ></script>
@endpush

@section('content')

        <br>  
        <div class="container">
          <div class="row">
            <div class="col-md-12 bg-secondary">
              <h1 class="text-center my-1">@lang('app.admin.menu.option9')</h1>
            </div>
          </div>

          @include('dashboard.dentalcases_table')
          
        </div>
      
    
    
@endsection
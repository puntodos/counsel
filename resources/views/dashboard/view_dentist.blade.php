@extends('layouts.admin_template')

@push('scripts')
<script src="{{ asset('js/dashboard.js') }}" ></script>
@endpush

@section('content')

     
<div class="container">
    <div class="row">
        <div class="col-md-12">
          <div class="row">
            <div class="col-md-12 bg-secondary">
              <h1 class="text-center my-1">@lang('app.dentist.personal_profile_title')</h1>
            </div>
          </div>
          <br>
          <div class="row">
            <div class="col-md-4">
              @if ($dentist->file_thumb == "")
                <img src="{{ asset(config('constants.options.dentist_profile')) }}" 
                      class="img-fluid attach_btn_img"
                     >
              @else
                <img src="{{ asset($user_path . '/' . $dentist->file_thumb) }}" class="img-fluid"
                      class="img-fluid attach_btn_img"
                >
              @endif
              
             </div>
            <div class="col-md-4">
              <div class="form-group"> 
                @app_label(['field' => 'name'])
                @endapp_label
                  {!! Form::text('name'
                              , $user->name
                              , ['class' => 'form-control'
                              , 'placeholder' => __('app.dentist.name')
                              , 'readonly' => 'readonly'
                              , 'required'])
                 !!}
                </div>
              <div class="form-group"> 
                @app_label(['field' => 'cell_phone'])
                @endapp_label
                {!! Form::text('cell_phone'
                              , $dentist->cell_phone
                              , ['class' => 'form-control'
                              , 'placeholder' => __('app.dentist.cell_phone')
                              , 'readonly' => 'readonly'
                              , 'required'])
                 !!}
              </div>
              <div class="form-group"> 
               @app_label(['field' => 'country'])
                @endapp_label
                {!! Form::select('country'
                                , $countries
                                , $dentist->country_id
                                , [
                                    'placeholder' => __('app.dentist.country')
                                    ,'class' => 'form-control'
                                    ,'id'=>'name'
                                    , 'required'
                                    , 'readonly' => 'readonly'
                                  ])
              !!}
                      

               
              </div>
            </div>
            <div class="col-md-4">
              <div class="form-group"> <label>@lang('app.dentist.email')</label>
                {!! Form::email(null
                              , $user->email
                              , ['class' => 'form-control'
                              , 'placeholder' => __('app.dentist.email')
                              , 'readonly' => 'readonly'])
                 !!}
               </div>
              <div class="form-group"> 
                @app_label(['field' => 'city'])
                @endapp_label
                {!! Form::text('city'
                              , $dentist->city
                              , ['class' => 'form-control'
                              , 'placeholder' => __('app.dentist.city')
                              , 'required'
                              , 'readonly' => 'readonly'
                              ])
                 !!}
                 </div>
              <div class="form-group"> <label>@lang('app.dentist.company')</label>
                {!! Form::text('company'
                              , $dentist->company
                              , ['class' => 'form-control'
                              , 'placeholder' => __('app.dentist.company')
                              , 'readonly' => 'readonly'
                              ])
                 !!}
               </div>
            </div>
          </div>
          <br>
          <div class="row">
            <div class="col-md-12 bg-secondary">
              <h1 class="text-center my-1">@lang('app.dentist.profile_title')</h1>
            </div>
          </div>
          <br>
          <div class="row">
            <div class="col-md-6">
              <div class="form-group"> <label>@lang('app.dentist.document_number')</label>
                {!! Form::text('document_number'
                              , $dentist->document_number
                              , ['class' => 'form-control'
                              , 'placeholder' => __('app.dentist.document_number')
                              , 'readonly' => 'readonly'
                              ])
                 !!}
              </div>
              <div class="form-group"> <label>@lang('app.dentist.associations')</label> 
                {{ Form::textarea('associations'
                                  ,  $dentist->associations
                                  , ['size' => '4x5'
                                  , 'class' => 'form-control'
                                  , 'readonly' => 'readonly'
                                  , 'placeholder' => __('app.dentist.associations')]) }}
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group"> <label>@lang('app.dentist.supports_institution')</label>
                {!! Form::text('supports_institution'
                              , $dentist->supports_institution
                              , ['class' => 'form-control'
                              , 'readonly' => 'readonly'
                              , 'placeholder' => __('app.dentist.supports_institution')
                              ])
                 !!}
              </div>
              <div class="form-group">
                @app_label(['field' => 'university'])
                @endapp_label
                {!! Form::text('university'
                              , $dentist->university
                              , ['class' => 'form-control'
                              , 'readonly' => 'readonly'
                              , 'placeholder' => __('app.dentist.university')
                              , 'required'
                              ])
                 !!}
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-12">
              <div class="form-group"> <label>@lang('app.dentist.specialities')</label> </div>
            </div>
          </div>
          <div class="row">
            @foreach ($specialities as $speciality)
              @php
                $selected = false;
                if(in_array($speciality->id, $dentistxSpecialitiesIds)):
                  $selected = true;
                endif;
              @endphp
            <div class="col-md-3">
              <div class="form-group">
                {{  Form::checkbox('specialities[]', $speciality->id , $selected, ['readonly' => 'readonly']) }}
                <label>{{ $speciality->name }}</label> </div>
            </div>
            @endforeach
          </div>
          <div class="row">
            <div class="col-md-12">
              <label> @lang('app.dentist.file_document')</label> <br>
            </div>
            <div class="col-md-12">
              <div class="form-inline">
                
                  @if($dentist->file_document != "")
                  {!! Form::button(__('app.dentist.file_document_btn_view')
                                  , ['class' => 'btn btn-primary'
                                  , 'onclick' => 'window.open("'.$user_path . '/' . $dentist->file_document.'")'
                                  ]) !!}
                  @else
                    <b>@lang('app.dentalcase.patient.file_attach_error')</b>
                  @endif
                {{ Form::file('file_document', ['id' => 'file_document'
                                              , 'style' => 'display:none'
                                              ]) }}
              
            </div>
                
            </div>
          </div>
          <hr>
      </div>
</div>


        <br>  
        <div class="container">
          <div class="row">
            <div class="col-md-12 bg-secondary">
              <h1 class="text-center my-1">@lang('app.dentist.cases')</h1>
            </div>
          </div>

          @include('dashboard.dentalcases_table')
          
        </div>
      
@endsection
@extends('layouts.dentist_template')


@section('content')
 
    {!! Form::open(['route' => ['dentist.update', $dentist], 'method' => 'PUT', 'files' => true]) !!}

      @csrf
      @include('dentist.form')
      <div class="container">
          <div class="row">
                <div class="col-md-12 text-center">
                  <div class="justify-content-center">
                    @app_label_error_alone(['field' => 'tyc'])
                    @endapp_label_error_alone
                    <br>
                    @if($dentist->tyc == 1)
                    {{ Form::checkbox('tyc', 'on', true) }} {{ __('app.dentist.tyc') }}
                    @else
                    {{ Form::checkbox('tyc', 'on') }} {{ __('app.dentist.tyc') }}
                    @endif
                    {{ Form::submit(__('app.dentist.create_btn'),['class' => 'btn btn-primary']) }}
                  </div>
                </div>
            </div>
          </div>
      </div>
    {!! Form::close(); !!}
@endsection
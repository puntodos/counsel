@php
//  dd($diagnostic); 
@endphp
<html>
<head>
  <style>
    body{
      font-family: sans-serif;
    }
    @page {
      margin: 160px 50px;
    }
    header { position: fixed;
      left: 0px;
      top: -160px;
      right: 0px;
      height: 100px;
      text-align: center;
    }
    header h1{
      margin: 10px 0;
    }
    header h2{
      margin: 0 0 10px 0;
    }
    footer {
      position: fixed;
      left: 0px;
      bottom: -50px;
      right: 0px;
      height: 40px;
      border-bottom: 2px solid #ddd;
    }
    footer .page:after {
      content: counter(page);
    }
    footer table {
      width: 100%;
    }
    footer p {
      text-align: right;
    }
    footer .izq {
      text-align: left;
    }

    .title-center { position: fixed;
      
      
      text-align: center;
      text-transform: uppercase;
    }
    .table, .td, .th {
        border: 1px solid black;
        text-align: center;
    }

    .table {
        border-collapse: collapse;
        width: 100%;
    }

    .th {
        height: 30px;
    }
  </style>
<body>
  <header>
    <br>
    <br>
    {{-- <img width="220px" src="{{ asset(config('constants.options.logo2')) }}"> --}}
  </header>
  <footer>
    <table>
      <tr>
        <td>
            <p class="izq">
              counselorthodontist.com
            </p>
        </td>
        <td>
          <p class="page">
            Página
          </p>
        </td>
      </tr>
    </table>
  </footer>
  <div id="content">
    <div class="title-center">
      <h3>@lang('app.admin.deltalcase.treatment.title')</h3>
    </div>
    <br><br><br><br>
    <table class="table">
      <tr class="tr">
        <th class="th" colspan="3">@lang('app.admin.deltalcase.diagnostic.pdf.patient.title')</th>
      </tr>
      <tr class="tr">
        <th class="th">@lang('app.admin.deltalcase.diagnostic.pdf.name')</th>
        <th class="th">@lang('app.admin.deltalcase.diagnostic.pdf.age')</th>
        <th class="th">@lang('app.admin.deltalcase.diagnostic.pdf.genere')</th>
      </tr>
      <tr class="tr">
        <td class="td">{{ $patient->name }}</td>
        <td class="td">{{ $patient->age }}</td>
        <td class="td">@lang('app.admin.deltalcase.diagnostic.pdf.genere.'.$patient->genere)</td>
      </tr>
    </table>
    <br><br><br>
    <table class="table">
      <tr class="tr">
        <th class="th" colspan="4">@lang('app.dentalcase.table.dentist')</th>
      </tr>
      <tr class="tr">
        <th class="th">@lang('app.admin.deltalcase.diagnostic.pdf.dentist.name')</th>
        <th class="th">@lang('app.dentalcase.table.date_begin')</th>
        <th class="th">@lang('app.dentalcase.table.date_end')</th>
        <th class="th">@lang('app.dentalcase.payment.plan.title')</th>
      </tr>
      <tr class="tr">
        <td class="td">{{ $user->name }}</td>
        <td class="td">{{ $dentalcase->begin_date }}</td>
        <td class="td">{{ $dentalcase->ended_date }}</td>
        <td class="td">
          @lang('app.dentalcase.payment.'.$dentalcase->plan)
        </td>
      </tr>
    </table>
   
    
    <p style="page-break-before: always;"></p>
        <div class="title-center">
          <h2>@lang('app.admin.deltalcase.treatment.title')</h2>
        </div>

        <br><br><br>
    @foreach($jsonTreatment['plan'] as $group)
        @if($group['type'] == 'break')
          <p style="page-break-before: always;"></p>
          @php
            continue;
          @endphp
        @endif
        <div style="text-align: center; text-transform: uppercase;">
          <h3>{{ $group['title'] }}</h3>
        </div>
        
        @if(isset($group['fields']) && count($group['fields']) > 0)

           <table class="table">
            @php
              $cols = 0;
              $title = '';
              $content = '';
              $subtitle = '';
              $colspan = '';
            @endphp
            @foreach($group['fields'] as $field)
              @php
                if($field['type'] == 'title'){
                  $subtitle .= '<tr class="tr"><td class="td" colspan=2><b>'.$field['title'].'</b></td></tr>';
                  continue;
                }
                $class = 'col';
                if(isset($field['width'])){
                  $cols += $field['width'];
                  if($field['width']  == 12){
                    $colspan = ' colspan=2';
                  }
                }
                
                if($field['type'] == 'break'){
                  echo '</table>';
                  echo '<p style="page-break-before: always;"></p>';
                  echo '<table class="table">';
                  continue;
                }

                
              //ENCABEZADO

                $title .= '<td class="th" '.$colspan.'><b>'.$field['title'].'</b></td>';

              //CUERPO

                if(isset($treatment_json[$field['name']])){
                  if(is_array($treatment_json[$field['name']])){
                    $intValue = '';
                    if(isset($field['fields'])){
                      foreach ($field['fields'] as $intField) {
                        $intValue .= '<br>'.$treatment_json[$intField['name']];
                      }
                    }
                    $value = '';
                    foreach($treatment_json[$field['name']] as $element){
                      $value .= '<li>'.$element.'</li>';
                    }
                    $content .= '<td class="td"'.$colspan.'><ul>'.$value.'</ul>'.$intValue.'</td>';
                  }else{
                    $intValue = '';
                    if(isset($field['fields'])){
                      foreach ($field['fields'] as $intField) {
                        $intValue .= '<br>'.$treatment_json[$intField['name']];
                      }
                    }
                    $content .= '<td class="td"'.$colspan.'>'.$treatment_json[$field['name']].$intValue.'</td>';
                  }
                    

                }else{
                  $content .= '<td class="td"'.$colspan.'>&nbsp;</td>';
                }

              
              $colspan = '';
              if($cols == 12){
                 
                  $cols = 0;
                  echo $subtitle;
                  echo '<tr class="tr">';
                  echo $title;
                  echo '</tr>';
                  echo '<tr class="tr">';
                  echo $content;
                  echo '</tr>';
                  
                  $title = '';
                  $content = '';
                  $subtitle = '';
               
              }
              @endphp
            @endforeach
            @php
            if($cols > 0){
                  echo '<tr class="tr">';
                  echo $title;
                  echo '</tr>';
                  echo '<tr class="tr">';
                  echo $content;
                  echo '</tr>';
            }
            @endphp
          </table>
          <br><br><br>
        @endif
        

      @endforeach
  
</body>
</html>
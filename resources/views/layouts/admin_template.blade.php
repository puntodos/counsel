<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link rel="shortcut icon" href="{{ asset(config('constants.options.favico')) }}" type="image/x-icon" />
    <title>{{ config('app.name', 'COUNSEL ORTHODONTIST') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" ></script>
    <script src="{{ asset('js/popper.min.js') }}" ></script>
    <script src="{{ asset('js/general.js') }}" ></script>

    @stack('scripts')
    <!-- Fonts -->
    <link rel="dns-prefetch" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Raleway:300,400,600" rel="stylesheet" type="text/css">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/card.css') }}" rel="stylesheet">
    <link href="{{ asset('css/font-awesome.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/bootstrap-datepicker.min.css') }}" rel="stylesheet">

    
</head>
<body>
    @app_errors_modal
    @endapp_errors_modal
    @include('layouts.header.admin')
    @if(isset(session()->all()['flash_notification']))
        <div class="container py-2">
            <div class="row">
                <div class="col-md-12">
                    @include('flash::message')
                </div>
            </div>
        </div>
        <script>
        $('div.alert').not('.alert-important').delay(3000).fadeOut(100  0);
        </script>
    @endif

    <div id="app"  style="min-height: 70%">
        
            <div class="container py-2">
              <div class="row">
                <div class="col-md-2">
                  <ul class="nav nav-pills flex-column">
                    <li class="nav-item">
                      <a href="{{ route('dashboard.index') }}" class="active nav-link">
                        <i class="fa fa-home fa-home"></i>&nbsp;
                      @lang('app.admin.menu.option1')
                      </a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link" href="{{ route('dashboard.listdentist') }}">@lang('app.admin.menu.option2')</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link" href="{{ route('dashboard.list.dentalcases') }}">@lang('app.admin.menu.option3')</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link" href="{{ route('dashboard.list.dentalcases.actives') }}">@lang('app.admin.menu.option4')</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link" href="{{ route('dashboard.list.dentalcases.ended') }}">@lang('app.admin.menu.option5')</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link" href="{{ route('dashboard.list.dentalcases.pending') }}">@lang('app.admin.menu.option8')</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link" href="{{ route('dashboard.list.dentalcases.unread') }}">@lang('app.admin.menu.option6')</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link" href="{{ route('dashboard.list.dentalcases.unread.messages') }}">@lang('app.admin.menu.option9')</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link" href="{{ route('dashboards.users') }}">@lang('app.admin.menu.option7')</a>
                    </li>
                  </ul>
                </div>
                <div class="col-md-10">
                    <div class="row">
                    @yield('content')   
                    </div>     
                </div>
              </div>
            </div>
            
        
    </div>
    @include('layouts.footer.footer')
</body>
</html>


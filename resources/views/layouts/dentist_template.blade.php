<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">


    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link rel="shortcut icon" type="image/png" href="{{ asset(config('constants.options.favico')) }}"/>
    <title>{{ config('app.name', 'COUNSEL ORTHODONTIST') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" ></script>
    <script src="{{ asset('js/popper.min.js') }}" ></script>
    <script src="{{ asset('js/general.js') }}" ></script>

    @stack('scripts')
    <!-- Fonts -->
    <link rel="dns-prefetch" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Raleway:300,400,600" rel="stylesheet" type="text/css">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/font-awesome.min.css') }}" rel="stylesheet">


    <style>
    #loading {
        background: #ffffff;
        color: #666666;
        position: fixed;
        height: 100%;
        width: 100%;
        z-index: 100000;
        top: 0;
        left: 0;
        float: left;
        text-align: center;
        padding-top: 25%;
        display: none;
    }
    .loading_img{
        height: 35%;
    }
</style>
    
</head>
<body>

    <div id="loading">
        <div class="container">
            <div class="row">
                <div class="col text-center">
                    @yield('loading_text')
                </div>
            </div>
        </div>
        <img class="loading_img" src="{{ asset(config('constants.options.loading')) }}" alt="Loading" /><br/>
    </div>
    @app_errors_modal
    @endapp_errors_modal
	@include('layouts.header.dentist')
    @if(isset(session()->all()['flash_notification']))
        <div class="container py-2">
            <div class="row">
                <div class="col-md-12">
                    @include('flash::message')
                </div>
            </div>
        </div>
        <script>
        $('div.alert').not('.alert-important').delay(3000).fadeOut(100 , 0);
        </script>
    @endif
    <div id="app" style="min-height: 70%">
        <main class="py-2">
            @yield('content')
        </main>
    </div>
    @include('layouts.footer.footer')
</body>
</html>

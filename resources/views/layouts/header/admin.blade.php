<nav class="navbar navbar-expand-md bg-primary navbar-dark">
    <div class="container">
      <a class="navbar-brand" >
        <img width="220px" src="{{ asset(config('constants.options.logo2')) }}">
      </a>
      <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbar2SupportedContent" aria-controls="navbar2SupportedContent" aria-expanded="false" aria-label="Toggle navigation"> <span class="navbar-toggler-icon"></span> </button>
      <div class="collapse navbar-collapse text-center justify-content-end">
        
        <div class="btn-group">
          <button class="btn dropdown-toggle btn-outline-light" style="min-width: 200px;" data-toggle="dropdown"> 
              {{{ isset(Auth::user()->name) ? Auth::user()->name : Auth::user()->email }}}
           </button>
          <div class="dropdown-menu">
            <a class="dropdown-item" href="{{ route('dentist.changepassword') }}">
            {{ __('app.dentist.changepassword') }}
            </a>
            <div class="dropdown-divider"></div>
            <a class="dropdown-item" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                                       @lang('app.general.logout')  
                                                     </a>
            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                @csrf
            </form>
          </div>
        </div>
      </div>
    </div>
  </nav>
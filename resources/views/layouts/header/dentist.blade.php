<nav class="navbar navbar-expand-md bg-primary navbar-dark">
    <div class="container">
      <?php /*<a class="navbar-brand" href="#"><i class="fa d-inline fa-lg fa-cloud"></i><b>  
        {{ config('app.name', 'COUNSEL ORTHODONTIST') }}</b>
      </a>*/ ?>
      
      <a class="navbar-brand" >
        <img width="220px" src="{{ asset(config('constants.options.logo2')) }}">
      </a>
       
      <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbar2SupportedContent" aria-controls="navbar2SupportedContent" aria-expanded="false" aria-label="Toggle navigation"> <span class="navbar-toggler-icon"></span> </button>
      <div class="collapse navbar-collapse text-center justify-content-end">
        <ul class="navbar-nav">
          <li class="nav-item">
            <a class="btn active btn-secondary text-light m-1" href="{{ route('dentalcases.index') }}">Panel </a>
          </li>
        </ul>
        <div class="btn-group">
          <button class="btn dropdown-toggle btn-outline-light" style="min-width: 200px;" data-toggle="dropdown"> 
              {{{ isset(Auth::user()->name) ? Auth::user()->name : Auth::user()->email }}}
           </button>
          <div class="dropdown-menu">
            <a class="dropdown-item" href="{{ route('dentist.edit', Auth::user()->id) }}">
            {{ __('app.dentist.settings') }}
          </a>
          <a class="dropdown-item" href="{{ route('dentist.changepassword') }}">
            {{ __('app.dentist.changepassword') }}
          </a>
            <div class="dropdown-divider"></div>
            <a class="dropdown-item" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
              @lang('app.general.logout')                          
            </a>
            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                @csrf
            </form>
          </div>
        </div>
      </div>
    </div>
  </nav>
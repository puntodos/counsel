  <div class="bg-dark text-white">
    <div class="container">
      <div class="row">
        <div class="col-md-12 mt-3">
          <p class="text-center text-white">
            <a href="#" data-toggle="modal" data-target="#tyc">
            @lang('app.general.footer.tyc')</a>
          </p>
          <p class="text-center text-white">
            @lang('app.general.footer.problems')
          </p>
        </div>
      </div>
    </div>
  </div>

<div class="modal fade" id="tyc" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalCenterTitle">
          @lang('app.general.tyc.title')
        </h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        @lang('app.general.tyc.description')
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">
          @lang('app.general.tyc.close')
        </button>
      </div>
    </div>
  </div>
</div>

<h5><b>@lang('app.admin.deltalcase.diagnostic.'.$field)</b></h5>

	@if(strpos(__('app.admin.deltalcase.diagnostic.'.$field.'.description'),'admin.deltalcase.diagnostic') == "")
		<p>@lang('app.admin.deltalcase.diagnostic.'.$field.'.description')</p>
	@endif

	@if(strpos(__('app.admin.deltalcase.diagnostic.'.$field.'.description2'),'admin.deltalcase.diagnostic') == "")
		<p>@lang('app.admin.deltalcase.diagnostic.'.$field.'.description2')</p><br>
	@endif


@foreach($fields[$field] as $item)
	@php
		$checked = false;
		if($item == $diagnostic[$field]):
			$checked = true;
		endif;
	@endphp
<div class="radio">
  <label>
  	{{ Form::radio($field, $item, $checked, ['class' => '']) }}
  	@lang('app.admin.deltalcase.diagnostic.'.$field.'.'.$item)
  </label>
</div>
@endforeach
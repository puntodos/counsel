
                  <label>
                    <h5>
                      <b> @lang('app.dentalcase.patient.'.$field)</b>
                    </h5>
                  </label> 
                  @if($file != "")
                  <a onclick="window.open('{!! $user_path.$file !!}')">
                  <i class="fa d-inline fa-lg fa-external-link bg-light"></i>
                  </a>
                  @else
                  N/A
                  @endif


<h5><b>{{ $field['title'] }}</b></h5>

	
{{-- 
@php
$value = "NA";
@endphp
@foreach($field['values'] as $value)
	@php
		 if(isset($diagnostic[$field['name']]) && $value['value'] == $diagnostic[$field['name']]):
		 	$value = $value['value']
		 endif;
	@endphp
@endforeach
--}}
<label>	
	@if(isset($diagnostic[$field['name']]))
		{{ $diagnostic[$field['name']] }}
	@else
		NA
	@endif
</label> 


<div class="modal fade" role="dialog" id="errors">
    <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title"><b style="color:red;">@lang('app.general.errors')</b></h5>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <div class="modal-body" style="color:red;">
          @foreach($errors->all() as $error)
             - {{$error}}<br>
          @endforeach
          <h6><b></b></h6>
          <h6></h6>
        </div>
      </div>
    </div>
  </div>
@php
  if(count($errors->all()) > 0):
@endphp
  <script>
    $(document).ready(function(){
        //$('.modal').modal();
        $('#errors').modal();
    }) 
  </script>
@php
  endif;
@endphp
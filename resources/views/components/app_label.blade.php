{{-- field --}}
@php

	if(!isset($namespace)):
		$namespace = '';
	endif;
	$style = '';
	$text = '';
	$error = false;
	if ($errors->has($field)):
		$error = true;
		$style = 'color:red';
		$text = ucfirst($errors->first($field));
	endif;

@endphp
@if (!$error)	
	@if ($namespace == '')
	<label style="{{ $style }}">@lang('app.dentist.'.$field)</label>
	@else
	<label style="{{ $style }}">@lang($namespace.'.'.$field)</label>
	@endif
@else
	<label style="{{ $style }}">{{ $text }}</label>
@endif

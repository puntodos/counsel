



<h5><b>{{ $field['title'] }}</b></h5>

	@if(isset($field['description']))
		<p>{{ $field['description'] }}</p>
	@endif

	@if(isset($field['description2']))
		<p>{{ $field['description2'] }}</p>
	@endif


@foreach($field['values'] as $value)
	@php
		$checked = false;
		
		 if(isset($diagnostic[$field['name']]) && $value['value'] == $diagnostic[$field['name']]):
		 	$checked = true;
		 endif;
	@endphp
<div class="radio">
  <label>
  	 {{ Form::radio($field['name'], $value['value'], $checked, ['class' => '']) }}
  	{{ $value['value'] }} 
  </label>
  @if(isset($value['help']))
  	<i class="fa d-inline fa-lg fa-question-circle">
	  <div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
	  <div class="modal-dialog modal-dialog-centered modal-md">
	    <div class="modal-content">
	      <div class="modal-header">
	        <button type="button" class="close modal_close" data-dismiss="modal">&times;</button>
	      </div>
	      <div class="modal-body">
	        <h6><b>{{ $field['title'] }} > {{ $value['value'] }}</b></h6>
	        <h6>{{ $value['help'] }}</h6>
	      </div>
	    </div>
	  </div>
	</div>
	</i>
  @endif
</div>
@endforeach
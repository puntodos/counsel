<label>
  <h5>
    <b> @lang('app.dentalcase.patient.'.$field)</b>
  </h5>
</label> 

<br>
<div class="form-inline">
  {!! Form::text(null
                , ''
                , ['class' => 'form-control'
                , 'placeholder' => __('app.dentalcase.patient.'.$field)
                , 'id' => 'attach_'.$field.'_resume'
                , 'style' => 'width:300px'
                , 'readonly' => 'readonly'])
   !!}
   {!! Form::button(__('app.dentalcase.patient.add_file')
                    , ['class' => 'btn btn-secondary attach_btn'
                    , 'data-id' => 'attach_'.$field.'_resume'
                    , 'data-id_file' => 'file_'.$field
                    ]) !!}  &nbsp;
  {{ Form::file($field, ['id' => 'file_'.$field
                                , 'style' => 'display:none'
                                ]) }}

</div>
@php

  if(!isset($width)):
    $width = '300px';
  endif;

  if(isset($patient)):
    if($patient[$field] != ''):
      $width = '200px';
    endif;
  endif;

@endphp

<label>
  <h5>
    <b> @lang('app.dentalcase.patient.'.$field)</b>
  </h5>
</label> 
<i class="fa d-inline fa-lg fa-question-circle">

  <div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered modal-md">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close modal_close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body">
        <h6><b>@lang('app.dentalcase.patient.'.$field)</b></h6>
        <h6>@lang('app.dentalcase.patient.'.$field.'desc')</h6>
      </div>
    </div>
  </div>
</div>

</i>
<br>
<div class="form-inline">
  {!! Form::text(null
                , ''
                , ['class' => 'form-control'
                , 'placeholder' => __('app.dentalcase.patient.'.$field)
                , 'id' => 'attach_'.$field.'_resume'
                , 'style' => 'width:'.$width
                , 'readonly' => 'readonly'])
   !!}
   {!! Form::button(__('app.dentalcase.patient.add_file')
                    , ['class' => 'btn btn-secondary attach_btn'
                    , 'data-id' => 'attach_'.$field.'_resume'
                    , 'data-id_file' => 'file_'.$field
                    ]) !!}  &nbsp;
  {{ Form::file($field, ['id' => 'file_'.$field
                                , 'style' => 'display:none'
                                ]) }}

  @if(isset($patient) && $patient[$field] != '')
  {!! Form::button(__('app.dentist.file_document_btn_view')
                  , ['class' => 'btn btn-primary'
                  , 'onclick' => 'window.open("'.$user_path . '/' . $patient[$field].'")'
                  ]) !!}
  @endif

</div>
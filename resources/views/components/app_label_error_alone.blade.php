{{-- field --}}
@if ($errors->has($field))
<div class="alert alert-danger" role="alert">
  <strong>{{ $errors->first($field) }}</strong>
</div>
@endif
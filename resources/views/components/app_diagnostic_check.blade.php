<h5><b>{{ $field['title'] }}</b></h5>

	@if(isset($field['description']))
		<p>{{ $field['description'] }}</p>
	@endif

	@if(isset($field['description2']))
		<p>{{ $field['description2'] }}</p>
	@endif

@foreach($field['values'] as $value)
	@php
		$checked = '';	
		 if(isset($diagnostic[$field['name']])):
		 	foreach($diagnostic[$field['name']] as $element):
		 		if($value['value'] == $element):
		 			$checked = 'checked="checked"';
		 		endif;
		 	endforeach;
		 endif;
	@endphp
<div class="checkbox">
  <label>
  	<input type="checkbox" name="{{ $field['name'] }}[]" value="{{ $value['value'] }}" {{ $checked }}>
  	{{ $value['value'] }} 
  </label>
</div>
@endforeach
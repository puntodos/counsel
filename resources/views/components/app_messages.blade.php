
      @foreach($messages as $message)
        @php
          $bg = 'bg-primary ';
          $text_color = 'text-white';
          if($message->user->type != 'dentist'):
            $bg = 'bg-light';
            $text_color = '';
          endif;
        @endphp
      <br>
      <div class="row">
        <div class="col-md-12">
          <div class="list-group">
            <span class="list-group-item list-group-item-action flex-column align-items-start {{ $bg }} {{ $text_color }}" >
              <div class="d-flex w-100 justify-content-between"> &nbsp; <small>{{ $message->created_at }}</small> </div>
              <p class="mb-1">
                {{ $message->text }}
              </p> 
              <small>
                {{ $message->user->name }}
                @if($message->file_attach != '')
                - <a class="{{ $text_color }}" href="{{ $user_path.$message->file_attach }}" target="_blank">Archivo adjunto 
                  <i class="fa d-inline fa-lg fa-external-link {{ $bg }}"></i>
                </a>
                @endif
              </small> 
            </span>
          </div>
        </div>
      </div>
      @endforeach
     
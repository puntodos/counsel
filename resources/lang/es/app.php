<?php

return [
	//DENTIST

		//GENERAL

		'general.app.name'						=> 'COUNSEL ORTHODONTIST',

		'general.login'							=> 'Login',
		'general.logout'						=> 'Logout',
		'general.errors'						=> 'Opps has occurred some error(s)',
		'general.register'						=> 'register',
		'general.name'							=> 'Name',
		'general.email'							=> 'E-mail',
		'general.password'						=> 'Password',
		'general.password_confirm'				=> 'Confirm Password',
		'general.rememberme'					=> 'Remember me',
		'general.forgot'						=> 'Forgot your password?',
		'general.reset'							=> 'Recover password',
		'general.reset_link'					=> 'Recover password',
		'general.back_btn'						=> 'Back',
		'general.back_error'					=> 'action not allowed',

		'general.footer.tyc'					=> 'Terms and conditions - Data use policy',
		'general.footer.problems'				=> 'Problems? Mail us at <a href="mailto:info@counselorthodontist.com">info@counselorthodontist.com</a> ',
		'general.tyc.title'						=> 'Terms and conditions',
		'general.tyc.close'						=> 'Close',
		'general.tyc.description'				=> 'By entering this page and having the service offered by it, you
recognizes and accepts that you are a professional dentist graduated from an university
endorsed by the education regulator in your country. <br>
You also agree that counselorthodontist.com only provides the
advice, but does not perform the treatment, therefore it is not responsible for the
result of it.',

		'general.email.address'	=> 'Calle 36 # 38 - 43 Bogota',

		'general.email.resetpassword.noregister'	=> 'Email not registered',
		'general.email.resetpassword.register'	=> 'An email has been sent to you, please check and follow the instructions to recover your password',
		'general.email.resetpassword.notmatch'	=> 'Passwords do not match',
		'general.email.resetpassword.success'	=> 'Passwords changed successfully',
		'general.email.resetpassword.invalidtoken'	=> 'The session has expired, try again to recover your password',
		'general.email.resetpassword.subject'	=> 'COUNSEL ORTHODONTIST - Password Change Request',
		'general.email.resetpassword.greeting'	=> 'Hi!',
		'general.email.resetpassword.message'	=> 'You are receiving this email because we received a password reset request for your account.',
			'general.email.resetpassword.message2'	=> 'If you did not request a password reset, no further action is required.<br><br>Regards,<br>COUNSEL ORTHODONTIST',

		'general.email.newaccount.address'	=> 'Calle 36 # 38 - 43 Bogota',
		'general.email.newaccount.subject'	=> 'COUNSEL ORTHODONTIST - A new account has been created',
		'general.email.newaccount.greeting'	=> 'Hello, a new user has registered on the platform!',
		'general.email.newaccount.message'	=> 'Congratulations a new user has registered on the platform, you can enter the platform to validate all your information from this moment',
		'general.email.newdentalcase.subject'	=> 'COUNSEL ORTHODONTIST - A new case has been created',
		'general.email.newdentalcase.greeting'	=> 'Hello, a new case has been registered on the platform!',
		'general.email.newdentalcase.message'	=> 'Congratulations a new case has been registered on the platform, you can enter the platform to validate all your information now',
		'general.email.newmessagereceived.subject'	=> 'COUNSEL ORTHODONTIST - A new message has been received',
		'general.email.newmessagereceived.greeting'	=> 'Hello, you have received a new message for one of the cases you are attending!',
		'general.email.newmessagereceived.message'	=> 'Congratulations you have received a new message for one of the cases you are attending, you can enter the platform to validate all your information now',

		'general.email.newresponsemessage.subject'	=> 'COUNSEL ORTHODONTIST - You have received an answer for one of your cases',
		'general.email.newresponsemessage.greeting'	=> 'Hello, you have received a new response on the platform!',
		'general.email.newresponsemessage.message'	=> 'Congratulations you have received a response on one of your cases. Please enter the platform so you can manage it.',

		'general.email.newcaseactived.subject'	=> 'COUNSEL ORTHODONTIST - One of your cases has been activated',
		'general.email.newcaseactived.greeting'	=> 'Hi, one of your cases has been activated on the platform!',
		'general.email.newcaseactived.message'	=> 'Congratulations one of your cases has been activated, please enter to the platform for start the management of this case, remember that you have an active message section so that you can expose all your doubts.',
		'general.email.newcaseactived.message.default'	=> 'Hello, we have already received your case, soon we will review it thoroughly and we will give you a diagnosis. We will respond maximum in 5 business days.',

		'dentist.settings'						=> 'Profile',
		'dentist.changepassword'				=> 'Change password',
		'dentist.changepassword.badpassword'	=> 'Error, the password entered does not match the current one',
		'dentist.changepassword.sucess'			=> 'The password has been successfully modified',
		'dentist.changepassword.nomatch'		=> 'Error, new passwords do not match',
		'dentist.currentpassword'				=> 'Current password',
		'dentist.newpassword'				=> 'New password',
		'dentist.newpassword2'				=> 'Retype the new password',



		//ADMIN DASHBOARD
		'admin.menu.title'						=> 'Dashboard',
		'admin.menu.option1'					=> 'General',
		'admin.menu.option2'					=> 'Dentists',
		'admin.menu.option3'					=> 'Cases',
		'admin.menu.option4'					=> 'Active Cases',
		'admin.menu.option5'					=> 'Completed Cases',
		'admin.menu.option6'					=> 'Unread Cases',	
		'admin.menu.option7'					=> 'Users',
		'admin.menu.option8'					=> 'Pending Payment Cases',
		'admin.menu.option9'					=> 'Unread Messages Cases',
		'admin.menu.seemore'					=> 'See more',
		'admin.dentalcase.change_status'		=> 'Change status',


		'admin.list.dentist.id'					=> 'Id',		
		'admin.list.dentist.dentist'			=> 'Dentist',		
		'admin.list.dentist.email'				=> 'E-mail',		
		'admin.list.dentist.country'			=> 'Country',		
		'admin.list.dentist.opencases'			=> 'Opened Cases',		
		'admin.list.dentist.cases'			=> 'Cases',		
		'admin.list.dentist.unreadmessages'		=> 'Unread Messages',	
		'admin.list.dentist.unreadcases'		=> 'Unread Cases',
		'admin.list.dentist.search'		=> 'Search',
		'admin.list.dentist.search.results'		=> 'Search results ',
		'admin.list.dentist.search.title'		=> 'Dentist',		
		'admin.list.dentist.show.status.pending' => 'The dentist has not completed the registration yet',		
		'admin.list.dentist.show.cases.patient.id' => 'Id',	
		'admin.list.dentist.show.cases.patient.name' => 'Name of patient',	
		'admin.list.dentist.show.cases.patient.date_begin' => 'Treatment date',	

		'admin.list.dentist.show.cases.patient.dentist_name' => 'Dentist',		
		'admin.list.dentist.show.cases.patient.status' => 'Status',		
	
		'admin.list.users.title' 						=> 'User List',		
		'admin.list.users.id' 							=> 'Id',		
		'admin.list.users.name' 						=> 'Name',		
		'admin.list.users.email' 						=> 'E-mail',		
		'admin.list.users.action' 						=> 'Action',		

		'admin.deltalcases.status.pending'		=> 'Pending payment',
		'admin.deltalcases.status.action.pending'	=> 'Set pending payment status',
		'admin.deltalcases.status.pending.confirm'	=> 'Are you sure you want to set this case pending?',
		'admin.deltalcases.status.active'			=> 'Active',
		'admin.deltalcases.status.action.active'	=> 'Activate',
		'admin.deltalcases.status.action.active.date'	=> 'Select an end date',

		'admin.deltalcases.status.action.active.date_begin'	=> 'Select a start date',
		'admin.deltalcases.status.action.active.action'	=> 'Activate',
		'admin.deltalcases.status.ended'			=> 'Ended',
		'admin.deltalcases.status.ended.confirm'			=> 'Are you sure you want to end this case?',
		'admin.deltalcases.status.action.ended'			=> 'End',
		'admin.deltalcases.status.message'			=> 'Status has been updated successfully',
		'admin.deltalcases.mark_as_read'			=> 'Mark as read',
		'admin.deltalcases.mark_as_read.message'	=> 'Message(s) marked as read(s)',
		'admin.deltalcases.diagnostic.view_btn'	=> 'See diagnostic',

		//TREATMENT
		'admin.deltalcases.treatment.view_btn'	=> 'See treatment plan',
		'admin.deltalcase.treatment.title'			=> 'TREATMENT PLAN',
		'admin.deltalcase.treatment.save_btn'			=> 'Save treatment',
		'admin.deltalcase.treatment.email.subject'			=> 'New treatment plan',
		//DIAGNOSTIC
		'admin.deltalcase.diagnostic.title'			=> 'DIAGNOSTIC',
		'admin.deltalcase.diagnostic.save_btn'			=> 'Save Diagnostic',
		'admin.deltalcase.diagnostic.frontal_photo'	=> 'Frontal photo',
		'admin.deltalcase.diagnostic.save_success'	=> 'Diagnostic saved successfully',

		
		'admin.deltalcase.diagnostic.email.subject'			=> 'New Diagnostic',
		'admin.deltalcase.diagnostic.email.greeting'		=> 'Congratulations a new diagnostic has been generated for one of your cases',
		'admin.deltalcase.diagnostic.email.message'			=> 'For further info please log into the platform.',
		'admin.deltalcase.diagnostic.message'			=> 'A diagnostic has been created.',
		'admin.deltalcase.treatment.message'			=> 'A treatment plan has been created.',

		//DIAGNOSTIC IN PDF FILE
		'admin.deltalcase.diagnostic.pdf.btn'			=> 'Generate Pdf',
		'admin.deltalcase.diagnostic.pdf.title'			=> 'DIAGNOSTIC',
		'admin.deltalcase.diagnostic.pdf.name'			=> 'Name',
		'admin.deltalcase.diagnostic.pdf.age'			=> 'Age',
		'admin.deltalcase.diagnostic.pdf.genere'			=> 'Gender',
		'admin.deltalcase.diagnostic.pdf.genere.m'			=> 'Male',
		'admin.deltalcase.diagnostic.pdf.genere.m'			=> 'Male',
		'admin.deltalcase.diagnostic.pdf.patient.title'			=> 'Patient',
		'admin.deltalcase.diagnostic.pdf.dentist.name'			=> 'Dentist name',
		

		// PROFILE
		'dentist.createdaccount'			=>'Welcome to COUNSEL ORTHODONTIST <br> Your account has been registered, please fill in all the information so you can start using our platform.',

		'dentist.personal_profile_title'	=>'Personal Profile',
		'dentist.name' 						=> 'Name',
		'dentist.cell_phone' 				=> 'Phone',
		'dentist.code_phone_id' 			=> 'Country',
		'dentist.country'					=> 'Choose a country',
		'dentist.email'						=> 'email',
		'dentist.city'						=> 'City',
		'dentist.company'					=> 'Company',
		'dentist.profile_title'				=> 'Profesional profile',
		'dentist.document_number'			=> 'Identification Number',
		'dentist.associations'				=> 'Associations',
		'dentist.supports_institution'		=> 'Institution',
		'dentist.university'				=> 'University where you did the degree',
		'dentist.specialities'				=> 'Specialties',
		'dentist.file_document'				=> 'Attach your professional ID or dental degree',
		'dentist.create_btn'				=> 'End registration process',
		'dentist.file_document_text'		=> 'Select a file(img o pdf)',
		'dentist.file_document_btn'			=> 'Attach file',
		'dentist.file_document_btn_view'	=> 'See file',
		'dentist.tyc'						=> 'Accept terms and conditions',
		'dentist.cases'						=> 'Cases',
		
		//CASES

		'deltalcases.welcome'				=> 'Welcome',
		'deltalcases.description'			=> 'Welcome to our platform, to start using our services, we invite you to create your first case',
		'deltalcases.newcase'				=> 'Create new case',
		'deltalcases.status.pending'		=> 'Continue',
		'deltalcases.status.active'			=> 'Active',
		'deltalcases.status.ended'			=> 'Finalized',
		'deltalcases.status.admin.pending'		=> 'Payment pending',
		'deltalcases.status.admin.active'			=> 'Active',
		'deltalcases.status.admin.ended'			=> 'Finalized',
		'dentalcase.case_profile_title'		=> 'New case creation (General Data)',
		'dentalcase.case_profile_title.edit'		=> 'Edit case (General Data)',
		'dentalcase.list.unreadmessages'	=> 'Unread messages',
		'dentalcase.patient.name'			=> 'Name of patient *',
		'dentalcase.patient.age'			=> 'Age of patient *',
		'dentalcase.patient.genere'			=> 'Gender of patient',
		'dentalcase.patient.genere.m'		=> 'Male',
		'dentalcase.patient.genere.f'		=> 'Female',
		'dentalcase.patient.genere.select'	=> 'Select a gender',
		'dentalcase.patient.diagnosticimages'	=> 'X-rays:',
		'dentalcase.patient.add_file'	=> 'Add file',
		'dentalcase.patient.radiography1'	=> 'Lateral cephalic x-ray:',
		'dentalcase.patient.radiography1desc'	=> 'to evaluate:<br>
- Profile features. <br>
- Skeletal features of the maxillary. <br>
- Dental features. <br>
- Facial features. <br>
- Total facial height, average facial height and lower anterior facial height.',
		'dentalcase.patient.radiography2'	=> 'Panoramic X-ray:',
		'dentalcase.patient.radiography2desc'	=> 'to evaluate:<br><br>

- Quality and quantity of alveolar bone that supports the teeth. <br>
- Mandibular morphology. <br>
- Abnormalities of shape, size of teeth. <br>
- Abnormalities in the number of teeth. <br>
- Abnormalities in the position of the teeth. <br>
- Degree of root education and dental development. <br>
- Stage of development of occlusion. <br>
- Features of different anatomical structures.',
		'dentalcase.patient.radiography3'	=> 'Posteroanterior X-ray:',
		'dentalcase.patient.radiography3desc'	=> 'evaluates:<br><br>
- Transversal skeletal problems, asymmetries and discrepancy of
midlines.',
		'dentalcase.patient.radiography4'	=> 'Periapical radiography:',
		'dentalcase.patient.radiography4desc'	=> 'Specific to one or two teeth, evaluate
more detailed dental structure.',
		'dentalcase.patient.radiography5'	=> 'Computed axial tomography:',
		'dentalcase.patient.radiography5desc'	=> '3D evaluation of the form, the
size and spatial relationship between teeth and craniofacial bones.',
		'dentalcase.patient.diagnosticimages2'	=> 'EXTRAORAL photos:',
		'dentalcase.patient.radiography6'	=> 'Front photo:',
		'dentalcase.patient.radiography6desc'	=> 'Used to make the transversal and vertical analysis of the
a patients face. It allows dividing the face into fifths vertically and thirds horizontally.',
		'dentalcase.patient.radiography7'	=> 'Front photo smiling:',
		'dentalcase.patient.radiography7desc'	=> 'can be seen the interlabial space and the line
of the smile.',
		'dentalcase.patient.radiography8'	=> 'Side photo:',
		'dentalcase.patient.radiography8desc'	=> 'used to make the vertical analysis of the face of the
patient.',
		'dentalcase.patient.diagnosticimages3'	=> 'INTRAORAL photos:',
		'dentalcase.patient.radiography9'	=> 'Front photo in occlusion:',
		'dentalcase.patient.radiography9desc'	=> 'evaluates:  <br><br>
- The relationship and symmetry of the maxillary dental midlines and the
jaw. <br>
- The vertical overbite of the incisors. <br>
- The relationship of the anterior and posterior occlusal <br>
- Defects and permanent damage of dental enamel.',
		'dentalcase.patient.radiography10'	=> 'Right and left lateral photo in occlusion <br> up to the molars:',
		'dentalcase.patient.radiography10desc'	=> 'evaluates:<br><br>
- The molar and canine relationship on each side <br>
- The relationship of the posterior occlusal plane with the previous one.',
		'dentalcase.patient.radiography11'	=> 'Anterior lateral photo of incisors to see the <br>overbite* and overjet*:',
		'dentalcase.patient.radiography11desc'	=> 'evaluates:<br><br>

- The amount of vertical overbite in the anterior area <br>
- The amount of horizontal overbite in the anterior area. <br>
- The vestibularization of the upper and lower incisors.',
		'dentalcase.patient.radiography12'	=> 'Upper maxillary occlusal photo:',
		'dentalcase.patient.radiography12desc'	=> 'The entire dental arch should be shown, even the third molars should be seen if they are present. The tongue should not
Appear. Evaluates: <br> <br>
- Maxillary arch shape. <br>
- Abnormalities of shape, size, position, number of maxillary teeth. <br>
- Magnitude of crowding.',
		'dentalcase.patient.radiography13'	=> 'Lower maxillary occlusal photo:',
		'dentalcase.patient.radiography13desc'	=> 'The entire dental arch should be shown, even the third molars should be seen if they are present. The tongue should not
Appear. Evaluates: <br> <br>
- Maxillary arch shape. <br>
- Abnormalities of shape, size, position, number of maxillary teeth. <br>
- Magnitude of crowding.',
		'dentalcase.patient.create_btn'				=> 'Save new patient',			
		'dentalcase.patient.edit_btn'				=> 'Save modified case',			
		'dentalcase.patient.objective_title'		=> 'Objectives',			
		'dentalcase.patient.objective_patient'		=> 'Patient objective',			
		'dentalcase.patient.objective_dentist'		=> 'Dentist objective',			
		'dentalcase.patient.begin_date'				=> 'Start date',			
		'dentalcase.patient.end_date'				=> 'End date',			
		'dentalcase.patient.show.trate'				=> 'PATIENT:',	
		'dentalcase.patient.show.files'				=> 'View files',	
		'dentalcase.patient.show.files.title'		=> 'X-rays:',			
		'dentalcase.patient.show.conversation.title'=> 'Conversation',			
		'dentalcase.patient.show.message'			=> 'Send message',			
		'dentalcase.patient.show.message.new'		=> 'New message',			
		'dentalcase.patient.show.message.field_message'=> 'Message',			
		'dentalcase.patient.show.message.field_discard'=> 'Discard',			
		'dentalcase.patient.show.message.field_send'=> 'Send',			
		'dentalcase.patient.show.edit_case'			=> 'Edit Case',			
		'dentalcase.patient.show.end_case'			=> 'End case',			
		'dentalcase.patient.show.ended_case'		=> 'Finalize',			
		'dentalcase.patient.show.active_case'		=> 'Activate ',			
		'dentalcase.patient.show.make_diagnostic'		=> 'Diagnostic',			
		'dentalcase.patient.show.make_treatmentplan'		=> 'Treatment Plan',			
		'dentalcase.patient.show.pending_case'			=> 'Set case as pending',			
		'dentalcase.patient.show.come_back_to_show'	=> 'Go back',			
		'dentalcase.patient.file_attach'			=> 'Attach file',			
		'dentalcase.patient.file_attach_error'		=> 'No Attached File',	
		'dentalcase.patient.show.pending_payment.text'	=> 'To open the conversation and receive advice, you must pay first',			
		'dentalcase.patient.show.pending_payment.cta'	=> 'Pay',	
		'dentalcase.patient.show.pending_payment.trate'	=> 'Payment for the patient:',
		'dentalcase.patient.edit.success'			=> 'Successfully modified',			
		
	// CASES TABLE
		'dentalcase.table.id'						=> 'Id',			
		'dentalcase.table.name'						=> 'Name of patient',			
		'dentalcase.table.date_begin'				=> 'Treatment Start Date
',		
		'dentalcase.table.date_end'					=> 'Treatment End Date',			
		'dentalcase.table.dentist'					=> 'Dentist',			
		'dentalcase.table.status'					=> 'Status',			
		'dentalcase.table.status.active'			=> 'Active',			
		'dentalcase.table.status.pending'			=> 'Pay',			
		'dentalcase.table.status.ended'				=> 'Ended',	

	//PAYMENTS
		'dentalcase.payment.tyc.title'				=> 'Terms and Conditions',	
		'dentalcase.payment.tyc.description'		=> 'By entering this page and having the service offered, you acknowledge and accept that you are a professional dentist graduated from a university endorsed by the regulatory body of education in your country. <br>
You also agree that counselorthodontist.com only provides advice, but does not perform the treatment, therefore it is not responsible for the result.',	
		'dentalcase.payment.plan.title'				=> 'Plan',	
		'dentalcase.payment.plan.select'			=> 'Choose plan',	
		'dentalcase.payment.plan1'					=> 'Simple',	
		'dentalcase.payment.plan1.title'			=> 'Simple advice:<br><br>',	
		'dentalcase.payment.plan1.price'			=> '29.99',	
		'dentalcase.payment.plan1.cta'				=> 'Pay',	
		'dentalcase.payment.plan1.terms'			=> 'Terms and Conditions',	
		'dentalcase.payment.plan1.desc'				=> 'Includes only one item: <br> -Diagnostic <br> -Pronostics <br> - Objectives and limitations <br> -Treatment plan <br> -Finalization and retention.',	
		'dentalcase.payment.plan2'					=> 'Complete',	
		'dentalcase.payment.plan2.title'			=> 'Complete case advise:',	
		'dentalcase.payment.plan2.price'			=> '149.99',	
		'dentalcase.payment.plan2.cta'				=> 'Pay',	
		'dentalcase.payment.plan2.terms'			=> 'Terms and Conditions',	
		'dentalcase.payment.plan2.desc'				=> 'It includes all the items: <br> -Diagnostic <br> -Pronostics <br> - Objectives and limitations <br> -Treatment plan <br> -Finalization and retention.',	
		'dentalcase.payment.plan3'					=> 'Extended.',	
		'dentalcase.payment.plan3.title'			=> 'Extension for 2 more years:',	
		'dentalcase.payment.plan3.price'			=> '149',	
		'dentalcase.payment.plan3.cta'				=> 'Request',	
		'dentalcase.payment.plan3.desc'				=> '<br><br>Includes case reevaluation and follow-up for 2
years and retention.<br><br><br>',	
		'dentalcase.create.loading' => 'Sending...<br> This process may take several minutes, depending on your internet connection and the amount of files you are uploading. <br> Please DO NOT CLOSE this window.',

];
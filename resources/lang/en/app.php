<?php

return [
	//DENTIST

		// PROFILE

		'dentist.personal_profile_title'	=>'Perfil Personal',
		'dentist.name' 						=> 'Nombre',
		'dentist.cell' 						=> 'Celular',
		'dentist.country'					=> 'Seleccione un País',
		'dentist.email'						=> 'Correo Electrónico',
		'dentist.city'						=> 'Ciudad',
		'dentist.company'					=> 'Compañia',
		'dentist.profile_title'				=> 'Perfil Profesional',
		'dentist.document_number'			=> 'Número de Identificación',
		'dentist.associations'				=> 'Asociaciones a las que pertenece',
		'dentist.supports_institution'		=> 'Institución que lo avala',
		'dentist.university'				=> 'Universidad donde hizo el pregrado',
		'dentist.specialities'				=> 'Especialidades',
		'dentist.file_document'				=> 'Adjunte su cédula profesional o título odontológico',
		
		
	//

];
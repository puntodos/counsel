$(document).ready(function(){
	
	$('.message_time_line').animate({ scrollTop: $(document).height()-$(window).height() });
	

	$('.dentalcase-show').on("click", function(){
		window.location.href = location.href +"/" + $(this).parent().data('id');
	});

	$('.dentalcase-show button').click(function(e) {
	    e.stopPropagation();
	});
	$('.dentalcase-show').css('cursor','pointer');


	$('.check_1').on('click', function(){
		
		if($(this).is(":checked")){
			$('.plan1').removeClass('disabled');
		}else{
			
			$('.plan1').addClass('disabled');
		}
	});

	$('.check_2').on('click', function(){
		if($(this).is(":checked")){
			$('.plan2').removeClass('disabled');
		}else{
			$('.plan2').addClass('disabled');
		}
	});

	$('#plan1').on('click', function(){
		$('.check_1').click();
	});

	$('#plan2').on('click', function(){
		$('.check_2').click();
	});

});
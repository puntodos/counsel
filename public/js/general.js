$(document).ready(function(){

	$('.attach_btn').on('click', function(){
		$('#' + $(this).data('id_file')).trigger('click');
		$('#'+$(this).data('id_file')).off('change');
		var name_file_input = $('#' + $(this).data('id'));
		$('#'+$(this).data('id_file')).change(function(e){
		    var fileName = e.target.files[0].name;
		    name_file_input.val(fileName);
		});

	});

	$('.attach_btn_img').on('click', function(){
		$('#' + $(this).data('id_file')).trigger('click');
		$('#'+$(this).data('id_file')).off('change');
		var img = $(this);
		var input_green = $('#' + $(this).data('id_green'));
		$('#'+$(this).data('id_file')).change(function(e){
		    var fileName = e.target.files[0].name;
		    img.attr('src', input_green.val());
		});

	});

	$('.fa-question-circle').on('click', function(){
		$(this).find('.modal').modal();
	});

	

});


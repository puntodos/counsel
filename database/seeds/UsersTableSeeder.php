<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         DB::table('users')->insert([
        	[
        		'id' => 1, 
        		'name' => "Administrador",
        		'password' => bcrypt('123'),
        		'type' => "admin",
        		'email' => "cristian@puntodos.co"
        	],
        	[
        		'id' => 2, 
        		'name' => "Dentista Carlo",
        		'password' => bcrypt('123'),
        		'type' => "dentist",
        		'email' => "prueba@prueba.com"
        	],
        ]);
    }
}

<?php

use Illuminate\Database\Seeder;

class DentistSpecialitiesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('dentist_specialities')->insert([
        ['id' => 1, 'name' => "Odontología general"],
        ['id' => 2, 'name' => "Ortodoncia"],
        ['id' => 3, 'name' => "Odontopediatría"],
        ['id' => 4, 'name' => "Ortopedia infantil"],
        ['id' => 5, 'name' => "Cirugía"],
        ['id' => 6, 'name' => "Endodoncia"],
        ['id' => 7, 'name' => "Patología"],
        ['id' => 8, 'name' => "Implantología"],
        ['id' => 9, 'name' => "Rehabilitación"],
        ['id' => 10, 'name' => "Estética facial"],
        ]);
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDentistsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dentists', function (Blueprint $table) {
            $table->increments('id');
            $table->string('cell_phone');
            $table->string('city');
            $table->string('company')->nullable();
            $table->string('file_thumb')->nullable();
            $table->string('document_number')->nullable();
            $table->string('supports_institution')->nullable();
            $table->text('associations')->nullable();
            $table->string('university');
            $table->string('file_document')->nullable();
            $table->boolean('tyc')->nullable();
            $table->enum('status',['complete','pending'])->default('pending');

            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users');
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dentists');
    }
}

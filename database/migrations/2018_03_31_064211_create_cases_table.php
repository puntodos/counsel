<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCasesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dentalcases', function (Blueprint $table) {
            $table->increments('id');
            $table->enum('status',['active','pending', 'ended'])->default('pending');
            $table->enum('status_read',['unread','read'])->default('unread');
            $table->integer('messages_unread_4_dentist')->unsigned()->default(0);
            $table->integer('messages_unread_4_admin')->unsigned()->default(0);
            $table->integer('case_by_dentist')->unsigned()->default(0);
            $table->date('begin_date')->nullable();
            $table->date('ended_date')->nullable();
            $table->string('plan')->nullable();
            $table->integer('dentist_id')->unsigned();
            $table->foreign('dentist_id')->references('id')->on('dentists');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dentalcases');
    }
}

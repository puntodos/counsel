<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDentistSpecialitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dentist_specialities', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->timestamps();
        });

        Schema::create('dentist_dentist_speciality', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('dentist_id')->unsigned();
            $table->integer('dentist_speciality_id')->unsigned();

            $table->foreign('dentist_id')->references('id')->on('dentists');
            $table->foreign('dentist_speciality_id')->references('id')->on('dentist_specialities');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dentist_dentist_speciality');
        Schema::dropIfExists('dentist_specialities');
    }
}

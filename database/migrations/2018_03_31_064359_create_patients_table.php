<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePatientsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('patients', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('age');
            $table->string('genere');
            $table->text('radiography1')->nullable();
            $table->text('radiography2')->nullable();
            $table->text('radiography3')->nullable();
            $table->text('radiography4')->nullable();
            $table->text('radiography5')->nullable();
            $table->text('radiography6')->nullable();
            $table->text('radiography7')->nullable();
            $table->text('radiography8')->nullable();
            $table->text('radiography9')->nullable();
            $table->text('radiography10')->nullable();
            $table->text('radiography11')->nullable();
            $table->text('radiography12')->nullable();
            $table->text('radiography13')->nullable();
            $table->text('objective_patient')->nullable();
            $table->text('objective_dentist')->nullable();
            
            $table->integer('dentalcase_id')->unsigned();
            $table->foreign('dentalcase_id')->references('id')->on('dentalcases');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('patients');
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDiagnosticsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('diagnostics', function (Blueprint $table) {
            $table->increments('id');
            
            $table->text('diagnostics')->nullable();
            $table->string('sndiagnostic')->default('N');
            $table->text('plan')->nullable();
            $table->string('snplan')->default('N');
            $table->integer('dentalcase_id')->unsigned();
            $table->foreign('dentalcase_id')->references('id')->on('dentalcases');
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('diagnostics', function (Blueprint $table) {
            //
            Schema::dropIfExists('diagnostics');
        });
    }
}

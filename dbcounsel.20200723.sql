-- MySQL dump 10.13  Distrib 5.7.30, for Linux (x86_64)
--
-- Host: localhost    Database: dbcounsel
-- ------------------------------------------------------
-- Server version	5.7.30-0ubuntu0.16.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `countries`
--

DROP TABLE IF EXISTS `countries`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `countries` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `active` tinyint(1) NOT NULL,
  `code` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=238 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `countries`
--

LOCK TABLES `countries` WRITE;
/*!40000 ALTER TABLE `countries` DISABLE KEYS */;
INSERT INTO `countries` VALUES (1,'Afghanistan',1,'93',NULL,NULL),(2,'Albania',1,'355',NULL,NULL),(3,'Alemania',1,'49',NULL,NULL),(4,'Algeria',1,'213',NULL,NULL),(5,'Andorra',1,'376',NULL,NULL),(6,'Angola',1,'244',NULL,NULL),(7,'Anguilla',1,'1000',NULL,NULL),(8,'Antárctica',1,'672',NULL,NULL),(9,'Antigua',1,'1001',NULL,NULL),(10,'Antillas Francesas',1,'596',NULL,NULL),(11,'Antillas Holandesas',1,'599',NULL,NULL),(12,'Arabia Saudita',1,'966',NULL,NULL),(13,'Argentina',1,'54',NULL,NULL),(14,'Armenia',1,'374',NULL,NULL),(15,'Aruba',1,'297',NULL,NULL),(16,'Australia',1,'61',NULL,NULL),(17,'Austria',1,'43',NULL,NULL),(18,'Azerbaiján',1,'994',NULL,NULL),(19,'Bahamas',1,'1002',NULL,NULL),(20,'Bahía de Guantanamo',1,'539',NULL,NULL),(21,'Bahrain',1,'973',NULL,NULL),(22,'Bangladesh',1,'880',NULL,NULL),(23,'Barbados',1,'1003',NULL,NULL),(24,'Belgica',1,'32',NULL,NULL),(25,'Belice',1,'501',NULL,NULL),(26,'Benin',1,'229',NULL,NULL),(27,'Bermuda',1,'1004',NULL,NULL),(28,'Bhutan',1,'975',NULL,NULL),(29,'Bolivia',1,'591',NULL,NULL),(30,'Bosnia y Herzegovina',1,'387',NULL,NULL),(31,'Botswana',1,'267',NULL,NULL),(32,'Brasil',1,'55',NULL,NULL),(33,'Brunei',1,'673',NULL,NULL),(34,'Bulgaria',1,'359',NULL,NULL),(35,'Burkina Faso',1,'226',NULL,NULL),(36,'Burma',1,'95',NULL,NULL),(37,'Burundi',1,'257',NULL,NULL),(38,'Cambodia',1,'855',NULL,NULL),(39,'Camerún',1,'237',NULL,NULL),(40,'Canada',1,'1',NULL,NULL),(41,'Colombia',1,'57',NULL,NULL),(42,'Comoros',1,'269',NULL,NULL),(43,'Congo',1,'242',NULL,NULL),(44,'Corea',1,'82',NULL,NULL),(45,'Corea del Norte',1,'850',NULL,NULL),(46,'Costa de Marfil',1,'225',NULL,NULL),(47,'Costa Rica',1,'506',NULL,NULL),(48,'Croacia',1,'385',NULL,NULL),(49,'Cuba',1,'53',NULL,NULL),(50,'Chad',1,'235',NULL,NULL),(51,'Chile',1,'56',NULL,NULL),(52,'China',1,'86',NULL,NULL),(53,'Chipre',1,'357',NULL,NULL),(54,'Diego Garcia',1,'246',NULL,NULL),(55,'Dinamarca',1,'45',NULL,NULL),(56,'Djibouti',1,'253',NULL,NULL),(57,'Dominica',1,'1007',NULL,NULL),(58,'Ecuador',1,'593',NULL,NULL),(59,'Egipto',1,'20',NULL,NULL),(60,'El Salvador',1,'503',NULL,NULL),(61,'Emiratos Arabes Unidos',1,'971',NULL,NULL),(62,'Eritea',1,'291',NULL,NULL),(63,'Eslovaquia',1,'421',NULL,NULL),(64,'España',1,'34',NULL,NULL),(65,'Estados Unidos',1,'1',NULL,NULL),(66,'Estonia',1,'372',NULL,NULL),(67,'Etiopía',1,'251',NULL,NULL),(68,'Filipinas',1,'63',NULL,NULL),(69,'Finlandia',1,'358',NULL,NULL),(70,'Francia',1,'33',NULL,NULL),(71,'Gambia',1,'220',NULL,NULL),(72,'Georgia',1,'995',NULL,NULL),(73,'Ghana',1,'233',NULL,NULL),(74,'Gibraltar',1,'350',NULL,NULL),(75,'Granada',1,'1009',NULL,NULL),(76,'Grecia',1,'30',NULL,NULL),(77,'Groelandia',1,'299',NULL,NULL),(78,'Guadalupe',1,'590',NULL,NULL),(79,'Guam',1,'1671',NULL,NULL),(80,'Guatemala',1,'502',NULL,NULL),(81,'Guinea',1,'224',NULL,NULL),(82,'Guinea-Bissau',1,'245',NULL,NULL),(83,'Guinea Ecuatorial',1,'240',NULL,NULL),(84,'Guyana',1,'592',NULL,NULL),(85,'Guyana Francesa',1,'594',NULL,NULL),(86,'Haití',1,'509',NULL,NULL),(87,'Honduras',1,'504',NULL,NULL),(88,'Hong Kong',1,'852',NULL,NULL),(89,'Holanda',1,'31',NULL,NULL),(90,'Hungría',1,'36',NULL,NULL),(91,'India',1,'91',NULL,NULL),(92,'Indonesia',1,'62',NULL,NULL),(93,'Inglaterra / Reino Unido',1,'44',NULL,NULL),(94,'Irán',1,'98',NULL,NULL),(95,'Iraq',1,'964',NULL,NULL),(96,'Irlanda',1,'353',NULL,NULL),(97,'Isla Cabo Verde',1,'238',NULL,NULL),(98,'Isla Navidad',1,'6724',NULL,NULL),(99,'Isla Norforlk',1,'6723',NULL,NULL),(100,'Isla Reunión',1,'262',NULL,NULL),(101,'Islandía',1,'354',NULL,NULL),(102,'Islas Ascención',1,'247',NULL,NULL),(103,'Islas Caimán',1,'1006',NULL,NULL),(104,'Islas Cook',1,'682',NULL,NULL),(105,'Islas Faroe',1,'298',NULL,NULL),(106,'Islas Fiji',1,'679',NULL,NULL),(107,'Islas Maldivas',1,'960',NULL,NULL),(108,'Islas Malvinas',1,'500',NULL,NULL),(109,'Islas Marshall',1,'692',NULL,NULL),(110,'Islas Mauricio',1,'230',NULL,NULL),(111,'Isla Mayotte',1,'2696',NULL,NULL),(112,'Islas Salomón',1,'677',NULL,NULL),(113,'Isla Seychelles',1,'248',NULL,NULL),(114,'Islas Tonga',1,'676',NULL,NULL),(115,'Islas Vírgenes Americanas',1,'340',NULL,NULL),(116,'Islas Vírgenes Británicas',1,'284',NULL,NULL),(117,'Israel',1,'972',NULL,NULL),(118,'Italia',1,'39',NULL,NULL),(119,'Jamaica',1,'1010',NULL,NULL),(120,'Japón',1,'81',NULL,NULL),(121,'Jordania',1,'962',NULL,NULL),(122,'Kazakhstán',1,'731',NULL,NULL),(123,'Kenia',1,'254',NULL,NULL),(124,'Kiribati',1,'686',NULL,NULL),(125,'Kuwait',1,'965',NULL,NULL),(126,'Kyrgyzstán',1,'733',NULL,NULL),(127,'Laos',1,'856',NULL,NULL),(128,'Latvia',1,'371',NULL,NULL),(129,'Lesoto',1,'266',NULL,NULL),(130,'Líbano',1,'961',NULL,NULL),(131,'Liberia',1,'231',NULL,NULL),(132,'Libia',1,'218',NULL,NULL),(133,'Liechtenstein',1,'417',NULL,NULL),(134,'Lituania',1,'370',NULL,NULL),(135,'Luxemburgo',1,'352',NULL,NULL),(136,'Macao',1,'853',NULL,NULL),(137,'Macedonia',1,'389',NULL,NULL),(138,'Madagascar',1,'261',NULL,NULL),(139,'Malasia',1,'60',NULL,NULL),(140,'Malawi',1,'265',NULL,NULL),(141,'Malta',1,'356',NULL,NULL),(142,'Marruecos',1,'212',NULL,NULL),(143,'Mauritania',1,'222',NULL,NULL),(144,'México',1,'52',NULL,NULL),(145,'Micronesia',1,'691',NULL,NULL),(146,'Moldova',1,'373',NULL,NULL),(147,'Mónaco',1,'377',NULL,NULL),(148,'Mongolia',1,'976',NULL,NULL),(149,'Montserrat',1,'1011',NULL,NULL),(150,'Mozambique',1,'258',NULL,NULL),(151,'Myanmar',1,'95',NULL,NULL),(152,'Namibia',1,'264',NULL,NULL),(153,'Nauru',1,'674',NULL,NULL),(154,'Nepal',1,'977',NULL,NULL),(155,'Nevis',1,'1012',NULL,NULL),(156,'Nicaragua',1,'505',NULL,NULL),(157,'Niger',1,'227',NULL,NULL),(158,'Nigeria',1,'234',NULL,NULL),(159,'Niue',1,'683',NULL,NULL),(160,'Noruega',1,'47',NULL,NULL),(161,'Nueva Caledonia',1,'687',NULL,NULL),(162,'Nueva Zelanda',1,'64',NULL,NULL),(163,'Oceano Atlántico Este',1,'871',NULL,NULL),(164,'Oceano Atlántico Oeste',1,'874',NULL,NULL),(165,'Oceano Indico',1,'873',NULL,NULL),(166,'Oceano Pacífico',1,'872',NULL,NULL),(167,'Omán',1,'968',NULL,NULL),(168,'Pakistan',1,'92',NULL,NULL),(169,'Palau',1,'680',NULL,NULL),(170,'Panamá',1,'507',NULL,NULL),(171,'Papua Nueva Guinea',1,'675',NULL,NULL),(172,'Paraguay',1,'595',NULL,NULL),(173,'Perú',1,'51',NULL,NULL),(174,'Polonia',1,'48',NULL,NULL),(175,'Portugal',1,'351',NULL,NULL),(176,'Puerto Rico',1,'1787',NULL,NULL),(177,'Qatar',1,'974',NULL,NULL),(178,'República Central Africana',1,'236',NULL,NULL),(179,'República Checa',1,'42',NULL,NULL),(180,'República Dominicana',1,'1008',NULL,NULL),(181,'República Gabona',1,'241',NULL,NULL),(182,'República de Mali',1,'223',NULL,NULL),(183,'República de Senegal',1,'221',NULL,NULL),(184,'República de Vanuatú',1,'7377',NULL,NULL),(185,'Rumanía',1,'40',NULL,NULL),(186,'Russia',1,'7',NULL,NULL),(187,'Rwanda',1,'250',NULL,NULL),(188,'Saipán',1,'1670',NULL,NULL),(189,'Samoa Americana',1,'684',NULL,NULL),(190,'Samoa Oeste',1,'685',NULL,NULL),(191,'San Croix',1,'340',NULL,NULL),(192,'San John',1,'340',NULL,NULL),(193,'San Kitts',1,'1013',NULL,NULL),(194,'San Marino',1,'378',NULL,NULL),(195,'San Thomas',1,'340',NULL,NULL),(196,'San Vicente',1,'1015',NULL,NULL),(197,'Santa Elena',1,'290',NULL,NULL),(198,'Santa Lucia',1,'1014',NULL,NULL),(199,'Santa Piera y Miquelón',1,'508',NULL,NULL),(200,'Sao Tome',1,'239',NULL,NULL),(201,'Sierra Leona',1,'232',NULL,NULL),(202,'Singapur',1,'65',NULL,NULL),(203,'Siria',1,'963',NULL,NULL),(204,'Slovakia',1,'421',NULL,NULL),(205,'Slovenia',1,'386',NULL,NULL),(206,'Somalía',1,'252',NULL,NULL),(207,'Sri Lanka',1,'94',NULL,NULL),(208,'Sudáfrica',1,'27',NULL,NULL),(209,'Sudán',1,'249',NULL,NULL),(210,'Suecia',1,'46',NULL,NULL),(211,'Suiza',1,'41',NULL,NULL),(212,'Surinam',1,'597',NULL,NULL),(213,'Swazilandia',1,'268',NULL,NULL),(214,'Tahití / Polinesia Francesa',1,'689',NULL,NULL),(215,'Tailandía',1,'66',NULL,NULL),(216,'Taiwan',1,'886',NULL,NULL),(217,'Tajikstan',1,'73',NULL,NULL),(218,'Tanzania',1,'255',NULL,NULL),(219,'Togo',1,'228',NULL,NULL),(220,'Trinidad y Tobago',1,'1016',NULL,NULL),(221,'Tunisia',1,'216',NULL,NULL),(222,'Turquía',1,'90',NULL,NULL),(223,'Turkmenistán',1,'993',NULL,NULL),(224,'Tuvalú',1,'688',NULL,NULL),(225,'Uganda',1,'256',NULL,NULL),(226,'Ukrania',1,'380',NULL,NULL),(227,'Uruguay',1,'598',NULL,NULL),(228,'Uzbekistán',1,'737',NULL,NULL),(229,'Vaticano',1,'396',NULL,NULL),(230,'Venezuela',1,'58',NULL,NULL),(231,'Vietnam',1,'84',NULL,NULL),(232,'Wallis y Futuna',1,'681',NULL,NULL),(233,'Yemen',1,'967',NULL,NULL),(234,'Yugoslavia',1,'381',NULL,NULL),(235,'Zaire',1,'243',NULL,NULL),(236,'Zambia',1,'260',NULL,NULL),(237,'Zimbabwe',1,'263',NULL,NULL);
/*!40000 ALTER TABLE `countries` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dentalcases`
--

DROP TABLE IF EXISTS `dentalcases`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dentalcases` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `status` enum('active','pending','ended') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'pending',
  `status_read` enum('unread','read') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'unread',
  `messages_unread_4_dentist` int(10) unsigned NOT NULL DEFAULT '0',
  `messages_unread_4_admin` int(10) unsigned NOT NULL DEFAULT '0',
  `case_by_dentist` int(10) unsigned NOT NULL DEFAULT '0',
  `begin_date` date DEFAULT NULL,
  `ended_date` date DEFAULT NULL,
  `plan` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dentist_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `dentalcases_dentist_id_foreign` (`dentist_id`),
  CONSTRAINT `dentalcases_dentist_id_foreign` FOREIGN KEY (`dentist_id`) REFERENCES `dentists` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dentalcases`
--

LOCK TABLES `dentalcases` WRITE;
/*!40000 ALTER TABLE `dentalcases` DISABLE KEYS */;
INSERT INTO `dentalcases` VALUES (1,'active','read',1,0,1001,NULL,NULL,'plan2',1,'2018-11-06 15:44:41','2018-11-06 15:46:28'),(2,'active','read',1,0,1001,'2019-01-30','2019-08-08','plan2',2,'2018-11-19 16:17:53','2019-01-17 19:18:41'),(3,'active','read',1,0,1002,'2019-01-31','1920-01-31','plan2',2,'2019-01-16 19:39:44','2019-01-16 19:56:05');
/*!40000 ALTER TABLE `dentalcases` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dentist_dentist_speciality`
--

DROP TABLE IF EXISTS `dentist_dentist_speciality`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dentist_dentist_speciality` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `dentist_id` int(10) unsigned NOT NULL,
  `dentist_speciality_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `dentist_dentist_speciality_dentist_id_foreign` (`dentist_id`),
  KEY `dentist_dentist_speciality_dentist_speciality_id_foreign` (`dentist_speciality_id`),
  CONSTRAINT `dentist_dentist_speciality_dentist_id_foreign` FOREIGN KEY (`dentist_id`) REFERENCES `dentists` (`id`),
  CONSTRAINT `dentist_dentist_speciality_dentist_speciality_id_foreign` FOREIGN KEY (`dentist_speciality_id`) REFERENCES `dentist_specialities` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dentist_dentist_speciality`
--

LOCK TABLES `dentist_dentist_speciality` WRITE;
/*!40000 ALTER TABLE `dentist_dentist_speciality` DISABLE KEYS */;
INSERT INTO `dentist_dentist_speciality` VALUES (1,2,1,NULL,NULL),(2,2,10,NULL,NULL);
/*!40000 ALTER TABLE `dentist_dentist_speciality` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dentist_specialities`
--

DROP TABLE IF EXISTS `dentist_specialities`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dentist_specialities` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dentist_specialities`
--

LOCK TABLES `dentist_specialities` WRITE;
/*!40000 ALTER TABLE `dentist_specialities` DISABLE KEYS */;
INSERT INTO `dentist_specialities` VALUES (1,'Odontología general',NULL,NULL),(2,'Ortodoncia',NULL,NULL),(3,'Odontopediatría',NULL,NULL),(4,'Ortopedia infantil',NULL,NULL),(5,'Cirugía',NULL,NULL),(6,'Endodoncia',NULL,NULL),(7,'Patología',NULL,NULL),(8,'Implantología',NULL,NULL),(9,'Rehabilitación',NULL,NULL),(10,'Estética facial',NULL,NULL);
/*!40000 ALTER TABLE `dentist_specialities` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dentists`
--

DROP TABLE IF EXISTS `dentists`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dentists` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `cell_phone` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `city` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `company` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `file_thumb` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `document_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `supports_institution` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `associations` text COLLATE utf8mb4_unicode_ci,
  `university` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `file_document` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tyc` tinyint(1) DEFAULT NULL,
  `status` enum('complete','pending') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'pending',
  `user_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `country_id` int(10) unsigned NOT NULL,
  `code_phone_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `dentists_user_id_foreign` (`user_id`),
  KEY `dentists_country_id_foreign` (`country_id`),
  KEY `dentists_code_phone_id_foreign` (`code_phone_id`),
  CONSTRAINT `dentists_code_phone_id_foreign` FOREIGN KEY (`code_phone_id`) REFERENCES `countries` (`id`),
  CONSTRAINT `dentists_country_id_foreign` FOREIGN KEY (`country_id`) REFERENCES `countries` (`id`),
  CONSTRAINT `dentists_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dentists`
--

LOCK TABLES `dentists` WRITE;
/*!40000 ALTER TABLE `dentists` DISABLE KEYS */;
INSERT INTO `dentists` VALUES (1,'3147917302','La Ceja',NULL,NULL,NULL,NULL,NULL,'upb',NULL,1,'complete',2,'2018-11-06 15:43:07','2018-11-06 15:43:07',41,41),(2,'3016182208','Medellin',NULL,NULL,'1152437479','CES','SCAO','CES',NULL,1,'complete',3,'2018-11-15 16:24:16','2018-11-15 16:24:16',41,41);
/*!40000 ALTER TABLE `dentists` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `diagnostics`
--

DROP TABLE IF EXISTS `diagnostics`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `diagnostics` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `diagnostics` text COLLATE utf8mb4_unicode_ci,
  `sndiagnostic` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'N',
  `plan` text COLLATE utf8mb4_unicode_ci,
  `snplan` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'N',
  `dentalcase_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `diagnostics_dentalcase_id_foreign` (`dentalcase_id`),
  CONSTRAINT `diagnostics_dentalcase_id_foreign` FOREIGN KEY (`dentalcase_id`) REFERENCES `dentalcases` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `diagnostics`
--

LOCK TABLES `diagnostics` WRITE;
/*!40000 ALTER TABLE `diagnostics` DISABLE KEYS */;
INSERT INTO `diagnostics` VALUES (1,'{\"_token\":\"g2OrLFF7qajwia2NFANTRXI2bfaL9xVPbFKEobx7\",\"facial_morphology\":\"Mesoprosopo (cara con similiar medida en largo y ancho)\",\"type_of_smile\":\"Media (muestra entre el 100 y el 75% del diente y enc\\u00eda interproximal)\",\"fifth_facials\":\"Sim\\u00e9tricos\",\"thirds_facials\":\"Sim\\u00e9tricos\",\"facial_type\":\"Ortognatico \\/ mesognathic (recto)\",\"angle_nasolabial\":\"promedio\",\"angle_mentolabial\":\"agudo\",\"upper_lip\":\"agudo\",\"lower_lip\":\"obtuso\",\"lip_competition\":\"no\",\"chin\":\"prognatico\",\"relationship_canine_right\":\"clase II (el canino superior ocluye entre el canino y el incisivo lateral inferior.\",\"relationship_canine_left\":\"clase II (el canino superior ocluye entre el canino y el incisivo lateral inferior.)\",\"molar_relationship_right\":\"clase III (la c\\u00faspide mesiovestibular del primer molar superior ocluye mesial al surco mesiovestibular del primer molar inferior)\",\"overjet\":\"-2\",\"molar_relationship_left\":\"clase II (la c\\u00faspide mesiovestibular del primer molar superior ocluye c\\u00faspide a c\\u00faspide o en el espacio interproximal del primer molar inferior y el segundo premolar.)\",\"overbite\":\"1\",\"cross_bite\":\"no\",\"cross_bite_type\":\"Posterior\",\"match_between_mid_line_up_w_mid_line_facial\":\"no\",\"match_between_mid_line_dw_w_mid_line_up\":\"si\",\"form_upper_arch\":\"Cuadrado\",\"symmetry_upper_arch\":\"no\",\"shape_lower_arch\":\"Cuadrado\",\"symmetry_down_arch\":\"no\",\"top_crowding\":\"no\",\"top_crowding_type\":\"Moderado\",\"lower_crowding\":\"no\",\"lower_crowding_type\":\"Moderado\",\"top_diastema\":\"no\",\"lower_diastema\":\"no\",\"facial_diagnostic_profile_type\":\"Progantico \\/ prognathic (concavo)\",\"facial_diagnostic_facial_type\":\"Leptoprosopo\",\"facial_diagnostic_smile_type\":\"No\",\"facial_diagnostic_smile_type_other\":null,\"facial_diagnostic_facial_asymetrics\":\"No\",\"facial_diagnostic_facial_asymetrics_other\":null,\"facial_diagnostic_lip_competition\":\"Si\",\"facial_diagnostic_lip_competition_other\":null,\"facial_diagnostic_middle_line\":\"No\",\"facial_diagnostic_middle_line_where\":null,\"skeletal_diagnostic_rel_inter\":\"Clase II: (alteraci\\u00f3n en las bases \\u00f3seas de los maxilares. El maxilar superior est\\u00e1 bastante adelantado con respecto a la mand\\u00edbula en sentido antero-posterior)\",\"skeletal_diagnostic_maxilar_position\":\"Descendido\",\"skeletal_diagnostic_mandible_position\":\"Promedio\",\"skeletal_diagnostic_maxilar_rotation\":\"Rotaci\\u00f3n neutra: (no hay rotaci\\u00f3n en ning\\u00fan sentido)\",\"skeletal_diagnostic_mandible_rotation\":\"Rotaci\\u00f3n anterior o en sentido de las manecillas del reloj: (gira abajo y atr\\u00e1s en cuadrante inferior derecho del sistema rectangular de coordenadas cartesianas.)\",\"skeletal_diagnostic_maxilar_size\":\"Promedio\",\"skeletal_diagnostic_mandible_size\":\"Promedio\",\"oclusal_diagnostic_right_type\":\"Clase II\",\"oclusal_diagnostic_left_type\":\"Clase II\",\"oclusal_diagnostic_inclination_superior_incisors\":\"Labializado (inclinaci\\u00f3n aumentada de los dientes con respecto a una l\\u00ednea vertical, estando siempre el borde incisal por delante del \\u00e1pice del mismo diente)\",\"oclusal_diagnostic_inclination_inferior_incisors\":\"Lingualizado (inclinaci\\u00f3n disminuida de los dientes con respecto a una l\\u00ednea vertical, estando el borde incisal por delante del \\u00e1pice del mismo diente, o aumentada, estando el borde incisal por detr\\u00e1s del \\u00e1pice)\",\"oclusal_diagnostic_molar_superior_vertial_position\":\"Promedio\",\"oclusal_diagnostic_molar_inferior_vertial_position\":\"Promedio\",\"oclusal_diagnostic_crowding\":[\"Inferior\"],\"oclusal_diagnostic_diasistema\":[\"Inferior\"],\"oclusal_diagnostic_cross_bite\":\"Si\",\"oclusal_diagnostic_cross_bite_where\":null,\"oclusal_diagnostic_open_bite\":\"Si\",\"oclusal_diagnostic_open_bite_where\":null,\"oclusal_diagnostic_teeth_included\":\"Si\",\"oclusal_diagnostic_teeth_included_where\":null,\"oclusal_diagnostic_deep_bite\":\"Si\"}','S','{\"_token\":\"g2OrLFF7qajwia2NFANTRXI2bfaL9xVPbFKEobx7\",\"aparatology_brackets_slot\":\"0.018 X 0.025\",\"aparatology_brackets_prescription\":\"MBT WITH HOOK\",\"aparatology_brackets_technique\":\"AUTOLIGADO PASIVO\",\"aparatology_brackets_tubes_open_size\":\"0.022 X 0.028\",\"aparatology_brackets_tubes_position\":\"INVERTIDOS DE LATERAL A LATERAL\",\"aparatology_brackets_tubes_teeth\":\"SECOND MOLAR\",\"aparatology_brackets_used_arch\":\"0.014 NITI\",\"aparatology_brackets_arch_shape\":\"BROAD\",\"aparatology_brackets_itermax_elastics\":\"1\\/8 HEAVY\",\"aparatology_brackets_aditional_material\":\"LIGADURA METALICA\",\"paratology_brackets_installation_material\":\"BUCAL RETRACTORS\",\"paratology_brackets_high_overbite\":\"CUSPID: 4.5  MM\",\"paratology_brackets_high_open_bites\":\"FIRST PREMOLAR: 4,5 MM\",\"orthopedics_appliances\":\"TRACCION ALTA\",\"orthopedics_appliances_other\":\"34\",\"orthopedics_appliances_desc\":\"234\",\"trament_plan_exod_pipes\":\"LOWER 876678\",\"trament_plan_exod_braces\":\"SI\",\"trament_plan_exod_extract_teeth\":\"LOWER:  8765432112345678\",\"trament_plan_exod_extract_begin_wire\":\"0.013\",\"trament_plan_exod_appointment_begin\":\"20 DIAS\",\"trament_plan_evol_line_teeth_up_inf\":\"SI\",\"trament_plan_evol_inprov_overbite\":\"NO\",\"trament_plan_evol_inprov_overbite_how\":null,\"trament_plan_evol_rel_can_right_1\":\"SI\",\"trament_plan_evol_mec_rel_can_1\":\"SI\",\"trament_plan_evol_llev_rel_mol\":\"NO\",\"trament_plan_evol_llev_rel_mol_how\":null,\"trament_plan_evol_llev_rel_mol_c_1_how\":null,\"trament_plan_evol_apa_rel_mol_c_1\":\"SI\",\"trament_plan_evol_llev_rel_mol_c_1\":\"CLASE II\",\"trament_plan_evol_llev_rel_mol_c_1_reason\":null,\"trament_plan_evol_close_spac_rel_mol_c_1\":\"SI\",\"trament_plan_evol_adjust_mordida\":\"SI\"}','S',1,'2018-11-06 15:50:48','2018-11-06 15:51:47'),(2,'{\"_token\":\"w16ccVFbu9kQGtLOqNvvOFCmwn9Q5H8sa7dGLETI\",\"facial_morphology\":\"Mesoprosopo\",\"type_of_smile\":\"Media\",\"fifth_facials\":\"Sim\\u00e9tricos\",\"thirds_facials\":\"Sim\\u00e9tricos\",\"facial_type\":\"Ortognatico \\/ mesognathic\",\"angle_nasolabial\":\"obtuso\",\"angle_mentolabial\":\"agudo\",\"upper_lip\":\"obtuso\",\"lower_lip\":\"promedio\",\"lip_competition\":\"si\",\"chin\":\"promedio\",\"relationship_canine_right\":\"clase I\",\"relationship_canine_left\":\"clase I\",\"molar_relationship_right\":\"clase I\",\"overjet\":\"-1\",\"molar_relationship_left\":\"clase I\",\"overbite\":\"2\",\"cross_bite\":\"no\",\"match_between_mid_line_up_w_mid_line_facial\":\"no\",\"match_between_mid_line_dw_w_mid_line_up\":\"no\",\"form_upper_arch\":\"Ovalado\",\"symmetry_upper_arch\":\"no\",\"shape_lower_arch\":\"Ovalado\",\"symmetry_down_arch\":\"si\",\"top_crowding\":\"si\",\"lower_crowding\":\"si\",\"lower_crowding_type\":\"Leve\",\"top_diastema\":\"si\",\"lower_diastema\":\"no\",\"facial_diagnostic_profile_type\":\"Ortognatico \\/ mesognathic\",\"facial_diagnostic_smile_type_other\":null,\"facial_diagnostic_facial_asymetrics\":\"No\",\"facial_diagnostic_facial_asymetrics_other\":null,\"facial_diagnostic_lip_competition\":\"Si\",\"facial_diagnostic_lip_competition_other\":null,\"facial_diagnostic_middle_line\":\"No\",\"facial_diagnostic_middle_line_where\":null,\"skeletal_diagnostic_rel_inter\":\"Clase I\",\"skeletal_diagnostic_maxilar_position\":\"Promedio\",\"skeletal_diagnostic_mandible_position\":\"Promedio\",\"skeletal_diagnostic_maxilar_rotation\":\"Rotaci\\u00f3n neutra\",\"skeletal_diagnostic_mandible_rotation\":\"Rotaci\\u00f3n neutra\",\"skeletal_diagnostic_maxilar_size\":\"Promedio\",\"skeletal_diagnostic_mandible_size\":\"Promedio\",\"oclusal_diagnostic_right_type\":\"Clase I\",\"oclusal_diagnostic_left_type\":\"Clase I\",\"oclusal_diagnostic_inclination_superior_incisors\":\"Promedio\",\"oclusal_diagnostic_inclination_inferior_incisors\":\"Promedio\",\"oclusal_diagnostic_molar_superior_vertial_position\":\"Promedio\",\"oclusal_diagnostic_molar_inferior_vertial_position\":\"Promedio\",\"oclusal_diagnostic_crowding\":[\"Si\",\"Superior\",\"Inferior\"],\"oclusal_diagnostic_diasistema\":[\"Si\"],\"oclusal_diagnostic_cross_bite\":\"No\",\"oclusal_diagnostic_cross_bite_where\":null,\"oclusal_diagnostic_open_bite\":\"No\",\"oclusal_diagnostic_open_bite_where\":null,\"oclusal_diagnostic_teeth_included\":\"Si\",\"oclusal_diagnostic_teeth_included_where\":\"23\",\"oclusal_diagnostic_deep_bite\":\"Si\"}','S','{\"_token\":\"w16ccVFbu9kQGtLOqNvvOFCmwn9Q5H8sa7dGLETI\",\"aparatology_brackets_slot\":\"0.022 X 0.028\",\"aparatology_brackets_prescription\":\"ROTH WITH HOOK\",\"aparatology_brackets_technique\":\"PLACAS ACTIVAS\",\"aparatology_brackets_tubes_open_size\":\"0.022 X 0.028\",\"aparatology_brackets_tubes_position\":\"NORMAL\",\"aparatology_brackets_tubes_teeth\":\"SECOND MOLAR\",\"aparatology_brackets_used_arch\":\"0.012 NITI\",\"aparatology_brackets_arch_shape\":\"NATURAL\",\"aparatology_brackets_itermax_elastics\":null,\"aparatology_brackets_aditional_material\":null,\"paratology_brackets_high_overbite\":\"CENTRAL: 4.5 MM\",\"paratology_brackets_high_open_bites\":null,\"orthopedics_appliances\":null,\"orthopedics_appliances_other\":null,\"orthopedics_appliances_desc\":null,\"trament_plan_exod_extract_teeth\":\"UPPER:  8765432112345678\",\"trament_plan_evol_line_teeth_up_inf\":\"SI\",\"trament_plan_evol_inprov_overbite\":\"SI\",\"trament_plan_evol_inprov_overbite_how\":null,\"trament_plan_evol_rel_can_right_1\":\"SI\",\"trament_plan_evol_mec_rel_can_1\":\"SI\",\"trament_plan_evol_llev_rel_mol\":\"SI\",\"trament_plan_evol_llev_rel_mol_how\":null,\"trament_plan_evol_llev_rel_mol_c_1_how\":null,\"trament_plan_evol_apa_rel_mol_c_1\":\"SI\",\"trament_plan_evol_llev_rel_mol_c_1\":\"CLASE I\",\"trament_plan_evol_llev_rel_mol_c_1_reason\":null,\"trament_plan_evol_close_spac_rel_mol_c_1\":\"SI\",\"trament_plan_evol_adjust_mordida\":\"SI\"}','S',3,'2019-01-16 20:50:08','2019-01-16 21:07:37');
/*!40000 ALTER TABLE `diagnostics` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `messages`
--

DROP TABLE IF EXISTS `messages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `messages` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `text` text COLLATE utf8mb4_unicode_ci,
  `file_attach` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` enum('unread','read') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'unread',
  `user_id` int(10) unsigned NOT NULL,
  `dentalcase_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `messages_user_id_foreign` (`user_id`),
  KEY `messages_dentalcase_id_foreign` (`dentalcase_id`),
  CONSTRAINT `messages_dentalcase_id_foreign` FOREIGN KEY (`dentalcase_id`) REFERENCES `dentalcases` (`id`),
  CONSTRAINT `messages_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `messages`
--

LOCK TABLES `messages` WRITE;
/*!40000 ALTER TABLE `messages` DISABLE KEYS */;
INSERT INTO `messages` VALUES (1,'Hola, ya hemos recibido tu caso, en breve nos podremos a revisarlo a fondo y te daremos un diagnóstico. Máximo en 5 días hábiles te responderemos.',NULL,'unread',1,1,'2018-11-06 15:46:28','2018-11-06 15:46:28',NULL),(2,'Se ha creado un diagnostico.',NULL,'unread',1,1,'2018-11-06 15:50:48','2018-11-06 15:50:48','new_diagnostic'),(3,'Se ha creado un plan de tratamiento.',NULL,'unread',1,1,'2018-11-06 15:51:09','2018-11-06 15:51:09','new_treatment'),(4,'Hola, ya hemos recibido tu caso, en breve nos podremos a revisarlo a fondo y te daremos un diagnóstico. Máximo en 5 días hábiles te responderemos.',NULL,'unread',1,3,'2019-01-16 19:56:05','2019-01-16 19:56:05',NULL),(5,'Se ha creado un diagnostico.',NULL,'unread',1,3,'2019-01-16 20:50:08','2019-01-16 20:50:08','new_diagnostic'),(6,'Se ha creado un plan de tratamiento.',NULL,'unread',1,3,'2019-01-16 21:07:37','2019-01-16 21:07:37','new_treatment'),(7,'Hola, ya hemos recibido tu caso, en breve nos podremos a revisarlo a fondo y te daremos un diagnóstico. Máximo en 5 días hábiles te responderemos.',NULL,'unread',1,2,'2019-01-17 19:18:41','2019-01-17 19:18:41',NULL);
/*!40000 ALTER TABLE `messages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=69 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES (56,'2014_10_12_000000_create_users_table',1),(57,'2014_10_12_100000_create_password_resets_table',1),(58,'2018_03_22_185555_create_dentists_table',1),(59,'2018_03_22_190225_create_dentist_specialities_table',1),(60,'2018_03_28_183429_create_countries_table',1),(61,'2018_03_28_183640_add_country_to_dentists_table',1),(62,'2018_03_31_064211_create_cases_table',1),(63,'2018_03_31_064336_create_messages_table',1),(64,'2018_03_31_064359_create_patients_table',1),(65,'2018_03_31_064517_create_payments_table',1),(66,'2018_05_01_211437_create_diagnostics_table',1),(67,'2018_10_28_132612_add_code_country_to_dentists_table',1),(68,'2018_11_06_111416_add_type_to_messages_table',1);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `password_resets`
--

LOCK TABLES `password_resets` WRITE;
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
INSERT INTO `password_resets` VALUES ('mari_agudelo29@hotmail.com','c8bf76d2a70c03a3611648efedee43eb',NULL);
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `patients`
--

DROP TABLE IF EXISTS `patients`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `patients` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `age` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `genere` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `radiography1` text COLLATE utf8mb4_unicode_ci,
  `radiography2` text COLLATE utf8mb4_unicode_ci,
  `radiography3` text COLLATE utf8mb4_unicode_ci,
  `radiography4` text COLLATE utf8mb4_unicode_ci,
  `radiography5` text COLLATE utf8mb4_unicode_ci,
  `radiography6` text COLLATE utf8mb4_unicode_ci,
  `radiography7` text COLLATE utf8mb4_unicode_ci,
  `radiography8` text COLLATE utf8mb4_unicode_ci,
  `radiography9` text COLLATE utf8mb4_unicode_ci,
  `radiography10` text COLLATE utf8mb4_unicode_ci,
  `radiography11` text COLLATE utf8mb4_unicode_ci,
  `radiography12` text COLLATE utf8mb4_unicode_ci,
  `radiography13` text COLLATE utf8mb4_unicode_ci,
  `objective_patient` text COLLATE utf8mb4_unicode_ci,
  `objective_dentist` text COLLATE utf8mb4_unicode_ci,
  `dentalcase_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `patients_dentalcase_id_foreign` (`dentalcase_id`),
  CONSTRAINT `patients_dentalcase_id_foreign` FOREIGN KEY (`dentalcase_id`) REFERENCES `dentalcases` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `patients`
--

LOCK TABLES `patients` WRITE;
/*!40000 ALTER TABLE `patients` DISABLE KEYS */;
INSERT INTO `patients` VALUES (1,'hans','21','m',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,'2018-11-06 15:44:41','2018-11-06 15:44:41'),(2,'Tatiana Agudelo','22','f','c0iw9iEmgJoJOHX8bm7o9A9pL7XeBsjFl210LupJ.jpeg','WImr3sBSRaovass0CtPcxrLCTmtW7Xw5IlJvTAeJ.jpeg',NULL,'1IxItAHLjtJSGqnKFy3fWWtX0IRcA2ndI9Tj8C6Q.jpeg',NULL,'BzQCsr9fg1UuKqApt91xxqyOPe4tYAR7y4f8LXzx.png','4Jq7nNrU0skb3jm0YBTwxczGr2aLtq6UV3X4Vbkl.png','l941Wzv0yO0mGV837xzwzenkSBegsPkLTVeOLGTw.png','vkmyOklQg753xuWotmMDxY0dhkdZa6xGLR6Wmq2A.png','csQhDIBcbZ8pR9mF8uMABzeWNsykBycNEpq64yo9.png',NULL,'PlHX7yYEkyIQpjbW0UE2fRIsbdYLICnOeyQGsPq6.png','LwdHy2E0BEUc31o8u7KR8dj40SkPBYstTnqZIeJs.png','Mejorar mis dientes','Lograr una mejor oclusión',2,'2018-11-19 16:17:53','2018-11-19 16:17:53'),(3,'Juanita Laverde','12','f',NULL,'FE6NraemD2BXTykYyvKvtTFsGE8Ol2k1XTneQ9S4.jpeg',NULL,NULL,NULL,'PUUFbU7ft2juUds4Ubxo4lRxUnRPmxH140d2bRz3.png','Dv44mXtlIVWhT8hDVGBtqHVQRiNtmtpINZO413M3.png','hZC0ZthRLuU5gsi8EqCLNhGBpC0GzpK2sQPKLLQr.png','6WPkrTvIuQ25o3eVaR00pLjIm2dGV76NWo6TCkeR.png','PSC9tsSstsyYkHeioK0SSfRHbIdanfjbYb7wp2Pj.png',NULL,'niOVlklUo9v8luBucdkK4zz2U8sQXhTVGuv4RvLF.png','Epfz7MpjZoSYGTNUR7CJomIPOPPiyLtLaSCpvo7P.png','MEJORAR LA APARIENCIA DE MIS DIENTES','EXTRUIR EL CANINO',3,'2019-01-16 19:39:44','2019-01-16 19:39:44');
/*!40000 ALTER TABLE `patients` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `payments`
--

DROP TABLE IF EXISTS `payments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `payments` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `dentalcase_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `payments_dentalcase_id_foreign` (`dentalcase_id`),
  CONSTRAINT `payments_dentalcase_id_foreign` FOREIGN KEY (`dentalcase_id`) REFERENCES `dentalcases` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `payments`
--

LOCK TABLES `payments` WRITE;
/*!40000 ALTER TABLE `payments` DISABLE KEYS */;
/*!40000 ALTER TABLE `payments` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` enum('admin','assistant','dentist') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'dentist',
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'Administrador','cristian@puntodos.co','$2y$10$qgirkv/8oZihPhpcrSr3jeD328IGUANrrVwH5xiavgqxvsroxc2vu','admin','n55v5FGC2h8trIDVLnCIKpthXw0yPfZalMgv60SCciFvtffhfKtGp63PNS5q',NULL,NULL),(2,'Dentista Carlo','prueba@prueba.com','$2y$10$OOehC0Ch1UGrkWUPXGGp..ehV0o9Ebi9fKRKnk4wKnJi46wcNizR.','dentist',NULL,NULL,NULL),(3,'Mariana Agudelo','mari_agudelo29@hotmail.com','$2y$10$1rsyLJj09vIryyy3/YrGnuqTSFG6I.6aB6vLG/n/LKeEO8BNiLNcm','dentist','DPHDQ8PhFaULMOL8r1fUCuSOkkPUOQK0Hek24c3nflZlc9rrBnoe3XZZQC92','2018-11-15 16:03:07','2019-01-17 19:17:39'),(4,'Jaime Agudelo Berrio','jaimeandresagudelo@gmail.com','$2y$10$DRvc.xeg3ppQwN10m8q49OEJx2k52KL870ncm8q0KtjtLdzmiy4Jm','dentist','HqqDWCzjXPOJNDN49TTICDSo65eDwwi3fVJAgJbvxl1KrLmTB5EXsPKyCM9w','2018-12-12 20:18:52','2018-12-12 20:18:52'),(5,'MichaelREICA','yourmail@gmail.com','$2y$10$rafODVXFCohOoIdZnG7xEeURe5PeH5872BVaESmdvNfFO/Lif7E7u','dentist',NULL,'2018-12-19 12:12:23','2018-12-19 12:12:23'),(6,'a','a@a.com','$2y$10$/9dfRsvU57YYIbcPyjgFYeGSSuAW8BysGwS.S1bArI63qynEkAEgW','dentist','kXschsaPUCQIc2DqOCwpvYyJLe6JhZydDl3ANqBrPmxXDddQaS8fzxcB0Y0t','2020-05-11 21:01:27','2020-05-11 21:01:27'),(7,'<script>alerT(1);</script>','an@a.com','$2y$10$prkr3A9K7SjdvWjcszWJkeruhrRl7mX1a.7ou7qhIsvwR01MG836O','dentist',NULL,'2020-05-11 21:03:17','2020-05-11 21:03:17');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-07-24  0:25:31
